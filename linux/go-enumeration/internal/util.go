package internal

import (
	"fmt"
	"io/fs"
	"os"
	"os/exec"
	"os/user"
	"strings"
	"syscall"
	"time"

	"github.com/charmbracelet/log"
)

// Convert a slice of bytes to string, strip whitespace, and split by newline.
func SplitLinesBytes(input []byte) []string {
	return strings.Split(strings.TrimSpace(string(input[:])), "\n")
}

// Remove duplicates from a slice of strings.
func RemoveDuplicateStr(strSlice []string) []string {
	allKeys := make(map[string]bool)
	list := []string{}

	for _, item := range strSlice {
		if _, value := allKeys[item]; !value {
			allKeys[item] = true
			list = append(list, item)
		}
	}

	return list
}

// Check if a file exists.
func FileExists(filename string) bool {
	if _, err := os.Stat(filename); err == nil {
		return true
	} else {
		return false
	}
}

// Get a file's mode.
func GetFileMode(filename string) fs.FileMode {
	info, err := os.Stat(filename)

	if err != nil {
		fmt.Fprintf(os.Stderr, "error: %v\n", err.Error())
		os.Exit(1)
	}

	return info.Mode()
}

// Check if a file is writable by the owner.
func FileWritableByOwner(filename string) bool {
	return GetFileMode(filename)&USER_W_PERMS != 0
}

// Check if a file is readable by the owning group.
func FileReadableByGroup(filename string) bool {
	return GetFileMode(filename)&GROUP_R_PERMS != 0
}

// Check if a file is writable by the owning group.
func FileWritableByGroup(filename string) bool {
	return GetFileMode(filename)&GROUP_W_PERMS != 0
}

// Check if a file is readable by others.
func FileReadableByOthers(filename string) bool {
	return GetFileMode(filename)&OTHER_R_PERMS != 0
}

// Check if a file is writable by others.
func FileWritableByOthers(filename string) bool {
	return GetFileMode(filename)&OTHER_W_PERMS != 0
}

// Execute a shell command and return stdout if it succeeds.
//
// TODO: This doesn't handle quotes in commands, but I don't anticipate that being needed for now.
func ExecuteShell(command string) (string, error) {
	command = strings.TrimSpace(command)

	parts := strings.Split(command, " ")
	executable := parts[0]

	args := make([]string, 0)

	if len(parts) > 1 {
		args = parts[1:]
	}

	cmd := exec.Command(executable, args...)
	stdout, err := cmd.Output()

	if err != nil {
		return "", err
	}

	return string(stdout), nil
}

// Get the hostname.
func GetHostname() string {
	hostname, err := os.Hostname()
	if err != nil {
		log.Fatal(err)
	}
	return hostname
}

// Return a string representing the date and time in ISO 8601 format.
func Isoformat(t time.Time) string {
	return t.Format("2006-01-02T15:04:05+00:00")
}

// Return a string representing the current date and time (UTC) in ISO 8601 format.
func IsoformatUtcNow() string {
	return Isoformat(time.Now().UTC())
}

// Check if a file is readable.
func CheckFileReadable(path string) bool {
	if _, err := os.ReadFile(strings.TrimSpace(path)); err != nil {
		return false
	} else {
		return true
	}
}

// Get the username of the user that owns a file.
func GetFileUsername(path string) string {
	info, err := os.Stat(path)

	if err != nil {
		log.Fatal(err)
	}

	if stat, ok := info.Sys().(*syscall.Stat_t); ok {
		uid := int(stat.Uid)
		if user, err := user.LookupId(fmt.Sprint(uid)); err != nil {
			return ""
		} else {
			return user.Username
		}
	} else {
		return ""
	}
}

// Get the name of the group that owns a file.
func GetFileGroupName(path string) string {
	info, err := os.Stat(path)

	if err != nil {
		log.Fatal(err)
	}

	if stat, ok := info.Sys().(*syscall.Stat_t); ok {
		gid := int(stat.Gid)
		if group, err := user.LookupGroupId(fmt.Sprint(gid)); err != nil {
			return ""
		} else {
			return group.Name
		}
	} else {
		return ""
	}
}

func Filter[T any](ss []T, test func(T) bool) (ret []T) {
	for _, s := range ss {
		if test(s) {
			ret = append(ret, s)
		}
	}
	return
}
