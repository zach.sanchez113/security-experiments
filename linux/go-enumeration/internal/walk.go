package internal

import (
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

// Search for paths in `root` that match regexp `pattern`
func locatePaths(root string, pattern string, matchDirs bool) []string {
	var matchedPaths []string

	// Easy mistake to make
	if matchDirs {
		pattern = strings.TrimSuffix(pattern, "/")
	}

	fileSystem := os.DirFS(root)
	r := regexp.MustCompile(pattern)

	fs.WalkDir(fileSystem, ".", func(path string, d fs.DirEntry, err error) error {
		// Can happen for permission denied
		if err != nil {
			return nil
		}

		if !matchDirs && d.IsDir() {
			return nil
		} else if matchDirs && !d.IsDir() {
			return nil
		}

		fullPath := filepath.Join(fmt.Sprintf("%s/%s", root, path))

		if r.MatchString(fullPath) {
			matchedPaths = append(matchedPaths, fullPath)
		}

		return nil
	})

	return matchedPaths
}

// Search for files in `root` that match regexp `pattern`.
func locateFiles(root string, pattern string) []string {
	return locatePaths(root, pattern, false)
}

// Search for directories in `root` that match regexp `pattern`.
func locateDirs(root string, pattern string) []string {
	return locatePaths(root, pattern, true)
}
