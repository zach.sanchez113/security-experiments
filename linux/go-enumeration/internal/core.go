package internal

import (
	"encoding/json"
	"fmt"

	"github.com/charmbracelet/log"
)

func RunEnum() {
	log.Info("Beginning enumeration...")

	var results []TestResult

	output := EnumOutput{
		Start: IsoformatUtcNow(),
		End:   "",
	}

	results = append(results, BaseFindings...)
	results = append(results, RunSSHKeyTests()...)

	// Cut down on clutter + potential for mistakes elsewhere in the code
	for _, result := range results {
		if result.Hostname == "" {
			result.Hostname = Hostname
		}
	}

	output.Results = results
	output.End = IsoformatUtcNow()

	x, err := json.Marshal(output)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("%s\n", x)
}
