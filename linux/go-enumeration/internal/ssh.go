package internal

import (
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/charmbracelet/log"
)

// Look for id_rsa and id_dsa files across the entire FS, and for any non-config files in user .ssh/
// directories.
//
// TODO: Walk the root directory, not just /home/
func GetSSHKeys() []string {
	log.Info("Looking for obvious SSH keys...")
	sshKeys := locateFiles("/home/", "[\\w\\-]/\\.ssh/id_[dr]sa")

	log.Info("Walking /home/ for .ssh/ directories...")
	for _, d := range locateDirs("/home/", "/\\.ssh") {
		log.Infof("Found .ssh/ directory: %s", d)

		entries, err := os.ReadDir(d)
		if err != nil {
			log.Infof(".ssh/ directory isn't readable: %s", d)
			continue
		}

		log.Infof("Number of files: %d", len(entries))

		for _, e := range entries {
			fullPath := filepath.Join(fmt.Sprintf("%s/%s", d, e.Name()))

			// Exclude likely-benign files
			r := regexp.MustCompile("authorized_keys.*|config|environment.*")

			if !r.MatchString(filepath.Base(fullPath)) {
				sshKeys = append(sshKeys, fullPath)
			}
		}
	}

	return RemoveDuplicateStr(sshKeys)
}

// Locate all PEM files.
//
// WARNING: If more than 100 PEM files are found, this function will not return anything since it's indicative
// of a lot of noise, e.g. you're in a desktop environment where lots of random things could've downloaded PEM
// files.
func GetPEMFiles() []string {
	log.Info("Looking for PEM files...")
	pemFiles := locateFiles("/", "\\.pem")

	if len(pemFiles) > 100 {
		return make([]string, 0)
	}

	return pemFiles
}

func RunSSHKeyTests() []TestResult {
	var results []TestResult

	sshKeys := GetSSHKeys()
	privateKeys := Filter(sshKeys, func(fp string) bool { return !strings.Contains(fp, ".pub") })
	publicKeys := Filter(sshKeys, func(fp string) bool { return strings.Contains(fp, ".pub") })
	pemFiles := GetPEMFiles()

	results = append(results, TestResult{
		Key:           "ssh_private_key",
		Title:         "SSH Private Keys",
		Informational: true,
		Data:          privateKeys,
	})

	results = append(results, TestResult{
		Key:           "ssh_public_key",
		Title:         "SSH Public Keys",
		Informational: true,
		Data:          publicKeys,
	})

	results = append(results, TestResult{
		Key:           "pem",
		Title:         "PEM Files",
		Informational: true,
		Data:          pemFiles,
	})

	var privateKeysWorldReadable []WorldReadableSecretsFile
	var pemFilesWorldReadable []WorldReadableSecretsFile
	var sshDirsWorldReadable []WorldReadableSecretsFile

	// Check for world-readable SSH keys
	for _, path := range privateKeys {
		if !FileExists(path) {
			log.Debugf("Path doesn't exist: %s", path)
			continue
		}
		if CheckFileReadable(path) {
			results = append(results, TestFileContents(path))
		}
		if FileReadableByOthers(path) {
			privateKeysWorldReadable = append(privateKeysWorldReadable, WorldReadableSecretsFile{
				Path:  path,
				User:  GetFileUsername(path),
				Group: GetFileGroupName(path),
			})
		}
	}

	if len(privateKeysWorldReadable) > 1 {
		results = append(results, TestResult{
			Key:           "world_readable_ssh_private_key",
			Title:         "World-Readable SSH Private Keys",
			Description:   "All SSH private keys should only be readable by the file's owner.",
			Informational: false,
			Remediation:   "Run `chmod 600 /path/to/ssh/key` to ensure that only the file's owner can read the SSH private key.",
			Data:          privateKeysWorldReadable,
		})
	}

	// Check for world-readable .ssh/ directories, create a finding for each + spit out file contents as
	// informational findings so they can be reviewed manually
	for _, d := range locateDirs("/home/", "/\\.ssh/") {
		if !FileExists(d) {
			log.Debugf("Path doesn't exist: %s", d)
			continue
		}
		if FileReadableByOthers(d) {
			sshDirsWorldReadable = append(sshDirsWorldReadable, WorldReadableSecretsFile{
				Path:  d,
				User:  GetFileUsername(d),
				Group: GetFileGroupName(d),
			})

			entries, err := os.ReadDir(d)
			if err != nil {
				log.Fatal(err)
			}

			for _, e := range entries {
				fullPath := filepath.Join(fmt.Sprintf("%s/%s", d, e))
				results = append(results, TestFileContents(fullPath))
			}
		}
	}

	if len(sshDirsWorldReadable) > 1 {
		results = append(results, TestResult{
			Key:           "world_readable_ssh_dir",
			Title:         "World-Readable .ssh/ Directories",
			Description:   "All .ssh/ directories should only be readable by the file's owner.",
			Informational: false,
			Remediation:   "Run `chmod 700 /path/to/.ssh/` to ensure that only the file's owner can read the directory.",
			Data:          sshDirsWorldReadable,
		})
	}

	// Check for world-readable PEM files
	// May or may not be an issue depending on the contents
	for _, path := range pemFiles {
		if !FileExists(path) {
			log.Debugf("Path doesn't exist: %s", path)
			continue
		}
		if FileReadableByOthers(path) {
			pemFilesWorldReadable = append(pemFilesWorldReadable, WorldReadableSecretsFile{
				Path:  path,
				User:  GetFileUsername(path),
				Group: GetFileGroupName(path),
			})
		}
	}

	if len(pemFilesWorldReadable) > 1 {
		results = append(results, TestResult{
			Key:           "world_readable_pem",
			Title:         "World-Readable PEM Files",
			Description:   "If this contains a secret, the file should only be readable by the file's owner.",
			Informational: false,
			Remediation:   "Run `chmod 600 /path/to/ssh/key` to ensure that only the file's owner can read the PEM file.",
			Data:          pemFilesWorldReadable,
		})
	}

	return results
}
