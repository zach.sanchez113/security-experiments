package internal

import (
	"os"
	"strings"
)

// Shell command output.
type CommandOutput struct {
	Command string // Command that was executed.
	Success bool   // Whether the command was successful.
	Output  string // Stdout if the command was successful.
}

// File contents.
type FileContents struct {
	Path     string // Filepath.
	Success  bool   // Whether the file could be read.
	Contents string // File contents if the file could be read.
}

// Files where we don't care about the contents, and instead want metadata.
//
// This is distinct from `FileContents` because I'd rather avoid duplicating data also found in informational
// findings, and I only care about ownership in certain scenarios, e.g. world-readable SSH keys.
type WorldReadableSecretsFile struct {
	Path  string
	User  string
	Group string
}

// An individual test result.
// TODO: Get rid of the user and group fields?
type TestResult struct {
	Key           string      // Internal key to make it easier to manipulate results.
	Title         string      // Title.
	Description   string      // Description of what was found.
	Informational bool        // Is this an issue or just informational?
	Remediation   string      // If this is an issue, how can it be fixed?
	Hostname      string      // Affected host.
	Data          interface{} // Any extra data associated with the finding.
}

// The actual JSON output of the enum.
type EnumOutput struct {
	Start   string       // Start time.
	End     string       // End time.
	Results []TestResult // Test results.
}

// Get file contents, using an empty string if an error occurs.
func GetFileContents(path string) FileContents {
	path = strings.TrimSpace(path)

	if contentsRaw, err := os.ReadFile(path); err != nil {
		return FileContents{
			Path:     path,
			Success:  false,
			Contents: "",
		}
	} else {
		return FileContents{
			Path:     path,
			Success:  true,
			Contents: string(contentsRaw),
		}
	}
}

// Get an informational TestResult for a file.
func TestFileContents(path string) TestResult {
	path = strings.TrimSpace(path)

	// Make the path underscore-delimited, e.g. /etc/passwd should become etc_passwd
	trimmedPath := strings.Trim(path, "/")
	key := strings.ReplaceAll(trimmedPath, "/", "_")

	// Other characters that could mess with the key
	key = strings.ReplaceAll(key, "-", "_")
	key = strings.ReplaceAll(key, " ", "_")

	return TestResult{
		Key:           key,
		Title:         path,
		Informational: true,
		Data:          GetFileContents(path),
	}
}

// Get sehll command output, using an empty string if an error occurs.
func GetCommandOutput(command string) CommandOutput {
	command = strings.TrimSpace(command)

	if stdout, err := ExecuteShell(command); err != nil {
		return CommandOutput{
			Command: command,
			Success: false,
			Output:  "",
		}
	} else {
		return CommandOutput{
			Command: command,
			Success: true,
			Output:  stdout,
		}
	}
}

// Get an informational TestResult for a shell command.
func TestShellCommand(command string) TestResult {
	command = strings.TrimSpace(command)

	// Make the command underscore-delimited
	key := strings.ReplaceAll(command, " ", "_")

	// Make sure quotes won't mess with JSON
	key = strings.ReplaceAll(key, "\"", "'")

	return TestResult{
		Key:           key,
		Title:         command,
		Informational: true,
		Data:          GetCommandOutput(command),
	}
}
