package internal

// See:
// https://stackoverflow.com/a/72367983/11832705
// https://codereview.stackexchange.com/a/79100
const (
	USER_R_PERMS  = 0b100000000
	USER_W_PERMS  = 0b010000000
	USER_X_PERMS  = 0b001000000
	GROUP_R_PERMS = 0b000100000
	GROUP_W_PERMS = 0b000010000
	GROUP_X_PERMS = 0b000001000
	OTHER_R_PERMS = 0b000000100
	OTHER_W_PERMS = 0b000000010
	OTHER_X_PERMS = 0b000000001
)

var (
	Hostname = GetHostname()
)

// Get the basics sorted out ahead of time
var BaseFindings = []TestResult{
	TestShellCommand("uname -a"),
	TestShellCommand("rpm -qa"),
	TestShellCommand("rpm -qai"),
	TestFileContents("/etc/passwd"),
	TestFileContents("/etc/group"),
	TestFileContents("/etc/os-release"),
	TestFileContents("/etc/lsb-release"),
	TestFileContents("/etc/redhat-release"),
	TestFileContents("/etc/debian_version"),
}
