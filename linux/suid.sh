#!/usr/bin/env bash

print_suid_file() {
  printf "Found the follow file with the SUID bit set: %s\n" "${1}"
  stat -L "${1}"
  printf "\n"
}

print_sgid_file() {
  printf "Found the follow file with the SGID bit set: %s\n" "${1}"
  stat -L "${1}"
  printf "\n"
}

print_sugid_file() {
  printf "Found the follow file with both the SUID and SGID bits set: %s\n" "${1}"
  stat -L "${1}"
  printf "\n"
}

find / -perm /4000 -print0 2>/dev/null | xargs -I '{}' print_suid_file '{}'
find / -perm /2000 -print0 2>/dev/null | xargs -I '{}' print_sgid_file '{}'
find / -perm /6000 -print0 2>/dev/null | xargs -I '{}' print_sugid_file '{}'
