#!/usr/bin/env bash

set -e
set -u
set -o pipefail
shopt -s failglob


root_only() {

  if [ "$EUID" -ne 0 ]; then
    echo "Please run as root"
    exit 1
  fi

}

# TODO: journald configuration
# TODO: Better description
#
# Set up user lingering, force evaluation of XDG_RUNTIME_DIR for bash and zsh
# (XDG_RUNTIME_DIR is needed for scripts, 'su', 'sudo', etc. since systemd user service management relies on it)
#
# References/credits:
# - https://serverfault.com/a/1026914
# - https://www.freedesktop.org/software/systemd/man/user@.service.html
# - https://www.freedesktop.org/software/systemd/man/loginctl.html
#
# Args:
# - username
systemd_user_setup() {

  root_only

  local _user
  local _uid
  local _home
  local _xdg_export

  _user="${1:?no username supplied}"
  _uid=$(id -u "~${_user}")
  _home=$(eval echo "~${_user}")

  # Set up user manager - also generates /run/user/UID, i.e. XDG_RUNTIME_DIR
  if ! [ -f "/etc/systemd/system/user@${_uid}.service" ]; then
    cp "$(dirname "${0}")/user@.service" "/etc/systemd/system/user@${_uid}.service"
    systemctl daemon-reload
    systemctl enable "user@${_uid}.service"
    systemctl start "user@${_uid}.service"
  fi

  # Enable user lingering via systemd login manager, i.e. spawn a user manager at boot
  loginctl enable-linger "${_user}"

  # Export XDG_RUNTIME_DIR if needed since I've had issues with 'machinectl shell' in scripts before
  _xdg_export='\n[ -z "${XDG_RUNTIME_DIR}" ] && export XDG_RUNTIME_DIR=$(id -ru)\n'

  declare -a _profiles=("${_home}/.profile" "${_home}/.bash_profile" "${_home}/.zprofile")
  declare -a _rcfiles=("${_home}/.bashrc" "${_home}/.zshrc")

  # Force evaluation for login shells
  for f in "${_profiles[@]}"; do
    if [ -f "${f}" ] && ! grep -q "XDG_RUNTIME_DIR" "${f}"; then
      echo -e "${_xdg_export}" | tee -a "${f}"
    fi
  done

  # Force evaluation for interactive shells
  for f in "${_rcfiles[@]}"; do
    if [ -f "${f}" ] && ! grep -q "XDG_RUNTIME_DIR" "${f}"; then
      echo -e "${_xdg_export}" | tee -a "${f}"
    fi
  done

}
