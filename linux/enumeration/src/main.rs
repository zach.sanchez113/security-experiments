use chrono::{DateTime, Utc};
use regex::Regex;
use serde::{Deserialize, Serialize};
use serde_json::json;
use std::process::Command;
use std::{collections::HashMap, fs, os::unix::prelude::PermissionsExt, path::PathBuf};
use walkdir::WalkDir;

#[derive(Debug, Serialize, Deserialize)]
enum TestResultType {
    Text(String),
    List(Vec<String>),
    FileContents(HashMap<String, String>),
}

#[derive(Debug, Serialize, Deserialize)]
struct TestResult<'a> {
    key: &'a str,
    title: &'a str,
    description: &'a str,
    finding: bool,
    data: TestResultType,
}

/// Whether to keep processing this file, i.e. stop if a junk/useless file (or not a regular file).
fn keep_file(path: &walkdir::DirEntry) -> bool {
    // This is obviously not extensive, but they're the ones I immediately thought of
    let unwanted_root_dirs: Vec<Regex> = Vec::from([
        Regex::new("^/boot").unwrap(),
        Regex::new("^/dev").unwrap(),
        Regex::new("^/proc").unwrap(),
        Regex::new("^/sys").unwrap(),
        Regex::new("\\.git/").unwrap(),
        Regex::new("node_modules/").unwrap(),
        Regex::new("venv/").unwrap(),
    ]);

    let filename: String = path.file_name().to_str().unwrap_or_default().to_owned();

    for dir in unwanted_root_dirs {
        if !path.file_type().is_file() || dir.is_match(&filename) {
            return false;
        }
    }

    return true;
}

/// Check if a path is world-readable.
fn world_readable(path: &PathBuf) -> bool {
    if let Ok(metadata) = fs::metadata(&path) {
        metadata.permissions().mode() & 0o004 != 0
    } else {
        false
    }
}

/// Check if a path is world-writable.
fn world_writable(path: &PathBuf) -> bool {
    if let Ok(metadata) = fs::metadata(&path) {
        metadata.permissions().mode() & 0o002 != 0
    } else {
        false
    }
}

/// Check if a file is executable (by non-owner) and has the SUID bit set
fn suid_executable(path: &PathBuf) -> bool {
    if let Ok(metadata) = fs::metadata(&path) {
        metadata.permissions().mode() & 0o4001 != 0
    } else {
        false
    }
}

/// Check if a file is executable (by non-owner) and has the SGID bit set
fn sgid_executable(path: &PathBuf) -> bool {
    if let Ok(metadata) = fs::metadata(&path) {
        metadata.permissions().mode() & 0o2001 != 0
    } else {
        false
    }
}

/// Returns the contents of a file if possible, else an empty string
fn file_contents(path: &'static str) -> String {
    fs::read_to_string(path).unwrap_or(String::new()).to_owned()
}

/// Wrap this because it's pretty ugly for a one-liner
fn test_result_file(path: &'static str) -> TestResultType {
    TestResultType::FileContents(HashMap::from([(path.to_owned(), file_contents(path))]))
}

fn get_proc_stdout(output: std::process::Output) -> String {
    if let Ok(stdout) = String::from_utf8(output.stdout) {
        stdout
    } else {
        "".to_string()
    }
}

fn test_result_proc_stdout(cmd: &mut std::process::Command) -> TestResultType {
    if let Ok(output) = cmd.output() {
        TestResultType::Text(get_proc_stdout(output))
    } else {
        TestResultType::Text("".to_owned())
    }
}

/// Script to perform common post-exploitation tasks with JSON output.
fn main() {
    let start_time: DateTime<Utc> = Utc::now();

    // Collect world-readable home directories
    let mut world_readable_home_dirs: Vec<String> = Vec::new();

    for entry in fs::read_dir("/home/").unwrap().filter_map(|e| e.ok()) {
        if let Some(metadata) = fs::metadata(&entry.path()).ok() {
            if metadata.permissions().mode() & 0o001 != 0 {
                world_readable_home_dirs.push(entry.path().to_str().unwrap_or_default().to_owned());
            }
        }
    }

    // TODO: This can have file contents or be empty, don't need to differentiate readable vs not
    let mut ssh_private_keys: Vec<String> = Vec::new();
    let mut ssh_private_keys_readable: HashMap<String, String> = HashMap::new();
    let mut ssh_private_keys_world_readable: HashMap<String, String> = HashMap::new();

    // TODO: This can have file contents or be empty, collapse into one HashMap
    let mut ssh_public_keys: Vec<String> = Vec::new();
    let mut ssh_public_keys_readable: HashMap<String, String> = HashMap::new();
    let mut ssh_public_keys_world_readable: HashMap<String, String> = HashMap::new();

    // TODO: This can have file contents or be empty, don't need to differentiate readable vs not
    let mut pem_files: Vec<String> = Vec::new();
    let mut pem_files_readable: HashMap<String, String> = HashMap::new();
    let mut pem_files_world_readable: HashMap<String, String> = HashMap::new();

    for entry in WalkDir::new("/").into_iter().filter_map(|e| e.ok()).filter(|e| keep_file(e)) {
        let path: PathBuf = entry.into_path().to_owned();
        let filename: String = path.to_str().unwrap_or_default().to_owned();
        let file_content = fs::read_to_string(&filename).unwrap_or_default();

        // Look for SSH keys
        if Regex::new("id_[rd]sa").unwrap().is_match(&filename) {
            if filename.contains("pub") {
                ssh_public_keys.push(filename.to_owned());

                if file_content != "" {
                    ssh_private_keys_readable.insert(filename.to_owned(), file_content.to_owned());
                }
                if world_readable(&path) {
                    ssh_private_keys_world_readable.insert(filename.to_owned(), file_content.to_owned());
                }
            } else {
                ssh_private_keys.push(filename.to_owned());

                if file_content != "" {
                    ssh_public_keys_readable.insert(filename.to_owned(), file_content.to_owned());
                }
                if world_readable(&path) {
                    ssh_public_keys_world_readable.insert(filename.to_owned(), file_content.to_owned());
                }
            }
        }

        // Check for PEM files since they *might* be interesting
        if path.extension().unwrap() == "pem" {
            pem_files.push(filename.to_owned());

            if file_content != "" {
                pem_files_readable.insert(filename.to_owned(), file_content.to_owned());
            }
            if world_readable(&path) {
                pem_files_world_readable.insert(filename.to_owned(), file_content.to_owned());
            }
        }
    }

    // TODO: Collapse 'info' and 'issues' into 'results'
    let output = json!({
        "start": format!("{}", start_time),
        "end": format!("{}", Utc::now()),
        // TODO: More /etc/*release files (see: https://serverfault.com/a/1014737)
        // TODO: rpm -qa | sort -u
        /*
            TODO: Add the following:
            - /tmp
            - System time
            - IP addresses/interfaces
            - Listening ports
            - Established connections
            - Running processes
            - All /home dirs
            - Hidden files in world-readable /home dirs
            - Sudo rules
            - [/etc/ssh | .ssh]/authorized_keys
            - Other concerning extensions: .key, .auth, .secret
            - .env files
            - Environment variables
            - Currently logged-on users + what they're doing
            - Mounted filesystems
            - SUID/SGID binaries
        */
        "info": [
            TestResult {
                key: "uname_a",
                title: "uname -a",
                description: "",
                finding: false,
                data: test_result_proc_stdout(&mut Command::new("uname").arg("-a")),
            },
            TestResult {
                key: "rpm_qa",
                title: "rpm -qa",
                description: "",
                finding: false,
                data: test_result_proc_stdout(&mut Command::new("rpm").arg("-qa")),
            },
            TestResult {
                key: "rpm_qai",
                title: "rpm -qai",
                description: "",
                finding: false,
                data: test_result_proc_stdout(&mut Command::new("rpm").arg("-qai")),
            },
            TestResult {
                key: "hostname",
                title: "Hostname",
                description: "",
                finding: false,
                data: TestResultType::Text(hostname::get().unwrap_or_default().into_string().unwrap_or("".to_string())),
            },
            TestResult {
                key: "etc_passwd",
                title: "/etc/passwd",
                description: "",
                finding: false,
                data: test_result_file("/etc/passwd"),
            },
            TestResult {
                key: "etc_group",
                title: "/etc/group",
                description: "",
                finding: false,
                data: test_result_file("/etc/group"),
            },
            TestResult {
                key: "etc_os_release",
                title: "/etc/os-release",
                description: "",
                finding: false,
                data: test_result_file("/etc/os-release"),
            },
            TestResult {
                key: "etc_lsb_release",
                title: "/etc/lsb-release",
                description: "",
                finding: false,
                data: test_result_file("/etc/lsb-release"),
            },
            TestResult {
                key: "etc_redhat_release",
                title: "/etc/redhat-release",
                description: "",
                finding: false,
                data: test_result_file("/etc/redhat-release"),
            },
            TestResult {
                key: "etc_debian_version",
                title: "/etc/debian_version",
                description: "",
                finding: false,
                data: test_result_file("/etc/debian_version"),
            },
            TestResult {
                key: "ssh_public_key",
                title: "SSH Public Keys",
                description: "",
                finding: false,
                data: TestResultType::List(ssh_public_keys),
            },
            TestResult {
                key: "ssh_private_key",
                title: "SSH Private Keys",
                description: "",
                finding: false,
                data: TestResultType::List(ssh_private_keys),
            },
            TestResult {
                key: "pem",
                title: "PEM Files",
                description: "",
                finding: false,
                data: TestResultType::List(pem_files),
            },
        ],
        /*
            TODO: Add the following:
            - Sudo rule for ALL
            - Sudo rules containing wildcard
            - Sudo rules containing just a script (implicit wildcard)
            - Sudo rules containing writable script
            - Sudo rules containing non-existent script
            - World-readable/writable .ssh
            - World-writable [/etc/ssh | .ssh]/authorized_keys
            - .bash_history, .zsh_history, .ksh_history, .mysql_history, .python_history
        */
        "issues": [
            TestResult {
                key: "world_readable_home",
                title: "World-Readable Directories in /home",
                description: "All directories in /home should only be readable by the directory's owner.",
                finding: true,
                data: TestResultType::List(world_readable_home_dirs),
            },
            TestResult {
                key: "world_readable_ssh_private_key",
                title: "World-Readable SSH Private Keys",
                description: "All SSH private keys should only be readable by the file's owner.",
                finding: true,
                data: TestResultType::FileContents(ssh_private_keys_world_readable),
            },
            TestResult {
                key: "world_readable_pem",
                title: "World-Readable PEM Files",
                description: "If this contains a secret, the file should only be readable by the file's owner.",
                finding: true,
                data: TestResultType::FileContents(pem_files_world_readable),
            },
            // TODO: Always mark as finding? Or should this be conditionally be added to report based on blank vs not?
            TestResult {
                key: "etc_shadow",
                title: "/etc/shadow",
                description: "/etc/shadow should only be readable by root.",
                finding: world_readable(&PathBuf::from("/etc/shadow")),
                data: test_result_file("/etc/shadow"),
            },
            TestResult {
                key: "etc_gshadow",
                title: "/etc/gshadow",
                description: "/etc/gshadow should only be readable by root.",
                finding: world_readable(&PathBuf::from("/etc/gshadow")),
                data: test_result_file("/etc/gshadow"),
            },
        ]
    });

    println!("{}", output);

    // For output that's easier for a human to read
    // println!("{:#}", output);
}
