#!/usr/bin/env bash

# Bash strict mode
# See: http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -e
set -u
set -o pipefail
shopt -s failglob


#################################################
#
# Helper for base64 encoding commands and printing
# out the final command to run
#
#################################################

# Encode input as base64
#
# Args:
# - Command to obfuscate
#
obfuscate_base64() {
  printf '%s' "$(echo "${cmd:-$(</dev/stdin)}" | base64)"
}


if [ "$#" -ne 1 ]; then
  echo "Wrong number of params - found $#, expected 1" 1>&2
  exit 1
fi

if [ -f "${1}" ]; then
  cmd=$(cat <"${1}")
else
  cmd="${1}"
fi

printf "\nprintf \x27%s\x27 | base64 --decode | bash\n" "$(obfuscate_base64 "$cmd")"
