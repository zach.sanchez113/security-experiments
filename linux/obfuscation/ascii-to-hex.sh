#!/usr/bin/env bash

# Bash strict mode
# See: http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -e
set -u
set -o pipefail
shopt -s failglob


#################################################
#
# Helper for encoding commands as hex and
# printing out the final command to run
#
#################################################

# Convert ASCII input to hex
#
# Args:
# - Command to obfuscate
#
obfuscate_ascii_hex() {

  declare cmd="${1:-$(</dev/stdin)}"
  declare encoded_cmd=""

  for ((i = 0; i <= ${#cmd}; i++)); do

    declare char="${cmd:$i:1}"
    declare char_octal=$(printf "%02x" "'${char}")

    encoded_cmd="${encoded_cmd}\\x${char_octal}"

  done

  printf '%s' "${encoded_cmd}"

}


if [ "$#" -ne 1 ]; then
  echo "Wrong number of params - found $#, expected 1" 1>&2
  exit 1
fi

if [ -f "${1}" ]; then
  cmd=$(cat <"${1}")
else
  cmd="${1}"
fi

printf "\nprintf \x27%s\x27 | bash\n" "$(obfuscate_ascii_hex "$cmd")"
