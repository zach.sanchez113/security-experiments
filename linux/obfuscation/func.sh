#!/usr/bin/env bash


# Convert ASCII input to hex
#
# Args:
# - Command
#
obfuscate_ascii_hex() {

  declare cmd="${1:-$(</dev/stdin)}"
  declare encoded_cmd=""

  for ((i = 0; i <= ${#cmd}; i++)); do

    declare char="${cmd:$i:1}"
    declare char_octal=$(printf "%02x" "'${char}")

    encoded_cmd="${encoded_cmd}\\x${char_octal}"

  done

  printf '%s' "${encoded_cmd}"

}

# Convert ASCII input to octal
#
# Args:
# - Command
#
obfuscate_ascii_octal() {

  declare cmd="${1:-$(</dev/stdin)}"
  declare encoded_cmd=""

  for ((i = 0; i <= ${#cmd}; i++)); do

    declare char="${cmd:$i:1}"
    declare char_octal=$(printf "%03o" "'${char}")

    encoded_cmd=$(printf '%s' "${encoded_cmd}\\${char_octal}")

  done

  printf '%s' "${encoded_cmd}"

}

# Encode input as base64
#
# Args:
# - Command
#
obfuscate_base64() {
  declare cmd="${1:-$(</dev/stdin)}"
  printf '%s' "$(printf '%s' "${cmd}" | base64)"
}

# Chain multiple techniques together
#
# Args:
# - Command
#
obfuscate_chain() {

  declare cmd="${1:-$(</dev/stdin)}"

  echo "number of chars in cmd: ${#cmd}"

  layer1="printf '$(obfuscate_base64 "${cmd}")' | base64 -d | bash"
  echo "got layer 1. number of chars: ${#layer1}"

  layer2="printf '$(obfuscate_ascii_octal "${layer1}")' | bash"
  echo "got layer 2. number of chars: ${#layer2}"

  layer3="printf '$(obfuscate_ascii_hex "${layer2}")' | bash"
  echo "got layer 3. number of chars: ${#layer3}"

  layer4="printf '$(obfuscate_base64 "${layer3}")' | base64 -d | bash"
  echo "got layer 4. number of chars: ${#layer4}"

  # printf 'layer1: %s\n\n' "${layer1}"
  # printf 'layer2: %s\n\n' "${layer2}"
  # printf 'layer3: %s\n\n' "${layer3}"
  # printf 'layer4: %s\n\n' "${layer4}"

  # printf '%s' "${layer4}" > obfuscate-chain-out.txt

}
