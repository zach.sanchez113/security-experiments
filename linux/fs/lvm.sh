#!/usr/bin/env bash

# Just a bunch of copy/pasting for my own reference/future use
#
# ! It's a good idea to do this during installation, or just use 'system-config-lvm' if available
#
# References:
# - https://www.thegeekdiary.com/redhat-centos-a-beginners-guide-to-lvm-logical-volume-manager/
# - https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/logical_volume_manager_administration/lvm_examples

# Scan available devices
lvmdiskscan

# Create physical volume
pvcreate /dev/sda1

# Display physical volumes
# Output is more verbose with each command
pvs
pvscan
pvdisplay

# Create volume group
vgcreate vg01 /dev/sda1

# Display info about volume groups
vgscan
vgs
vgdisplay

# Disable/enable vg
vgchange -a n vg01
vgchange -a y vg01
# or:
vgchange --activate y vg01

# Create logical volume
lvcreate -L 5G -n lvol01 vg01
# or:
lcreate --size 5G --name lvol01 vg01

# Create striped volume (RAID 0, i.e. spread data across multiple disks)
# Note that -i 3 is the number of physical volumes, which is different in the tutorial I copied from
lvcreate -L 5G -I 4096 -i 3 -n lvol01 vg01
# or:
lvcreate -L 5G --stripesize 4096 --stripes 3 -n lvol01 vg01

# Create mirror volume (RAID 1, i.e. mirror data across multiple disks)
lvcreate -L 1G -m 2 -n lvol01 vg01
# or:
lvcreate -L 5G --mirrors 2 -n lvol01 vg01


# Display LV info
lvscan
lvs
lvdisplay
