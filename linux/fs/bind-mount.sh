#!/usr/bin/env bash

set -euo pipefail

# Reference: https://unix.stackexchange.com/questions/198590/what-is-a-bind-mount

main() {

  _source="${1:?missing source dir}"
  _target="${2:?missing target dir}"

  if ! [ -d $_source ]; then
    mkdir -p $_source
  fi
  if ! [ -d $_target ]; then
    mkdir -p $_target
  fi

  mount --bind $_source $_target

  echo "${_source} ${_target} none bind,ro" >> /etc/fstab

}

main "$@" || exit 1
