#!/usr/bin/env python3

"""
Also see:
https://psutil.readthedocs.io/en/latest/
https://github.com/giampaolo/psutil/tree/master/scripts
https://github.com/giampaolo/psutil/blob/4225d799439525bea5d77935e3a73af1caf27738/psutil/_common.py#L171
"""

from __future__ import annotations

import datetime
import json
import os
import re
import shutil
import subprocess
import sys

import psutil
import rich_click as click
from attrs import define
from fabric import Connection
from loguru import logger
from path import Path
from psutil._common import bytes2human

# Config for rich-click
click.rich_click.SHOW_ARGUMENTS = True
click.rich_click.SHOW_METAVARS_COLUMN = False
click.rich_click.APPEND_METAVARS_HELP = True
click.rich_click.STYLE_OPTIONS_TABLE_PAD_EDGE = True
click.rich_click.COMMAND_GROUPS = {}

# Grab the Rich console
console = click.rich_click._get_rich_console()

# Base logging configuration
LOG_FORMAT = (
    "<green>{time:YYYY-MM-DD HH:mm:ss}</green> | <level>{level: <8}</level> | <level>{message}</level>"
)

logger.configure(
    handlers=[
        dict(sink=sys.stdout, level=os.environ.get("LOGURU_LEVEL", "INFO"), format=LOG_FORMAT),
    ],
    levels=[
        {"name": "INFO", "color": "<blue><bold>"},
        {"name": "DEBUG", "color": "<magenta><bold>"},
    ],
)

# Constants
SUDO_SYSTEMCTL = f"{'sudo ' if os.geteuid() != 0 else ''}{shutil.which('systemctl')}"


@define
class SystemDService:
    name: str

    def _sudo(self, command: str) -> subprocess.CompletedProcess:
        return subprocess.run(f"{'sudo ' if os.geteuid() != 0 else ''}{command}")

    def exists(self) -> bool:
        return subprocess.run(f"systemctl status {self.name}").returncode == 4

    def cat(self, force_color: bool = False) -> str:
        command = f"systemctl cat --no-pager {self.name}"

        if force_color:
            command = f"SYSTEMD_COLORS=1 {command}"

        return subprocess.run(command).stdout.decode()

    def show(self, force_color: bool = False) -> str:
        """NOTE: This is pretty verboses"""
        command = f"systemctl show --no-pager {self.name}"

        if force_color:
            command = f"SYSTEMD_COLORS=1 {command}"

        return subprocess.run(command).stdout.decode()

    def status(self, force_color: bool = False) -> str:
        command = f"systemctl status --no-pager {self.name}"

        if force_color:
            command = f"SYSTEMD_COLORS=1 {command}"

        return subprocess.run(command).stdout.decode()

    def enable(self) -> bool:
        return self._sudo(f"systemctl enable --quiet {self.name}").returncode == 0

    def disable(self) -> bool:
        return self._sudo(f"systemctl disable --quiet {self.name}").returncode == 0

    def start(self) -> bool:
        return self._sudo(f"systemctl start --quiet {self.name}").returncode == 0

    def stop(self) -> bool:
        return self._sudo(f"systemctl stop --quiet {self.name}").returncode == 0

    def restart(self) -> bool:
        return self._sudo(f"systemctl restart --quiet {self.name}").returncode == 0

    def is_enabled(self) -> bool:
        return subprocess.run(f"systemctl is-enabled --quiet {self.name}").returncode == 0

    def is_active(self) -> bool:
        return subprocess.run(f"systemctl is-active --quiet {self.name}").returncode == 0

    def is_failed(self) -> bool:
        return subprocess.run(f"systemctl is-failed --quiet {self.name}").returncode == 0


def psutil_nt_to_dict(nt, convert_bytes: bool = True):
    data = {}

    for k, v in nt._asdict().items():
        if k == "percent":
            data[k] = f"{v}%"
        elif isinstance(v, int) and convert_bytes:
            data[k] = bytes2human(v)
        else:
            data[k] = v

    return data


def systemd_list_units() -> list[dict]:
    res = subprocess.run("systemctl list-units --all --full --plain --no-pager --no-legend")
    lines = res.stdout.decode().split("\n")
    lines = [[part for part in re.split(r"[\s]{2,}", line) if part] for line in lines]

    units = []
    for parts in lines:
        if len(parts) != 5:
            continue

        units.append(
            {
                "unit": parts[0],
                "load": parts[1],
                "active": parts[2],
                "sub": parts[3],
                "description": parts[4],
            }
        )

    return units


def boot_time() -> datetime.datetime:
    return datetime.datetime.fromtimestamp(psutil.boot_time())


def logged_in_users() -> list[dict]:
    logged_in_users = [u._asdict() for u in psutil.users()]

    for u in logged_in_users:
        u["started"] = datetime.datetime.fromtimestamp(u["started"])

    return logged_in_users


# TODO: What can I even return with this...?
def cpu_stats() -> None:
    logger.debug(psutil.cpu_count())
    logger.debug(psutil.cpu_percent(interval=1, percpu=True))


def mem_stats() -> dict:
    return {
        "memory": psutil_nt_to_dict(psutil.virtual_memory()),
        "swap": psutil_nt_to_dict(psutil.swap_memory()),
    }


def disk_usage() -> dict:
    # sdiskpart -> device, mountpoint, fstype, opts, maxfile, maxpath
    # sdiskuage -> total, used, free, percent

    partition_data = {}

    for partition in psutil.disk_partitions(all=False):
        if partition.mountpoint != "/":
            continue
        usage = psutil.disk_usage(partition.mountpoint)
        partition_data[partition.mountpoint] = {
            "device": partition.device,
            **psutil_nt_to_dict(usage),
            "type": partition.fstype,
            "mount": partition.mountpoint,
        }

    return partition_data


def processes() -> dict:
    # ! WARNING: There's a ton of attributes, add them in any real scripts.
    processes = {p.pid: p.as_dict() for p in psutil.process_iter()}

    for pid, proc_data in processes.items():
        for k, v in proc_data.items():
            try:
                processes[pid][k] = v._asdict()
            except:
                pass

    return processes


def nic_info() -> dict:
    net_if_addrs_data = {}

    for k, v in psutil.net_if_addrs().items():
        net_if_addrs_data[k] = [v_._asdict() for v_ in v]

    return net_if_addrs_data


def network_conns(listening: bool = True) -> list[dict]:
    conns = [c._asdict() for c in psutil.net_connections(kind="int")]

    if listening:
        conns = list(filter(lambda c: c["status"] == "LISTEN", conns))

    return conns


@click.command(context_settings=dict(help_option_names=["-h", "--help"]))
@click.option("--debug", is_flag=True, help="Enable debug logging.")
def cli(debug: bool = False):
    """A (very) simple healthcheck script.

    Core idea is to use a config file like this to monitor a system/set of services:

    ```
        [services]
        enable = ["web", "mysqld"]
        disable = ["sqlite"]
        restart_on_failure = ["web"]
        alert_on_failure = ["web"]

        [network.listening]
        all = ["80", "443"]
        local = ["3306"]

        [network.discards]
        warn = ""

        [processes]
        ensure = ["mysqld"]

        [thresholds.cpu]
        warn = ""

        [thresholds.memory]
        warn = 80

        [thresholds.disk]
        warn = {"/var/log" = 80}
    ```
    """

    if debug:
        logger.remove()
        logger.add(sink=sys.stdout, level="DEBUG", format=LOG_FORMAT)

    try:
        pass

    except Exception as e:
        console.print_exception()
        raise click.Abort


if __name__ == "__main__":
    cli()
