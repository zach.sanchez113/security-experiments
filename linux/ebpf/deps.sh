#!/usr/bin/env bash

set -eu


#################################################
#
# On Ubuntu 20.04 (Focal Fossa)
#
# It was surprisingly annoying to figure out how
# to get the headers required for IntelliSense
# when looking at eBPF scripts, so for posterity...
#
#################################################

sudo apt install \
  sysdig-dkms \              # provides tools like bpftool
  linux-tools-$(uname -r) \  # provides some useful headers for this kernel version
  linux-tools-generic \      # allows update to linux-tools when kernel updates
  libbpfcc \                 # actually libbpf
  libbpfcc-dev \             # actually libbpf-dev
  bpfcc-tools \              # BPF compiler collection tools
  bpfcc-lua                  # idk, but lua is eternal

bpftool btf dump file /sys/kernel/btf/vmlinux format c > vmlinux.h
sudo mv vmlinux.h /usr/local/include/


#################################################
#
# Even more steps are needed for dev work...
#
# golang: https://github.com/cilium/ebpf
# python (bcc): https://github.com/iovisor/bcc/blob/master/INSTALL.md
# rust (libbpf-rs): https://github.com/libbpf/libbpf-rs
# rust (redbpf): https://github.com/foniod/redbpf#install
# rust (aya): https://github.com/aya-rs/aya
#
# Leaning towards aya since it's written ground-up
# in rust and focuses on compile once, run
# everywhere. Plus the tutorial is nice.
#
#################################################
