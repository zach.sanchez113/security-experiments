#!/usr/bin/env bash

set -eu


#################################################
#
# References:
#
# https://lwn.net/Articles/660331/ (oh, the irony)
# https://www.suse.com/support/kb/doc/?id=000020545
# https://access.redhat.com/security/cve/cve-2021-33624
#
#
#################################################

val=$(sysctl -n kernel.unprivileged_bpf_disabled)

if [[ $val == 0 ]]; then
  echo "Unprivileged eBPF is enabled."
elif [[ $val == 1 ]]; then
  echo "Privileged eBPF is disabled and cannot be cleared until reboot."
elif [[ $val == 2 ]]; then
  echo "Privileged eBPF is disabled but can be changed at runtime."
else
  echo "Unknown value ${val} found for 'kernel.unprivileged_bpf_disabled'."
fi
