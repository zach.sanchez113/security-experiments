#!/usr/bin/env bash

###########################################################################
#
# Check for sudo rules and potential issues.
#
# In particular, this function will:
#
# - Output all sudo rules
# - Detect unrestricted sudo
# - Detect rules for simpler GTFOBins
# - Detect rules for writable scripts
# - Detect rules for non-existent scripts (persistence!)
#
# ! Do note that this is meant for quick checks - only sudo rules beginning
# ! with '/' will be checked.
# ! The rest is easier to analyze with JSON output from `visudo -x` or
# ! `cvtsudoers` (I just haven't scripted it out yet).
#
###########################################################################

if [ -n "$BASH_VERSION" ] && [[ "$BASH_VERSION" =~ [4-9]* ]]; then
  sudo_output=$(sudo -ll | awk '{ $1=$1; print }' | grep -E "^/")
  readarray -t sudo_rules <<< "$sudo_output"

  for sudo_rule in "${sudo_rules[@]}"; do
    sudo_script=$(echo "${sudo_rule}" | awk -F ' ' '{ print $1 }')

    if ! [ -f "${sudo_script}" ]; then
      echo "Sudo rule for non-existent binary: ${sudo_rule}"

    elif echo "${sudo_rule}" | grep -qE "journalctl|rpm|yum|dnf|/su"; then
      echo "Sudo rule for GTFOBin: ${sudo_rule}"

    elif [ -w "${sudo_script}" ]; then
      echo "Sudo rule for writable binary: ${sudo_rule}"

    elif [[ "${sudo_script}" = "ALL" ]]; then
      echo "$(id -un) can execute everything with sudo!"

    else
      echo "Sudo rule (unknown exploitability): ${sudo_rule}"

    fi

  done

else
  echo "Looks like your bash version is too low for this script - need >= 4.x, found ${BASH_VERSION:-''}."

fi
