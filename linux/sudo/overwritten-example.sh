#!/bin/bash

####################################################################################################
#
# Example of what overwritten.sh will produce because syntax highlighting is nice to have
#
####################################################################################################

__original_script="/var/tmp/restart-apache.sh.bak"
__hijacked=0
__script_to_exec=""

# getopts haxx for extra portability
# Source: https://stackoverflow.com/a/7680682/11832705
__optspec=":hv-:"
while getopts "$__optspec" optchar; do
  case "${optchar}" in
  -)
    case "${OPTARG}" in
    hijack)
      __hijacked=1
      __script_to_exec="${!OPTIND}"
      $__script_to_exec
      exit 0
      ;;

    # Skip if this is not us
    *)
      :
      ;;
    esac
    ;;

  # Skip if this is not us
  *)
    :
    ;;
  esac
done


# Execute the original script if we didn't hijack control flow
# As a safeguard, wrap execution in an if statement
if [[ $__hijacked -eq 0 ]]; then
  $__original_script "$@"
fi

# Unset everything in case the script was sourced for some reason
unset __script_to_exec
unset __hijacked
unset __original_script
