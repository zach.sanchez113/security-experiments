package main

import (
	"fmt"
	"io/fs"
	"log"
	"os"
	"os/exec"
	"path"
	"runtime"
	"slices"
	"strings"
)

// See:
// https://stackoverflow.com/a/72367983/11832705
// https://codereview.stackexchange.com/a/79100
const (
	USER_W_PERMS  = 0b010000000
	GROUP_W_PERMS = 0b000010000
	OTHER_W_PERMS = 0b000000010
)

// Convert a slice of bytes to string, strip whitespace, and split by newline.
func SplitLinesBytes(input []byte) []string {
	return strings.Split(strings.TrimSpace(string(input[:])), "\n")
}

// Remove duplicates from a slice of strings.
func RemoveDuplicateStr(strSlice []string) []string {

	allKeys := make(map[string]bool)
	list := []string{}

	for _, item := range strSlice {
		if _, value := allKeys[item]; !value {
			allKeys[item] = true
			list = append(list, item)
		}
	}

	return list

}

// Get a list of users on the system by enumerating /etc/passwd and /home/
func GetUsers() []string {

	var users []string

	dirs, err := os.ReadDir("/home/")

	if err != nil {
		fmt.Fprintf(os.Stderr, "error: %v\n", err.Error())
		os.Exit(1)
	}

	for _, dir := range dirs {
		users = append(users, path.Base(dir.Name()))
	}

	passwd, err := os.ReadFile("/etc/passwd")

	if err != nil {
		fmt.Fprintf(os.Stderr, "error: %v\n", err.Error())
		os.Exit(1)
	}

	for _, line := range SplitLinesBytes(passwd) {
		username := strings.Split(line, ":")[0]

		if !slices.Contains(users, username) {
			users = append(users, strings.Split(line, ":")[0])
		}
	}

	return RemoveDuplicateStr(users)

}

// Get sudo rules for a user.
func GetSudoRules(username string) []string {

	cmd := exec.Command("sudo", "-ll", "-U", username)
	stdout, err := cmd.Output()

	if err != nil {
		fmt.Fprintf(os.Stderr, "error: %v\n", err.Error())
		os.Exit(1)
	}

	var sudoRules []string
	inCommandBlock := false

	for _, line := range SplitLinesBytes(stdout) {
		line := strings.TrimSpace(line)

		if line == "Commands:" {
			inCommandBlock = true
			continue
		}

		if inCommandBlock {
			if line != "" && !slices.Contains(sudoRules, line) {
				sudoRules = append(sudoRules, line)
			} else {
				inCommandBlock = false
			}
		}
	}

	return sudoRules

}

// Check if a file exists.
func FileExists(filename string) bool {
	if _, err := os.Stat(filename); err == nil {
		return true
	} else {
		return false
	}
}

// Get a file's mode.
func GetFileMode(filename string) fs.FileMode {

	info, err := os.Stat(filename)

	if err != nil {
		fmt.Fprintf(os.Stderr, "error: %v\n", err.Error())
		os.Exit(1)
	}

	return info.Mode()

}

// Check if a file is writable by the owner.
func FileWritableByOwner(filename string) bool {
	return GetFileMode(filename)&USER_W_PERMS != 0
}

// Check if a file is writable by the group.
func FileWritableByGroup(filename string) bool {
	return GetFileMode(filename)&GROUP_W_PERMS != 0
}

// Check if a file is writable by others.
func FileWritableByOthers(filename string) bool {
	return GetFileMode(filename)&OTHER_W_PERMS != 0
}

func main() {

	if runtime.GOOS != "linux" {
		log.Fatal("This script can only be run on Linux!")
	}

	for _, username := range GetUsers() {
		for _, sudoRule := range GetSudoRules(username) {

			fmt.Printf("Current sudo rule: %s\n", sudoRule)

			if sudoRule == "ALL" {
				fmt.Printf("%s has a sudo rule for ALL", username)
			}

			// This is fine
			if sudoRule == "sudoedit" {
				continue
			}

			if !FileExists(sudoRule) {
				fmt.Printf("Sudo rule binary/script isn't a file: %s\n", sudoRule)
			} else {
				if FileWritableByOwner(sudoRule) {
					fmt.Printf("Sudo rule binary/script is writable by its owner: %s\n", sudoRule)
				}
				if FileWritableByGroup(sudoRule) {
					fmt.Printf("Sudo rule binary/script is writable by its group: %s\n", sudoRule)
				}
				if FileWritableByOthers(sudoRule) {
					fmt.Printf("Sudo rule binary/script is writable by others: %s\n", sudoRule)
				}
			}

		}
	}

}
