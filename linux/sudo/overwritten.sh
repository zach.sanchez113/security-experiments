#!/usr/bin/env bash

####################################################################################################
#
# Safely hijack the execution flow of a writable script in sudo rules.
#
# To use the resulting script, just run `sudo /path/to/script --hijack $SCRIPT_NAME`, where
# $SCRIPT_NAME is the script that should be executed as root (or the target user, depending on the
# sudo rule).
#
# TODOs:
#   - Fix the optspec (don't need -h or -v)
#   - Check if the script is being sourced
#
####################################################################################################

set -e
set -u

target_file="${1:?no target file was provided}"

# Sanity check
if ! [ -f "${target_file}" ]; then
  echo "${target_file} doesn't exist!" >&2
  exit 1
fi

# Now let's determine where we can write to
# Use the original directory if at all possible
if [ -w "$(dirname "${target_file}")" ]; then
  bak_file="${target_file}.bak"

# /var/tmp if the original directory isn't writable
elif [ -w /var/tmp ]; then
  bak_file="/var/tmp/$(basename "${target_file}").bak"

# /tmp as a last resort since that'll disappear if a reboot happens for any reason
else
  bak_file="/tmp/$(basename "${target_file}").bak"

fi

# Make sure we haven't done this already since it wouldn't be super cool to nuke a file that's
# likely important to keep intact
if [ -f "${bak_file}" ]; then
  echo "${bak_file} already exists!" >&2
  exit 1
fi

# "Back up" the target file so that we can call it from our new one
if ! cp "${target_file}" "${bak_file}"; then
  echo "Failed to copy ${target_file} to ${bak_file}!" >&2
  exit 1
fi

# Handle the shebang + `__original_script` variable first so we can avoid expanding variables in the
# heredoc
echo -e "$(head -n 1 "${bak_file}")\n\n__original_script=\"${bak_file}\"\n" >"${target_file}"

# Add the rest of the script
cat >"${target_file}" <<"EOF"
__hijacked=0
__script_to_exec=""

# getopts haxx for extra portability
# Source: https://stackoverflow.com/a/7680682/11832705
optspec=":hv-:"
while getopts "$optspec" optchar; do
  case "${optchar}" in
  -)
    case "${OPTARG}" in
    hijack)
      __hijacked=1
      __script_to_exec="${!OPTIND}"
      $__script_to_exec
      exit 0
      ;;

    # Skip if this is not us
    *)
      :
      ;;
    esac
    ;;

  # Skip if this is not us
  *)
    :
    ;;
  esac
done


# Execute the original script if we didn't hijack control flow
# As a safeguard, wrap execution in an if statement
if [[ $__hijacked -eq 0 ]]; then
  $__original_script "$@"
fi

# Unset everything in case the script was sourced for some reason
unset __script_to_exec
unset __hijacked
unset __original_script

EOF
