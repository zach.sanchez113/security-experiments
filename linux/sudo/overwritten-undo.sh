#!/usr/bin/env bash

####################################################################################################
#
# Undo anything done by overwritten.sh since that's not awesome to keep around (especially if the
# backup file ended up going to /tmp!)
#
####################################################################################################

set -e
set -u

target_file="${1:?no target file was provided}"

# Sanity check
if ! [ -f "${target_file}" ]; then
  echo "${target_file} doesn't exist!" >&2
  exit 1
fi

# Use the original directory if at all possible
if [ -w "$(dirname "${target_file}")" ]; then
  bak_file="${target_file}.bak"

# /var/tmp if the original directory isn't writable
elif [ -w /var/tmp ]; then
  bak_file="/var/tmp/$(basename "${target_file}").bak"

# /tmp as a last resort since that'll disappear if a reboot happens for any reason
else
  bak_file="/tmp/$(basename "${target_file}").bak"

fi

# Sanity check
if ! [ -f "${bak_file}" ]; then
  echo "${bak_file} doesn't exist!" >&2
  exit 1
fi

# Revert the target file to its original state
if ! cp "${bak_file}" "${target_file}"; then
  echo "Failed to copy ${bak_file} to ${target_file}!" >&2
  exit 1
fi
