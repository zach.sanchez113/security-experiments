#!/usr/bin/env bash

set -euo pipefail

main() {

  _WRITER_GROUP="${1:?missing writer group}"
  _READER_GROUP="${2:?missing reader group}"

  # Create SFTP user/group
  useradd "${_WRITER_GROUP}" --user-group --shell /sbin/nologin

  # Create SSH authorized keys dir + file for SFTP service account specifically
  mkdir -p /etc/ssh/authorized_keys
  chmod 755 /etc/ssh/authorized_keys
  touch /etc/ssh/authorized_keys/"${_WRITER_GROUP}"
  chmod a+r /etc/ssh/authorized_keys/"${_WRITER_GROUP}"

  # Create chroot dir w/ the required ownership + perms for the chroot parent dirs
  # (Don't bother w/ 'install' since it doesn't fix preexisting perms)
  mkdir -p /srv/sftp/chroot/
  chown -R root:root /srv/
  chmod -R 0755 /srv/

  # setgid so that uploads inherit group <service account> (read access is needed for script)
  # Owner (root) does whatever it wants anyway
  # Read-only for group (no need to write, so let's not)
  # Write-only for others (read: the SFTP service account shouldn't even be allowed to read anything)
  # (NOTE: don't test with file perms >=555 - it's still possible to read files if the filename is known in that case)
  declare -a _DROPZONE_DIRS=(
    "dropzone1"
    "dropzone2"
  )

  for _DIR in "${_DROPZONE_DIRS[@]}"; do
    install --directory --mode=2753 --owner=root --group="${_READER_GROUP}" "/srv/sftp/chroot/${_DIR}"
  done

}

main "$@" || exit 1
