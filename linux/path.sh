#!/usr/bin/env bash

# Credit for this function: https://unix.stackexchange.com/a/60823
lspath () {
  shopt -s extglob

  local IFS pattern
  IFS='|'
  pattern="*@($*)*"
  IFS=':'
  for d in $PATH; do
    for x in "$d/"$pattern; do
      [ "$x" = "$d/$pattern" ] || echo "${x##*/}"
    done
  done | sort -u

  shopt -u extglob
}
