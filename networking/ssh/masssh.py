#!/usr/bin/env python3

"""Experiments in parallel-ssh"""

from __future__ import annotations

import logging
import os
import re
import sys
import traceback
from textwrap import dedent
from typing import Any, Optional, cast
from uuid import uuid4

import arrow
import pssh.output
import rich_click as click
from attrs import Factory, define
from gevent import joinall
from loguru import logger
from path import Path
from pssh.clients import ParallelSSHClient
from pyrsistent import m

# Config for rich-click
click.rich_click.STYLE_HELPTEXT = ""
click.rich_click.SHOW_ARGUMENTS = True
click.rich_click.SHOW_METAVARS_COLUMN = False
click.rich_click.STYLE_OPTIONS_TABLE_PAD_EDGE = True

# Grab the Rich console
console = click.rich_click._get_rich_console()

# Default log message format (multiline to make it more readable)
LOG_FORMAT_DEFAULT = """
    <green>{time:YYYY-MM-DD HH:mm:ss}</green> | <level>{level: <8}</level>
    | <cyan>{name}</cyan>:<cyan>{function}</cyan>:<cyan>{line}</cyan> - <level>{message}</level>
"""

LOG_FORMAT = re.sub(r"\n", "", dedent(LOG_FORMAT_DEFAULT))

LOGURU_DEFAULT_KWARGS = m(
    format=LOG_FORMAT,
    colorize=None,
    backtrace=False,
    diagnose=False,
    enqueue=True,
    catch=True,
)

LOGURU_FILE_KWARGS = m(
    rotation="50MB",
    retention=5,
    compression="gz",
)


def env_bool(key: str, default: bool = False) -> bool:
    """Check if an environment variable is set to a truth-like value.

    Specifically, if the value is in ["1", "true", "yes"], then it's considered to be true. Anything
    else is assumed to be false.

    Args:
        key (str): Environment variable name.
        default (bool, optional): Fallback if the environment variable isn't found. Defaults to False.

    Returns:
        bool: Whether the key is set to a truth-like value.
    """

    if not (value := os.environ.get(key)):
        return default

    value = value.lower()

    if value in ["1", "true", "yes"]:
        return True
    else:
        return False


def in_jupyter() -> bool:
    """Are we running in a Jupyter notebook?

    Credit: https://stackoverflow.com/a/39662359/11832705

    Returns:
        bool: Whether the code is being run in a Jupyter notebook.
    """

    try:
        shell = get_ipython().__class__.__name__  # pyright: ignore[reportUndefinedVariable]
        if shell == "ZMQInteractiveShell":
            return True
    except Exception:
        ...

    return False


class InterceptHandler(logging.Handler):
    """Redirect all stdlib logging to loguru."""

    def emit(self, record: logging.LogRecord):
        # Get corresponding Loguru level if it exists
        try:
            level = logger.level(record.levelname).name
        except ValueError:
            level = record.levelno

        # Find caller from where originated the logged message
        frame, depth = sys._getframe(6), 6
        while frame and frame.f_code.co_filename == logging.__file__:
            frame = frame.f_back
            depth += 1

        logger.opt(depth=depth, exception=record.exc_info).log(level, record.getMessage())


def configure_logging(
    log_level: str = "",
    log_file: Path | None = None,
    intercept_stdlib_logging: bool = False,
) -> None:
    """Configure logging with loguru.

    In order, log level/file is determined by:

    1. Function argument
    2. Environment variable
    3. Configuration file (you need to add this one yourself!)

    If log level isn't found, a default of `INFO` is used.
    If log file isn't found, log messages will only be printed to stdout.

    NOTE: Adjust this to your codebase as needed. This is pure boilerplate.

    Args:
        log_level (str): Log level. Defaults to INFO.
        log_file (Path | None): Log file to use if desired. Defaults to None.
        intercept_stdlib_logging (bool, optional): Redirect standard library logging to loguru. Defaults to False.
    """

    loguru_kwargs = LOGURU_DEFAULT_KWARGS

    if log_level:
        loguru_kwargs.set("level", log_level)
    # Check if environment variables contain log level if not specifed
    else:
        if os.environ.get("LOGURU_LEVEL"):
            loguru_kwargs = loguru_kwargs.set("level", os.environ["LOGURU_LEVEL"].upper())
        else:
            loguru_kwargs = loguru_kwargs.set("level", "INFO")

    # Check if environment variables contain a log file if not specifed
    if not log_file:
        if os.environ.get("LOGURU_LOG_FILE"):
            log_file = Path(os.environ["LOGURU_LOG_FILE"])

    # Local variables may be desired in non-production environments
    loguru_kwargs = loguru_kwargs.set("diagnose", env_bool("LOGURU_DIAGNOSE", default=False))

    # Passing log messages through a multiprocessing-safe queue is generally desireable, but not
    # always, e.g. when running under pytest
    loguru_kwargs = loguru_kwargs.set("enqueue", env_bool("LOGURU_ENQUEUE", default=True))

    # Forcibly enable color and deinitialize colorama if in Jupyter since it's not a TTY, but colors
    # are still nice in that case
    #
    # Also set enqueue to False since it can mess with the output
    if in_jupyter():
        try:
            import colorama  # pyright: ignore

            colorama.deinit()
            loguru_kwargs = loguru_kwargs.set("colorize", True).set("enqueue", False)

        except ImportError:
            ...

    handlers: list[dict[str, Any]] = [
        dict(sink=sys.stdout, **loguru_kwargs),
    ]

    if log_file:
        handlers.append(dict(sink=log_file.expand().abspath(), **loguru_kwargs, **LOGURU_FILE_KWARGS))

    # Clear existing loggers
    logger.remove()

    # Create handlers + custom coloring
    logger.configure(
        handlers=handlers,
        levels=[
            {"name": "INFO", "color": "<blue><bold>"},
            {"name": "DEBUG", "color": "<magenta><bold>"},
        ],
    )

    if intercept_stdlib_logging:
        logging.basicConfig(handlers=[InterceptHandler()], level=0)


@define
class ScopeManager:
    """Manager that standardizes all of the files required to test multiple combinations of hosts, SSH keys,
    and users.
    """

    base_directory: Path
    keyfile: Path
    user: str
    _scope: list[str] = Factory(list)

    def __attrs_post_init__(self):
        self.base_directory = self.base_directory.expand().abspath().makedirs_p()
        self.keyfile = self.keyfile.expand().abspath()

    @property
    def global_scope_file(self) -> Path:
        """Newline-delimited file containing all hostnames and/or IP addresses in scope for this test."""
        return (self.base_directory / "scope.txt").expand()

    @property
    def global_scope(self) -> list[str]:
        """All hostnames and/or IP addresses in scope for this test."""
        return self.global_scope_file.lines(retain=False)

    @property
    def output_directory(self) -> Path:
        """Output directory for this SSH key."""
        return (self.base_directory / self.keyfile.basename() / self.user).expand()

    @property
    def scope_file(self) -> Path:
        """Newline-delimited file containing hostnames and/or IP addresses that have been logged in to
        before with the given SSH key and user.
        """
        return (self.output_directory / "scope.txt").expand()

    @property
    def scope(self) -> list[str]:
        """Hostnames and/or IP addresses that can be logged in to with the given SSH key and user."""
        self._scope = self.scope_file.lines(retain=False) if self.scope_file.exists() else []
        return self._scope

    def update_scope(self, hosts: list[str]) -> None:
        """Update the scope of hosts that can be logged in to."""
        self._scope = sorted(set(self._scope + hosts))
        Path(self.scope_file).dirname().makedirs_p()
        Path(self.scope_file).write_lines(self._scope)

    def host_log(self, host: str) -> Path:
        """Get the log file for a given host."""
        return (self.output_directory / f"{host}.log").expand()

    def write_host_log(self, host: str, script: Path, output: str) -> None:
        """Append to the log file for a given host.

        Args:
            host (str): Hostname.
            script (Path): Script that was run.
            output (str): Output of the script.
        """

        if not (host_log := self.host_log(host)).exists():
            host_log.touch()

        script_name = script.basename()
        now = arrow.utcnow().floor("microsecond")
        header = f"{script_name} - {now}"

        log_header = f"""
            {header}
            {'=' * len(header)}


        """

        logger.debug(f"Writing script output to {host_log}...")
        host_log.write_text(dedent(log_header).lstrip("\n") + output + "\n", append=True)


class LoguruCatchGroup(click.RichGroup):
    """Subclass of RichGroup that wraps commands in loguru's `logger.catch` context manager."""

    def __call__(self, *args: Any, **kwargs: Any):
        with logger.catch(onerror=lambda e: sys.exit(1), exclude=(click.ClickException, click.Abort)):
            return super().__call__(*args, **kwargs)


@click.group(cls=LoguruCatchGroup, context_settings=dict(help_option_names=["-h", "--help"]))
def cli():
    """Test SSH access and/or run a script against multiple hosts in parallel.

    \b
    NOTE: A base directory will be used for structuring output to simplify scope/output management.

    The list of targets should be inside of `$BASE_DIRECTORY/scope.txt`, login results will be outputted
    to `$BASE_DIRECTORY/$KEY_NAME/targets.txt`, and script output will be outputted to `$BASE_DIRECTORY/$SCRIPT_NAME/$TARGET.log`
    """

    configure_logging(log_level="INFO", intercept_stdlib_logging=False)


@cli.command(
    no_args_is_help=True,
    context_settings=dict(
        show_default=True,
        help_option_names=["-h", "--help"],
    ),
)
@click.option(
    "-b",
    "--base-directory",
    metavar="PATH",
    default=Path("./").expand().abspath(),
    type=click.Path(exists=True, file_okay=False, path_type=Path),
    help="Base directory for structuring output (see `masssh.py --help`).",
)
@click.option(
    "-k",
    "--keyfile",
    required=True,
    type=click.Path(exists=True, dir_okay=False, path_type=Path),
    help="Location of the public key to test.",
)
@click.option(
    "-u",
    "--user",
    required=True,
    type=str,
    help="User to log in as.",
)
@click.option(
    "-p",
    "--port",
    default=22,
    type=int,
    help="Port to connect to.",
)
@click.option(
    "-t",
    "--timeout",
    default=5,
    type=int,
    help="Number of seconds for timeout.",
)
@click.option(
    "--pool-size",
    default=100,
    type=int,
    help="How many hosts to execute on in parallel.",
)
@click.option(
    "--debug",
    is_flag=True,
    help="Enable debug logging.",
)
def test_key(
    base_directory: Path = Path("./").expand().abspath(),  # pyright: ignore[reportCallInDefaultInitializer]
    keyfile: Path | None = None,
    user: str = "",
    port: int = 22,
    timeout: int = 60,
    pool_size: int = 100,
    debug: bool = False,
):
    """Test an SSH key against multiple hosts."""

    # For the type checker (-k is required)
    assert keyfile

    if debug:
        configure_logging(log_level="DEBUG", intercept_stdlib_logging=False)

    logger.debug(f"{base_directory = }")
    logger.debug(f"{keyfile = }")
    logger.debug(f"{user = }")
    logger.debug(f"{port = }")
    logger.debug(f"{timeout = }")
    logger.debug(f"{pool_size = }")

    manager = ScopeManager(base_directory=base_directory, keyfile=keyfile, user=user)

    if not manager.global_scope_file.exists():
        raise click.ClickException(f"Please place your scope.txt in {manager.base_directory}.")
    elif not len(manager.global_scope):
        raise click.ClickException(f"No hosts are defined in {manager.global_scope_file}.")

    client = ParallelSSHClient(
        hosts=manager.global_scope,
        user=user,
        port=port,
        pkey=manager.keyfile,
        timeout=timeout,
        pool_size=pool_size,
        allow_agent=False,
        identity_auth=False,
    )

    logger.info(f"Testing login to {len(manager.global_scope):,} hosts with {manager.keyfile} as {user}.")
    logger.info(f"Running `whoami` across all hosts...")

    output: list[pssh.output.HostOutput] = client.run_command(
        "whoami",
        stop_on_errors=False,
    )

    client.join(output)

    successes: list[str] = [host_output.host for host_output in output if host_output.exit_code == 0]
    failures: list[str] = [host_output.host for host_output in output if host_output.exit_code != 0]

    logger.success(
        f"Done! Login was successful for {len(successes):,} out of {len(manager.global_scope):,} hosts."
    )

    if len(manager.scope):
        logger.info(f"Found {len(manager.scope)} pre-existing targets for this keyfile.")

    logger.info(f"Writing targets to {manager.scope_file}...")
    manager.update_scope(successes)


# TODO: Split output by script name and use the current time (or 'latest.log') as the filename
# TODO: Allow overwriting latest results
# TODO: Option to compile shell scripts with shc (provides rc4 as well)
@cli.command(
    no_args_is_help=True,
    context_settings=dict(
        show_default=True,
        help_option_names=["-h", "--help"],
    ),
)
@click.argument(
    "script",
    type=click.Path(exists=True, dir_okay=False, path_type=Path),
)
@click.option(
    "-e",
    "--extra-file",
    "extra_files",
    metavar="PATH",
    type=click.Path(exists=True, dir_okay=False, path_type=Path),
    multiple=True,
    help="Extra file(s) to transfer to /tmp/ on the remote hosts."
    + " For example, this could be a script that will be executed from the main script.",
)
@click.option(
    "-b",
    "--base-directory",
    metavar="PATH",
    default=Path("./").expand().abspath(),
    type=click.Path(exists=True, file_okay=False, path_type=Path),
    help="Base directory for structuring output (see `masssh.py --help`).",
)
@click.option(
    "-k",
    "--keyfile",
    required=True,
    type=click.Path(exists=True, dir_okay=False, path_type=Path),
    help="Location of the public key to test.",
)
@click.option(
    "-u",
    "--user",
    required=True,
    type=str,
    help="User to log in as.",
)
@click.option(
    "-p",
    "--port",
    default=22,
    type=int,
    help="Port to connect to.",
)
@click.option(
    "-t",
    "--timeout",
    default=5,
    type=int,
    help="Number of seconds for timeout.",
)
@click.option(
    "--pool-size",
    default=100,
    type=int,
    help="How many hosts to execute on in parallel.",
)
@click.option(
    "--encoding",
    default="utf-8",
    type=str,
    help="Encoding to use when executing commands and decoding output. (Defaults to UTF-8, but this doesn't always work.)",
)
@click.option(
    "--debug",
    is_flag=True,
    help="Enable debug logging.",
)
def run(
    script: Path,
    extra_files: list[Path],
    base_directory: Path = Path("./").expand().abspath(),  # pyright: ignore[reportCallInDefaultInitializer]
    keyfile: Path | None = None,
    user: str = "",
    port: int = 22,
    timeout: int = 60,
    pool_size: int = 100,
    encoding: str = "utf-8",
    debug: bool = False,
):
    """Run a script against multiple hosts.

    Note that this currently only support Linux hosts.
    """

    # For the type checker (-k is required)
    assert keyfile

    if debug:
        configure_logging(log_level="INFO", intercept_stdlib_logging=False)

    if not extra_files:
        extra_files = []

    script = script.expand().abspath()
    extra_files = [f.expand().abspath() for f in extra_files]

    logger.debug(f"{script = }")
    logger.debug(f"{extra_files = }")
    logger.debug(f"{base_directory = }")
    logger.debug(f"{keyfile = }")
    logger.debug(f"{user = }")
    logger.debug(f"{port = }")
    logger.debug(f"{timeout = }")
    logger.debug(f"{pool_size = }")

    manager = ScopeManager(base_directory=base_directory, keyfile=keyfile, user=user)

    if not manager.scope_file.exists():
        raise click.ClickException(
            f"Scope file {manager.scope_file} doesn't exist. Please run `masssh.py test-pubkey` first."
        )
    elif not manager.scope:
        raise click.ClickException(f"No hosts are defined in {manager.scope_file}.")

    client = ParallelSSHClient(
        hosts=manager.scope,
        user=user,
        port=port,
        pkey=manager.keyfile,
        timeout=timeout,
        pool_size=pool_size,
        allow_agent=False,
        identity_auth=False,
    )

    logger.info(f"Filtering out any Windows hosts...")

    output: list[pssh.output.HostOutput] = client.run_command(
        "uname",
        stop_on_errors=False,
        encoding=encoding,
    )

    client.join(output)

    linux_hosts: list[str] = [
        host_output.host
        for host_output in output
        if host_output.exit_code == 0 and not list(host_output.stderr or [])
    ]

    for host_out in output:
        if host_out.exit_code != 0:
            logger.debug(f"{host_out = }")

    logger.info(f"Found {len(linux_hosts):,} Linux hosts out of {len(manager.scope):,}. Excluding the rest.")

    if not linux_hosts:
        raise click.ClickException("No Linux hosts were found. Aborting.")

    client = ParallelSSHClient(
        hosts=linux_hosts,
        user=user,
        port=port,
        pkey=manager.keyfile,
        timeout=timeout,
        pool_size=pool_size,
        allow_agent=False,
        identity_auth=False,
    )

    remote_file = f"/tmp/.{uuid4()}"

    logger.info(f"Copying {script} to {remote_file} across all hosts...")

    cmds = client.copy_file(local_file=str(script), remote_file=remote_file)
    joinall(cmds)

    # Need to keep track of this for cleanup later
    extra_files_remote = []

    for f in extra_files:
        tmp_remote_file = Path("/tmp") / f.basename()

        logger.info(f"Copying {f} to {tmp_remote_file} across all hosts...")
        cmds = client.copy_file(local_file=str(f), remote_file=str(tmp_remote_file))
        joinall(cmds)

        extra_files_remote += tmp_remote_file

    logger.info(f"Running {remote_file} across all hosts...")

    remote_command = f"test -f {remote_file}"
    remote_command += f" && chown $(id -un):$(id -gn) {remote_file}"
    remote_command += f" && chmod 700 {remote_file}"
    remote_command += f" && {remote_file} 2>&1;"

    # Remove files that need to be removed
    remote_command += f" rm -f {remote_file} {' '.join(extra_files_remote)};"

    logger.debug(f"{remote_command = }")

    output: list[pssh.output.HostOutput] = client.run_command(
        remote_command,
        stop_on_errors=False,
        encoding=encoding,
    )

    client.join(output)

    successes: list[str] = [host_output.host for host_output in output if host_output.exit_code == 0]
    failures: list[str] = [host_output.host for host_output in output if host_output.exit_code != 0]

    logger.success(f"Done! Login was successful for {len(successes):,} out of {len(manager.scope):,} hosts.")

    for host_output in output:
        try:
            if host_output.exit_code == 0:
                host: str = host_output.host

                logger.debug(f"Successfully executed script on {host}")

                script_output = ""
                stdout = "\n".join(line for line in list(host_output.stdout or []))
                stderr = "\n".join(line for line in list(host_output.stderr or []))

                if stdout:
                    stdout_header = f"""
                        stdout
                        ======

                    """

                    script_output += dedent(stdout_header).lstrip() + stdout.lstrip("\n") + "\n\n"

                if stderr:
                    stderr_header = f"""
                        stderr
                        ======

                    """

                    script_output += dedent(stderr_header).lstrip() + stderr.lstrip("\n") + "\n\n"

                if not stdout and not stderr:
                    logger.warning(f"Received no output from host {host}.")
                    logger.debug(f"{host_output = }")
                    if host_output.exception:
                        logger.warning(
                            f"Found an exception even though script ran on {host}. Exception: {host_output.exception}"
                        )

                manager.write_host_log(host, script, script_output)

            else:
                host: str = host_output.host
                exception: Exception | None = cast(Optional[Exception], host_output.exception)

                logger.debug(f"Failed to execute script on {host}")
                logger.debug(f"{host_output = }")

                if isinstance(exception, Exception):
                    exception_msg = "".join(traceback.format_tb(exception.__traceback__))
                else:
                    exception_msg = "Unknown error"

                manager.write_host_log(host, script, exception_msg)

        except UnicodeDecodeError as e:
            logger.error(f"Skipping {host_output.host}. Failed to decode output: {e}")


if __name__ == "__main__":
    cli()
