#!/usr/bin/env bash

cat  "$1"    |
    awk '{ print $2 }' |  # Only the actual key data without prefix or comments
    base64 -d          |  # decode as base64
    sha256sum          |  # SHA256 hash (returns hex)
    awk '{ print $1 }' |  # only the hex data
    xxd -r -p          |  # hex to bytes
    base64                # encode as base64
