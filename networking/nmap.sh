#!/usr/bin/env bash

################################################################################
#
# Base nmap scan
#
# ! WARNING: As of writing, this is geared towards the OSCP.
# ! Important information may be missed, and some parts may be outright dangerous.
# ! Don't blindly run this in a non-lab environment.
#
################################################################################

# Credit: https://stackoverflow.com/a/17841619/11832705
function join_by {
  local d=${1-} f=${2-}
  if shift 2; then
    printf %s "$f" "${@/#/$d}"
  fi
}

# Ports that are typically of interest
declare -a common_ports=(
  # common
  20
  21
  22
  23
  53
  79
  513  # rlogin
  631
  # snmp (for reference)
  161
  162
  # *more* MS-specific (though not necessarily!)
  88
  111
  445
  389
  636
  3389
  # email
  25
  109
  110
  143
  993
  995
  # most common http/alts
  80
  443
  3000
  8000
  8080
  8888
  # databases
  1433
  ## oracle
  1521
  1522
  1523
  1524
  1525
  1526
  1527
  1528
  1529
  ## onward
  3306
  5432
  5433
)

# nmap scripts that are a good default
declare -a nmap_script_categories=(
  auth
  default
  discovery
  safe
)

# Extra nmap scripts that aren't as safe and/or are more intrusive
# TODO: Separate array for less-safe nmap scripts
declare -a nmap_scripts=(
  banner
  docker-version
  ftp-vsftpd-backdoor
  jdwp-version
  http-aspnet-debug
  http-enum
  http-shellshock  # TODO: URI arg https://nmap.org/nsedoc/scripts/http-shellshock.html
  http-sitemap-generator
  http-userdir-enum
  http-vhosts
  http-vuln-cve2010-0738
  https-redirect
  ms-sql-empty-password
  mysql-empty-password
  mysql-enum
  mysql-vuln-cve2012-2122
  oracle-enum-users
  rdp-vuln-ms12-020
  rmi-vuln-classloader
  samba-vuln-cve-2012-1182
  smb-enum-domains
  smb-enum-groups
  smb-enum-processes
  smb-enum-services
  smb-enum-sessions
  smb-enum-shares
  smb-enum-users
  smb-server-stats
  smb-system-info
  smb-vuln-ms08-067
  smb-vuln-ms17-010
  smtp-open-relay
  smtp-vuln-cve2010-4344
  smtp-vuln-cve2011-1720  # TODO: too dangerous?
  smtp-vuln-cve2011-1764
  ssh-auth-methods
  ssh2-enum-algos
  vnc-title
)

# Vulners is way too verbose for my liking, and it's a default in the 'safe' category
cp /usr/share/nmap/scripts/vulners.nse /usr/share/nmap/scripts/vulners.nse.bak

# TODO: Any way to exclude vulners?
nmap -vvv -T4 -sS -n -Pn \
  -p- \
  -O \
  -sV \
  -sC \
  --script=$(join_by , $nmap_script_categories),$(join_by , $nmap_scripts) \
  -iL targets.txt


# Use a container:
sudo podman run --rm -it --privileged instrumentisto/nmap -T4 $other_opts
