#!/usr/bin/env bash

# Or use --host-file
smbmap -H "${ip:?}" -L -v --admin

# Get the domain, then:
smbmap -H "${ip:?}" -d "${domain:?}"
# Or:
smbmap -H "${ip:?}" -d "${domain:?}" -u 'guest' -p ''

# For a directory
smbmap -R "${share:?}"
