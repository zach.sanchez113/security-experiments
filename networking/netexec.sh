#!/bin/bash

# Test logins
netexec smb 192.168.0.1 -u usernames.txt -p passwords.txt --continue-on-success
netexec ssh 192.168.0.1 -u usernames.txt -p passwords.txt

# Get shares
netexec smb 192.168.0.1 -u "${TARGET_USER}" -p "${TARGET_PASSWORD}" --shares
