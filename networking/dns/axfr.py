#!/usr/bin/env python3

import os
import re
import sys
from textwrap import dedent
from typing import *

import click
from loguru import logger
from path import Path

import dns.query
import dns.resolver
import dns.zone
from dns.name import Name
from dns.rdata import Rdata
from dns.rdataset import Rdataset
from dns.rdatatype import ANY
from dns.rdtypes.ANY.CNAME import CNAME
from dns.rdtypes.ANY.MX import MX
from dns.rdtypes.ANY.NS import NS
from dns.rdtypes.ANY.PTR import PTR
from dns.rdtypes.ANY.SOA import SOA
from dns.rdtypes.ANY.SPF import SPF
from dns.rdtypes.ANY.TXT import TXT
from dns.rdtypes.IN.A import A
from dns.rdtypes.IN.AAAA import AAAA
from dns.rdtypes.IN.SRV import SRV
from dns.resolver import Answer
from dns.zone import Zone

LOG_FORMAT = (
    "<green>{time:YYYY-MM-DD HH:mm:ss}</green> | <level>{level: <8}</level> | <level>{message}</level>"
)


def rdata_slots_to_dict(record: Rdata) -> dict:
    return {
        "rdclass": record.rdclass,
        "rdtype": record.rdtype,
        "rdcomment": record.rdcomment,
        "_raw": record.to_text(),
    }


def cname_to_dict(record: CNAME) -> dict:
    target: Name = record.target
    return {"target": target.to_text()}


def mx_to_dict(record: MX) -> dict:
    preference: int = record.preference
    exchange: Name = record.exchange
    return {"preference": preference, "exchange": exchange.to_text()}


def ns_to_dict(record: NS) -> dict:
    target: Name = record.target
    return {"target": target.to_text()}


def ptr_to_dict(record: PTR) -> dict:
    target: Name = record.target
    return {"target": target.to_text()}


def soa_to_dict(record: SOA) -> dict:
    mname: Name = record.mname
    rname: Name = record.rname
    serial: int = record.serial
    refresh: int = record.refresh
    retry: int = record.retry
    expire: int = record.expire
    minimum: int = record.minimum

    return {
        "mname": mname.to_text(),
        "rname": rname.to_text(),
        "serial": serial,
        "refresh": refresh,
        "retry": retry,
        "expire": expire,
        "minimum": minimum,
    }


def spf_to_dict(record: SPF) -> dict:
    return {"text": record.to_text()}


def txt_to_dict(record: TXT) -> dict:
    return {"text": record.to_text()}


def a_to_dict(record: A) -> dict:
    address: str = record.address
    return {"address": address}


def aaaa_to_dict(record: AAAA) -> dict:
    address: str = record.address
    return {"address": address}


def srv_to_dict(record: SRV) -> dict:
    priority: int = record.priority
    weight: int = record.weight
    port: int = record.port
    target: Name = record.target

    return {
        "priority": priority,
        "weight": weight,
        "port": port,
        "target": target.to_text(),
    }


@click.command()
@click.argument("domain", type=str, required=True)
@click.option("--debug", is_flag=True, help="Enable debug logging")
def cli(domain: str, debug: bool = False):
    """Starter template for CLI scripts."""

    if debug:
        log_level = "DEBUG"
    else:
        # In case "TRACE" or something above "INFO" is desired
        log_level = os.environ.get("LOGURU_LEVEL") or "INFO"

    # Remove default logger, add custom
    logger.remove()
    logger.add(
        sys.stdout,
        format=LOG_FORMAT,
        level=log_level,
    )
    logger.configure(
        levels=[dict(name="INFO", color="<blue><bold>"), dict(name="DEBUG", color="<magenta><bold>")]  # type: ignore
    )

    # TODO: Cleanup?

    # Excessive typing/variables to illustrate how this is working
    soa_answer: Answer = dns.resolver.resolve(domain, SOA)
    soa_record: SOA = soa_answer[0]
    mname: str = soa_record.mname

    master_answer: Answer = dns.resolver.resolve(mname, A)

    z: Optional[Zone] = dns.zone.from_xfr(dns.query.xfr(master_answer[0].address, domain))

    if not z:
        logger.error(f"Failed to get DNS zones from {master_answer[0].address}!")
        sys.exit(1)

    # for rrset in r.answer:
    #     nodes[rrset.name] = Zone(origin, rdclass, relativize=relativize)
    n: Union[Name, str]
    for n in sorted(z.nodes.keys()):
        node: Zone = z[n]
        logger.debug(node.to_text())

        name: Union[Name, str]
        rdataset: Rdataset
        for name, rdataset in node.iterate_rdatasets(MX):
            logger.debug(name)

            rdata: Rdata  # actually MX, but for illustration
            for rdata in rdataset:
                logger.debug(rdata.to_text())


if __name__ == "__main__":
    cli()
