#!/usr/bin/env bash

onesixtyone -c /usr/share/seclists/Discovery/SNMP/common-snmp-community-strings-onesixtyone.txt 10.0.0.1

# For more community strings:
onesixtyone -c /usr/share/seclists/Discovery/SNMP/snmp.txt 10.0.0.1

snmp-check -c public -p 161 10.0.0.1
