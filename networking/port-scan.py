#!/usr/bin/env python3

from scapy.layers.inet import IP, ICMP, TCP
from scapy.sendrecv import send, sr1, sr

# TODO: scapy ACK scan https://scapy.readthedocs.io/en/latest/usage.html#ack-scan
# ofc other scan types as well
res, unans = sr(IP(dst="target")/TCP(flags="S", dport=(1,1024)) )
