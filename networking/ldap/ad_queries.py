"""[WIP] Queries I don't wanna forget.

https://github.com/skelsec/msldap/blob/master/msldap/client.py
https://github.com/shellster/LDAPPER/blob/master/queries.py

TODO: ACLs (this is more involved to rewrite due to custom classes in msldap)

TODO: Look at queries from LDAP wiki
    All examples: https://ldapwiki.com/wiki/LDAP%20Query%20Examples%20for%20AD
    Badness: https://ldapwiki.com/wiki/Active%20Directory%20RISK%20Related%20Searches
"""

from __future__ import annotations

from ad_constants import SAM_ACCOUNT_TYPES, UAC_FLAGS
from ldap3 import Connection
from util import LDAPResult, search_ldap

# Reference for extensible match rules:
#   https://ldapwiki.com/wiki/Microsoft%20Active%20Directory%20Extensible%20Match%20Rules

# LDAP_MATCHING_RULE_BIT_AND
BITWISE_AND = "1.2.840.113556.1.4.803"
# LDAP_MATCHING_RULE_BIT_OR
BITWISE_OR = "1.2.840.113556.1.4.804"


def uac(name: str) -> int:
    value = [k for k, v in UAC_FLAGS.items() if v == name.upper()]
    if not value:
        raise Exception(f"Unknown UAC flag {name}")
    return value[0]


def sam_account_type(name: str) -> int:
    value = [k for k, v in SAM_ACCOUNT_TYPES.items() if v == name.upper()]
    if not value:
        raise Exception(f"Unknown sAMAccountType {name}")
    return value[0]


def get_gpos(conn: Connection, base_dn: str) -> list[LDAPResult]:
    return search_ldap(
        conn,
        base_dn=base_dn,
        query="(objectCategory=groupPolicyContainer)",
    )


def get_gpo_by_ou(conn: Connection, base_dn: str, ou: str) -> list[LDAPResult]:
    query = f"(cn={ou})"
    search_kwargs = dict(attributes=["gpLink"])


# TODO: ldapper has more robust query
def get_laps(conn: Connection, base_dn: str) -> list[LDAPResult]:
    # NOTE: 0x30000001 == 805306369
    results = search_ldap(
        conn,
        base_dn,
        query=f"(&(sAMAccountType={sam_account_type('SAM_MACHINE_ACCOUNT')})(ms-mcs-admPwd=*))",
    )

    return [r for r in results if r.get("ms-mcs-admPwd")]


# TODO: ldapper has more robust query
def get_kerberoastable(conn: Connection, base_dn: str) -> list[LDAPResult]:
    # NOTE: 0x30000000 == 805306368
    results = search_ldap(
        conn,
        base_dn,
        query=f"(&(sAMAccountType={sam_account_type('SAM_USER_OBJECT')})(servicePrincipalName=*))",
    )

    return [r for r in results if r.get("servicePrincipalName")]


# TODO: ldapper has more robust query
def get_asrep_roastable(conn: Connection, base_dn: str) -> list[LDAPResult]:
    # NOTE: 0x00400000 == 4194304
    return search_ldap(
        conn,
        base_dn,
        query=f"(userAccountControl:{BITWISE_AND}:={uac('DONT_REQ_PREAUTH')})",
    )


def get_krb_unconstrained_delegation(conn: Connection, base_dn: str) -> list[LDAPResult]:
    """Kerberos unconstrained delegation

    Further reading:
        https://www.ired.team/offensive-security-experiments/active-directory-kerberos-abuse/domain-compromise-via-unrestricted-kerberos-delegation
        https://adsecurity.org/?p=1667
    """

    query = f"""
        (&
            (|
                (sAMAccountType={sam_account_type('SAM_USER_OBJECT')})
                (sAMAccountType={sam_account_type('SAM_MACHINE_ACCOUNT')})
            )
            (userAccountControl:{BITWISE_AND}:={uac('TRUSTED_FOR_DELEGATION')})
        )
    """

    return search_ldap(
        conn,
        base_dn,
        query=query,
    )


def get_krb_constrained_delegation(conn: Connection, base_dn: str) -> list[LDAPResult]:
    """Kerberos constrained delegation

    Further reading:
        https://www.ired.team/offensive-security-experiments/active-directory-kerberos-abuse/abusing-kerberos-constrained-delegation
    """

    # TODO: Remove check for msDS-AllowedToDelegateTo?
    query = f"""
        (&
            (|
                (sAMAccountType={sam_account_type('SAM_USER_OBJECT')})
                (sAMAccountType={sam_account_type('SAM_MACHINE_ACCOUNT')})
            )
            (userAccountControl:{BITWISE_AND}:={uac('TRUSTED_TO_AUTH_FOR_DELEGATION')})
            (msDS-AllowedToDelegateTo=*)
        )
    """

    return search_ldap(
        conn,
        base_dn,
        query=query,
    )


def query_naughty_password(conn: Connection, base_dn: str) -> list[LDAPResult]:
    """Perform checks for bad password policies/management.

    - Password stored with reversible encryption
    - Password not required
    - Password that doesn't expire

    TODO: Is PASSWD_CANT_CHANGE interesting?
    TODO: Only check user objects?

    Further reading (reversible encryption):
        https://github.com/The-Viper-One/Pentest-Everything/blob/Main/everything/everything-active-directory/credential-access/modify-authentication-process/reversible-encryption.md
        http://blog.teusink.net/2009/08/passwords-stored-using-reversible.html
        https://adsecurity.org/?p=2053
            ^ DCSync page, just mentions in passing
    """

    query = f"""
        (&
            (|
                (sAMAccountType={sam_account_type('SAM_USER_OBJECT')})
                (sAMAccountType={sam_account_type('SAM_MACHINE_ACCOUNT')})
            )
            (userAccountControl:{BITWISE_AND}:={uac('ENCRYPTED_TEXT_PWD_ALLOWED')})
            (userAccountControl:{BITWISE_AND}:={uac('PASSWD_CANT_CHANGE')})
            (userAccountControl:{BITWISE_AND}:={uac('DONT_EXPIRE_PASSWD')})
        )
    """

    return search_ldap(
        conn,
        base_dn,
        query=query,
    )
