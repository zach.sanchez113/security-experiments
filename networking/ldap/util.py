"""LDAP utility functions.

These are really just functions that I use as a base to write functions for specific domains, with the settings
I'd prefer as defaults.

For example, I may create search_contoso() and search_contoso_username(), which would set up the LDAP connection
and execute the appropriate query respectively.
"""

from __future__ import annotations

from collections.abc import Mapping, Sequence
from typing import Any

import dynaconf.utils.functional
import ldap3
from ad_constants import (
    PWD_POLICY_FLAGS,
    SAM_ACCOUNT_TYPES,
    TRUST_DIRECTIONS,
    TRUST_FLAGS,
    TRUST_TYPE,
    UAC_FLAGS,
)
from ldap3 import ALL, ALL_ATTRIBUTES, Connection, Server
from loguru import logger

# Type alias for LDAP result dict cause it's long
LDAPResult = dict[str, list[Any]]


def walk_nested(
    obj: Sequence | Mapping | dynaconf.utils.functional.LazyObject,
    *args: Any,
    default: Any = None,
    all_or_nothing: bool = True,
) -> Any:
    """Walk the nested objects inside 'obj' with the specified *args as key names and return the value at the end.

    This is slightly more flexible than obj.get('foo', {}).get('bar', {}).get('baz') because a TypeError can be thrown
    if the key name exists but is not a dictionary.

    Examples:
        obj = {'foo': {'bar': {'baz': 1}}, 'buzz': 'ERROR' }
        walk_nested(obj, 'foo', 'bar', 'baz')  # 1
        walk_nested(obj, 'foo', 'bar', 'buzz', all_or_nothing=False)  # {'baz': 1}
        walk_nested(obj, 'buzz', 'baz', all_or_nothing=False) # 'ERROR'

    Args:
        obj (Sequence | Mapping): A dictionary/iterable-like object containing nested dictionaries/elements.
        *args (Any): The individual keys and/or indices to walk.
        default (Any, optional): Default value to use if an exception occurs. Defaults to None.
        all_or_nothing (bool, optional): If True and an error is encountered, return the default. If False, return the most recent obj. Defaults to True.

    Returns:
        Any: The return value, determined per above.
    """

    try:
        for arg in args:
            obj = obj[arg]
        return obj
    except (KeyError, TypeError, IndexError, AttributeError):
        return default if all_or_nothing else obj


def internal_to_human(internal: dict[int, str], value: int) -> str:
    return " & ".join(v for k, v in internal.items() if k & value)

def get_ldap_server(ldap_uri: str, server_kwargs: dict | None = None) -> ldap3.Server:
    """Get an ldap3.Server object for the given URI.

    Args:
        ldap_uri (str): LDAP connection string.
        server_kwargs (dict, optional): kwargs for ldap3.Server. Defaults to None.

    Returns:
        ldap3.Server: LDAP server object.
    """

    if server_kwargs is None:
        server_kwargs = {}

    server_kwargs = {"get_info": ALL, "connect_timeout": 5, **server_kwargs}

    logger.debug(f"Initializing LDAP server object for {ldap_uri}...")
    logger.trace(f"{server_kwargs = }")

    return Server(ldap_uri, **server_kwargs)


def get_ldap_connection(
    ldap_uri: str,
    bind_dn: str | None = None,
    bind_password: str | None = None,
    server_kwargs: dict | None = None,
    connection_kwargs: dict | None = None,
) -> ldap3.Connection:
    """Get an ldap3.Connection object for the given LDAP server.

    Args:
        ldap_uri (str): LDAP connection string.
        bind_dn (str | None, optional): Bind DN (username). Defaults to None.
        bind_password (str | None, optional): Password for user. Defaults to None.
        server_kwargs (dict, optional): kwargs for ldap3.Server. Defaults to None.
        connection_kwargs (dict, optional): kwargs for ldap3.Connection. Defaults to None.

    Returns:
        ldap3.Connection: LDAP server connection.
    """

    if connection_kwargs is None:
        connection_kwargs = {}

    connection_kwargs = {"user": bind_dn, "password": bind_password, "auto_bind": True, **connection_kwargs}

    server = get_ldap_server(ldap_uri, server_kwargs)

    logger.debug(f"Initializing LDAP connection for {ldap_uri}...")
    logger.trace(f"{connection_kwargs = }")
    logger.trace(f"{bind_dn = }")

    try:
        return Connection(server, **connection_kwargs)
    except Exception as e:
        logger.error(f"Failed to initialize LDAP connection for {ldap_uri}!")
        logger.error(e)
        raise


def get_ldap_naming_contexts(
    ldap_uri: str,
    bind_dn: str | None = None,
    bind_password: str | None = None,
    server_kwargs: dict | None = None,
    connection_kwargs: dict | None = None,
) -> list[str] | None:
    """Get the LDAP naming contexts (i.e. base/search DN) for the given LDAP server.

    Args:
        ldap_uri (str): LDAP connection string.
        bind_dn (str | None, optional): Bind DN (username). Defaults to None.
        bind_password (str | None, optional): Password for user. Defaults to None.
        server_kwargs (dict, optional): kwargs for ldap3.Server. Defaults to None.
        connection_kwargs (dict, optional): kwargs for ldap3.Connection. Defaults to None.

    Returns:
        list[str] | None: List of naming contexts for the given server if available, else None.
    """

    if connection_kwargs is None:
        connection_kwargs = {}

    connection_kwargs = {"user": bind_dn, "password": bind_password, "auto_bind": True, **connection_kwargs}

    server = get_ldap_server(ldap_uri, server_kwargs)

    logger.debug(f"Initializing LDAP connection for {ldap_uri}...")
    logger.trace(f"{connection_kwargs = }")
    logger.trace(f"{bind_dn = }")

    # Need to initialize connection before info is available
    try:
        conn = Connection(server, **connection_kwargs)
        return server.info.naming_contexts
    except Exception as e:
        logger.error(f"Failed to initialize LDAP connection for {ldap_uri}!")
        logger.error(e)
        raise


def search_ldap(
    conn: ldap3.Connection,
    base_dn: str,
    query: str,
    translate_attrs: bool = True,
    search_kwargs: dict | None = None,
) -> list[LDAPResult]:
    """Perform a query against LDAP.

    Args:
        connection (ldap3.Connection): LDAP connection to use.
        base_dn (str): Base DN for the search.
        query (str): LDAP query.
        translate_attrs (bool, optional): Convert binary values to human-readable ones, e.g. sAMAccountType. Defaults to True.
        search_kwargs (dict | None, optional): kwargs for ldap3.Connection.search. Defaults to None.

    Returns:
        list[LDAPResult]: Results from LDAP search.
    """

    if search_kwargs is None:
        search_kwargs = {}

    search_kwargs = {
        "attributes": ALL_ATTRIBUTES,
        "get_operational_attributes": True,
        **search_kwargs,
    }

    logger.debug(f"Running LDAP query against {conn.server.host}...")
    logger.trace(f"{base_dn = }")
    logger.trace(f"{query = }")
    logger.trace(f"{search_kwargs = }")

    conn.search(base_dn, query, **search_kwargs)

    entries = []
    for entry in conn.entries:
        data = entry.entry_attributes_as_dict

        # This isn't always included in entry_attributes_as_dict, so add it back in if needed
        if "distinguishedName" not in data.keys():
            data["distinguishedName"] = [entry.entry_dn]

        # This is massive and unnecessary the vast majority of the time, so snip it
        if walk_nested(entry, "thumbnailPhoto", 0):
            entry["thumbnailPhoto"] = ["<snip>"]

        if translate_attrs:
            if (sAMAccountType := walk_nested(data, "sAMAccountType", 0)) is not None:
                data["samAccountType"] = [internal_to_human(SAM_ACCOUNT_TYPES, sAMAccountType)]
            if (userAccountControl := walk_nested(data, "userAccountControl", 0)) is not None:
                data["userAccountControl"] = [internal_to_human(UAC_FLAGS, userAccountControl)]

            # TODO: The rest of these haven't been tested
            if (pwdProperties := walk_nested(data, "pwdProperties", 0)) is not None:
                data["pwdProperties"] = [internal_to_human(PWD_POLICY_FLAGS, pwdProperties)]
            if (trustAttributes := walk_nested(data, "trustAttributes", 0)) is not None:
                data["trustAttributes"] = [internal_to_human(TRUST_FLAGS, trustAttributes)]
            if (trustDirection := walk_nested(data, "trustDirection", 0)) is not None:
                data["trustDirection"] = [internal_to_human(TRUST_DIRECTIONS, trustDirection)]
            if (trustType := walk_nested(data, "trustType", 0)) is not None:
                data["trustType"] = [internal_to_human(TRUST_TYPE, trustType)]

        entries.append(data)

    logger.debug(f"Got {len(conn.entries)} results for search: {query}")

    return entries
