"""[WIP] Get GPO with LDAP and SMB.

Reference:
https://www.totalnetsolutions.net/2017/09/13/getting-group-policy-information-via-ldap-and-smb-only/
https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Active%20Directory%20Attack.md#exploit-group-policy-objects-gpo

Basics: https://techgenix.com/group-policy-settings-part1/

- Administrative Templates Settings -> registry change -> Registry.pol
- Security Options Settings -> Machine\\Microsoft\\Windows NT\\SecEdit\\gpttmpl.inf
    - Backslashes escaped to avoid annoying pyright
- Preferences Settings -> XML files (location varies)

The other two parts are good to know as well, but less relevant for SMB portion.

Also see: https://github.com/Hackndo/pyGPOAbuse

Library I'm amazed I haven't seen yet: https://msldap.readthedocs.io/en/latest/
-> https://github.com/skelsec/winacl

TODO: Store this somewhere...
https://www.howtogeek.com/268337/what-is-this-process-and-why-is-it-running/
https://web.archive.org/web/20220814004500/https://adcoding.com/
"""

from __future__ import annotations

from more_itertools import flatten
from util import get_ldap_connection, search_ldap

conn = get_ldap_connection("", "", "")

# GPO doesn't live in the actual domain - we need to get it for the OU
# To get all: query="(objectCategory=groupPolicyContainer)"
results = search_ldap(
    conn,
    base_dn="",
    query="(objectClass=organizationalUnit)",
    search_kwargs=dict(attributes=["gpLink"]),
)

# This will be the new search base
# gpLink is a multi-valued array, but for the sake of argument, assume a single one
# Example: [LDAP://CN={6AC1786C-016F-11D2-945F-00C04fB984F9},CN=Policies,CN=System,DC=company,Dc=com;0]
gpLink = list(flatten([r.get("gpLink", []) for r in results]))[0]

# Next:
results = search_ldap(
    conn,
    base_dn=gpLink,
    query="(objectClass=*)",
    search_kwargs=dict(
        attributes=[
            "displayName",  # Default Domain Policy
            "versionNumber",  # 15 (should be same as in `gPCFileSysPath\GPT.INI`)
            # gPC -> group policy client
            "gPCFileSysPath",  # \\example.com\sysvol\example.com\Policies\{31B2F340-016D-11D2-945F-00C04FB984F9}
            "gPCMachineExtensionNames",  # [{35378EAC-683F-11D2-A89A-00C04FBBCFA2}{53D6AB1B-2488-11D1-A28C-00C04FB94F17}<...>]
        ]
    ),
)

# For password policy, {827D319E-6EAC-11D2-A4EA-00C04F79F83A} Security
#
# TODO: Does this imply GUID in the path?
# \\domain\sysvol\domain\\MACHINE\Microsoft\Windows NT\SecEdit\GptTmpl.inf
# -> MACHINE\System\CurrentControlSet\Services\Netlogon\Parameters\MaximumPasswordAge=4,15
#
# From PayloadsAllTheThings:
#   GPO are stored in the DC in \\<domain.dns>\SYSVOL\<domain.dns>\Policies\<GPOName>\, inside two folders User and Machine.
#   If you have the right to edit the GPO you can connect to the DC and replace the files.

gPCFileSysPath = "\\\\example.com\\sysvol\\example.com\\Policies\\{31B2F340-016D-11D2-945F-00C04FB984F9}"
