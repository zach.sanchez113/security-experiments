# Security Experiments

This is just a collection of scripts/config that I'm writing to learn more about whatever topics, test out new ideas, etc.

- [Security Experiments](#security-experiments)
- [Windows](#windows)
  - [GIFShell (WIP)](#gifshell-wip)
- [Linux](#linux)
  - [suid.sh](#suidsh)
  - [path.sh](#pathsh)
  - [amnesia.sh](#amnesiash)
  - [ssh-scrape.sh](#ssh-scrapesh)
  - [Obfuscation](#obfuscation)
  - [eBPF](#ebpf)
    - [Windows](#windows-1)
  - [sysadmin.py](#sysadminpy)
  - [fs](#fs)
  - [sftp-chroot](#sftp-chroot)
  - [systemd-user-setup](#systemd-user-setup)
  - [sudo (TODO)](#sudo-todo)
- [Passwords](#passwords)
  - [rainbow.py](#rainbowpy)
  - [db.py](#dbpy)
  - [truffles.py (WIP)](#trufflespy-wip)
- [Networking](#networking)
  - [icmp-payload.py](#icmp-payloadpy)
  - [DNS (TODO)](#dns-todo)
  - [LDAP (TODO)](#ldap-todo)
- [Containers](#containers)
  - [esedbexport](#esedbexport)

# Windows

## GIFShell (WIP)

Cleaned up the code from [bobbyrsec/Microsoft-Teams-GIFShell](https://github.com/bobbyrsec/Microsoft-Teams-GIFShell) so that it fit my preferences/is easier to customize, and turned the GIFShell server into a proper CLI.

# Linux

## suid.sh

Find all SUID/SGID files in the FS.

## path.sh

Print all files in $PATH.

**NOTE:** An alternative quick-and-dirty way could be to run 'compgen -c' or 'compgen -A function -abck', though this will capture a lot of garbage.

**NOTE:** `apropos` came up, may be useful later.

## amnesia.sh

Create an amnesiac shell/environment.

No, this isn't comprehensive, but it's something at least.

## ssh-scrape.sh

Find all SSH directories, create a tarball of their contents.

## Obfuscation

Or rather, "obfuscation".

May or may not be useful, just messing around with dumb ideas when I really should be doing other things. At the very least, turns out these tactics are used by Metasploit when dropping a shell (only in Linux?).

## eBPF

Seems like this could be a pretty powerful tool for the post-exploit toolbox. No interesting code as of yet, just compiling notes.

Resources:

- [Official intro](https://ebpf.io/what-is-ebpf)
- [Less-official, but more detailed](https://docs.cilium.io/en/stable/concepts/ebpf/intro/)
- [BPF and XDG Reference Guide](https://docs.cilium.io/en/stable/bpf/)

Toolkits/theory:

- [Equation Group's backdoor `Bvp47`](https://cloudflare-ipfs.com/ipfs/QmY22JB4i86y1FWH6thz1kxbcLez8K94wznGZ943Pn2ohx?filename=NSA-Backdoor-Bvm47.pdf)
- [DEF CON 29: Bad BPF - Warping reality using eBPF](https://blog.tofile.dev/2021/08/01/bad-bpf.html)
- [Jeff Dileo - Evil eBPF Practical Abuses of In-Kernel Bytecode Runtime- DEF CON 27 Conference](https://www.youtube.com/watch?v=yrrxFZfyEsw)
- [CVE-2020-8835: LINUX KERNEL PRIVILEGE ESCALATION VIA IMPROPER EBPF PROGRAM VERIFICATION](https://www.zerodayinitiative.com/blog/2020/4/8/cve-2020-8835-linux-kernel-privilege-escalation-via-improper-ebpf-program-verification)

**TODO - look into these related technologies:**

- [Kernel runtime security instrumentation](https://lwn.net/Articles/798157/)
- [Seccomp BPF](https://www.kernel.org/doc/html/v4.16/userspace-api/seccomp_filter.html)
- Linux Security Modules (LSM)
  - Kernel modules that can hook into security functionality, e.g. SELinux
  - Lab idea at https://cs-uob.github.io/COMSM0049/labs/LAB3.html

### Windows

This may be somewhat usable on Windows as well... https://github.com/Microsoft/ebpf-for-windows

## sysadmin.py

Extracts system/network/process info. Just some handy Python functionality for my own reference/future use.

## fs

Filesystem-related scripts that I wanna hang onto for my own reference.

## sftp-chroot

Set up an SFTP chroot with directories that can be written to by (non-root) users belonging to a specific group, but can only be read by users belonging to a different (non-root) group.

In my script specifically:

- The SFTP user isn't allowed to get a shell in order to avoid the possibility of a chroot escape.
- The SFTP directory is placed under `/srv/sftp` since I'm pretty sure that's how `/srv` is supposed to be used.
- The chroot will drop you into `/srv/sftp/chroot` - obviously this can be whatever you want, I just chose an arbitrary name.
  - Changing this also requires a modification to `sshd_config`.
- The directory `/srv/sftp/chroot/dropzone` is where the SFTP user can drop their files.
- `sshd_config` requires SSH keys for the SFTP group, and specifies that keys must be placed under `/etc/ssh/authorized_keys/` for each user.
- `sshd_config` has a logging config that may be a bit too verbose for your tastes.

**WARNING: This won't work if the SFTP application requires the ability to list files in the directory.** For example, WinSCP may create temporary files if the original file is large enough (this can be toggled by a flag).

## systemd-user-setup

Setup for systemd user services.

Steps:

- Create and start systemd user manager.
  - See: `user@.service`. Do note that the user ID must follow the `@`, e.g. `user@1000.service`.
- Enable user lingering via systemd login manager.
- Export `XDG_RUNTIME_DIR` in (the shell's) `.profile` and the shell's rcfile.
  - This is required so the appropriate `/run` directory can be located.
- Enable persistent logging in the `journald` config.

## sudo (TODO)

Goal is to create a single tool that does the following:

- Detect weak sudoers rules
- Detect sudoers rules that point to suid
- Detect sudoers rules that point to nonexistent files

Sources of inspiration (for down the line):

- suid3num
- SUDO_KILLER
- https://github.com/broadinstitute/python-sudoers


# Passwords

## rainbow.py

Goals:

- Generate NTLM hashes for a given password
- Figure out how to create a rainbow table
- Investigate feasibility of a rainbow table for NTLM
  - Answer: Probably not a good idea past 7 or so characters at most.

## db.py

Followup to rainbow.py, attempt to create an SQLite DB that holds the rainbow table and is indexed on the NTLM hash.

**WARNING: This script doesn't work 100%.**

- The status message upon committing a transaction stops firing after the first one.
- The script doesn't seem to finish executing in the timeframe I'd expect. Either there's some runaway condition that I didn't catch due to missing status messages, or committing transactions takes a prohibitively long time after a certain point.
- Even after cancelling, the DB seems wayyy bigger than I would've expected.

## truffles.py (WIP)

Adjusting the formatting/structure of truffleHog v2 to fit my preferences and make it easier to customize. Not to mention that truffleHog decided to get rid of entropy-based detections for some reason, so new features will never come.

End goal is to:

- Allow tuning of the entropy threshold, i.e. specify how random a string must be before it counts as a match
- Add an option to print out only the unique matches

Why the second one? The default behavior is to print every **diff** that contains a string with high entropy, which gets noisy really quick if changes are happening to surrounding words/lines. For a bad example, this pseudocode would be a match:

```
passwords = ["l3tm31jn!@123"]
```

However, let's say somebody adds another password to this list later:

```
passwords = ["l3tm31jn!@123", "sUp3RS3cure5^7&"]
```

Now you have duplicate hits for `l3tm31jn!@123`. Not the end of the world in this case (hence "bad example"), but I noticed it can be a major issue when there are false positives. In my case, I got dozens of results for the same garbage string on a very long line. This made analysis a lot more annoying, and automation would've been required to handle this on a larger scale.

Or I could just give new life to the relatively simple code.

# Networking

## icmp-payload.py

Send a payload over ICMP with scapy.

Plays into (e)BPF usecases.

## DNS (TODO)

Goals:

- General-purpose AXFR script.
- Classes (or whatever else) to cleanly/clearly represent all DNS record types, dump to JSON/CSV, etc.

Reminder for myself: https://www.netmeister.org/blog/dns-rrs.html

## LDAP (TODO)

Goals:

- Template to base LDAP-related scripts off of.
- General-purpose CLI script with:
  - Common LDAP queries/methods
  - Common AD-specific functionality

**TODO:**

- Queries from `LDAPPER`
- Utility functions to translate AD values to human-readable/usable
- Queries from `ldapdomaindump`
- Queries from `bloodhound.py`

# Containers

## esedbexport

A quick POC for compiling libesedb and exposing the esedbexport binary.

```bash
cd ./esedbexport/
podman build --tag=esedbexport .
podman run --rm esedbexport -h
```

Invocation is a pretty long command though, unfortunately:

```bash
podman run --rm -v /path/to/ntds.dit:/path/to/ntds.dit -v /path/to/out/:/path/to/out/ esedbexport -t /path/to/out/ /path/to/ntds.dit
```
