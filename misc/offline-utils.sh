#!/usr/bin/env zsh

###############################################################################################################
#
# rust-based utils
#
# ! Using musl builds where possible to prevent glibc compatibility issues
#
# This won't work if native libraries are needed, though - in that case, use a container w/ an older glibc
# https://kobzol.github.io/rust/ci/2021/05/07/building-rust-binaries-in-ci-that-work-with-older-glibc.html
#
# To check if musl or not, use this command (after making sure it outputs good!):
# eval $(ldd "${path_to_binary}" 2>/dev/null | grep libc --color=none | awk '{ print $3; }')
#
###############################################################################################################

mkdir -p /usr/local/pkg/{musl,gnu}

wget -O /usr/local/pkg/musl/exa-v0.10.1.zip \
  https://github.com/ogham/exa/releases/download/v0.10.1/exa-linux-x86_64-musl-v0.10.1.zip

unzip -p /usr/local/pkg/musl/exa-v0.10.1.zip bin/exa > /usr/local/pkg/musl/exa
chmod +x /usr/local/pkg/musl/exa

wget -O /usr/local/pkg/musl/bat-v0.22.0.tar.gz \
  https://github.com/sharkdp/bat/releases/download/v0.22.0/bat-v0.22.0-x86_64-unknown-linux-musl.tar.gz

tar xvzf /usr/local/pkg/musl/bat-v0.22.0.tar.gz -C /usr/local/pkg/musl/ bat-v0.22.0-x86_64-unknown-linux-musl/bat --strip-components=1

wget -O /usr/local/pkg/musl/zellij-v0.31.3.tar.gz \
  https://github.com/zellij-org/zellij/releases/download/v0.31.3/zellij-x86_64-unknown-linux-musl.tar.gz

tar xvzf /usr/local/pkg/musl/zellij-v0.31.3.tar.gz -C /usr/local/pkg/musl/ zellij

wget -O /usr/local/pkg/musl/ripgrep-13.0.0.tar.gz \
    https://github.com/BurntSushi/ripgrep/releases/download/13.0.0/ripgrep-13.0.0-x86_64-unknown-linux-musl.tar.gz

tar xvzf /usr/local/pkg/musl/ripgrep-13.0.0.tar.gz -C /usr/local/pkg/musl/ ripgrep-13.0.0-x86_64-unknown-linux-musl/rg --strip-components=1

wget -O /usr/local/pkg/musl/du-dust-v0.8.3.tar.gz \
    https://github.com/bootandy/dust/releases/download/v0.8.3/dust-v0.8.3-x86_64-unknown-linux-musl.tar.gz

tar xvzf /usr/local/pkg/musl/du-dust-v0.8.3.tar.gz -C /usr/local/pkg/musl/ dust-v0.8.3-x86_64-unknown-linux-musl/dust --strip-components=1

wget -O /usr/local/pkg/musl/tealdeer-v1.5.0 \
    https://github.com/dbrgn/tealdeer/releases/download/v1.5.0/tealdeer-linux-x86_64-musl

cp /usr/local/pkg/musl/tealdeer-v1.5.0 /usr/local/pkg/musl/tealdeer
chmod +x /usr/local/pkg/musl/tealdeer

wget -O /usr/local/pkg/musl/bottom-0.6.8.tar.gz \
    https://github.com/ClementTsang/bottom/releases/download/0.6.8/bottom_x86_64-unknown-linux-musl.tar.gz

tar xvzf /usr/local/pkg/musl/bottom-0.6.8.tar.gz -C /usr/local/pkg/musl/ btm

# NOTE: Does this even use libc??
wget -O /usr/local/pkg/musl/procs-v0.13.0.zip \
    https://github.com/dalance/procs/releases/download/v0.13.0/procs-v0.13.0-x86_64-linux.zip

unzip -p /usr/local/pkg/musl/procs-v0.13.0.zip procs > /usr/local/pkg/musl/procs
chmod +x /usr/local/pkg/musl/procs

wget -O /usr/local/pkg/gnu/rcat https://github.com/robiot/rustcat/releases/download/v3.0.0/rcat-v3.0.0-linux-x86_64
chmod +x /usr/local/pkg/gnu/rcat

mkdir /usr/local/pkg/musl/archive
mv /usr/local/pkg/musl/*gz /usr/local/pkg/musl/archive
mv /usr/local/pkg/musl/*zip /usr/local/pkg/musl/archive
mv /usr/local/pkg/musl/*0 /usr/local/pkg/musl/archive

mkdir /usr/local/pkg/gnu/archive
mv /usr/local/pkg/gnu/*gz /usr/local/pkg/gnu/archive
mv /usr/local/pkg/gnu/*zip /usr/local/pkg/gnu/archive
mv /usr/local/pkg/gnu/*0 /usr/local/pkg/gnu/archive

chown -R root:root /usr/local/pkg/


###############################################################################################################
#
# Not rust, but still useful!
#
###############################################################################################################

# GNU coreutils statically-compiled against musl libc
# For more targeted binaries/other architectures: https://busybox.net/downloads/binaries/
wget https://busybox.net/downloads/binaries/1.35.0-x86_64-linux-musl/busybox -O /usr/local/pkg/musl/busybox

# For JSON processing
wget https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64 -O /usr/local/pkg/jq

# For JSON/YAML/whatever conversion
wget https://github.com/sclevine/yj/releases/download/v5.1.0/yj-linux-amd64 -O /usr/local/pkg/yj

# Pager for grep
wget https://github.com/vrothberg/vgrep/releases/download/v2.6.1/vgrep_2.6.1_Linux_x86_64 -O /usr/local/pkg/vgrep
