#!/usr/bin/env bash

################################################################################
#
# Reference point for common jq usage
#
################################################################################

# Sort keys
jq -S < file.json

# Select where field != val for a list of dicts
jq '.[] | select(.status==403 | not)' < wappalyzer-out.json
# ...plus specific key
jq '.[] | select(.status==403 | not) | .url' < wappalyzer-out.json

# String equality
jq '.[] | select(.name=="thing")' <  out.json

# Substring
jq '.[] | select(.name | test("thing"))' <  out.json

# Remove null
jq -S '.[] | with_entries(select(.value != null))' < ntds.dit.json
