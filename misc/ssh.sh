#!/usr/bin/env bash

# Use a jumpbox to access an otherwise inaccessible target (usual options apply)
jumpbox="10.1.1.1"
target="10.2.2.2"

ssh -J $jumpbox $target

# Don't allocate a PTY, which prevents detection via utmp or w/who
# Credit: https://twitter.com/Alh4zr3d/status/1578406155453276160
ssh -o UserKnownHostsFile=/dev/null -T $user@$target 'bash -i'
