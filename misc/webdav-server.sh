#!/usr/bin/env bash

# In explorer: \\$IP_ADDY\DavWWWRoot\
wsgidav --host=0.0.0.0 --port=80 --auth=anonymous --root /srv/webdav
