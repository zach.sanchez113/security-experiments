#!/usr/bin/env python3

"""GHSA parsing.

Schema: https://ossf.github.io/osv-schema/
Example: https://github.com/github/advisory-database/blob/main/advisories/github-reviewed/2023/03/GHSA-xw5p-hw8j-xg4q/GHSA-xw5p-hw8j-xg4q.json
CVE-2023-0594: https://nvd.nist.gov/vuln/detail/CVE-2023-0594
CVSS v3.1 spec: https://www.first.org/cvss/calculator/3.1

Notes:
    - Cloning the repo took ~20 minutes w/ 2Mb/s download. This is presumably faster for future pulls.
    - `ecosystem_specific` and `database_specific` fields require custom processing depending on the advisory
      source.
"""

from __future__ import annotations

import os
import sys
import typing
from typing import Any, Literal, assert_never, cast

from loguru import logger
from path import Path
from pydantic import BaseModel, ConfigDict, Field, ValidationError, computed_field, field_validator
from rich.console import Console

# Base logging configuration
LOG_FORMAT = "<green>{time:YYYY-MM-DD HH:mm:ss}</green> | <level>{level: <8}</level> | <level>{message}</level>"  # fmt: skip

logger.configure(
    handlers=[
        dict(
            sink=sys.stdout, level=os.environ.get("LOGURU_LEVEL", "INFO"), format=LOG_FORMAT, diagnose=False
        ),
    ],
    levels=[
        {"name": "INFO", "color": "<blue><bold>"},
        {"name": "DEBUG", "color": "<magenta><bold>"},
    ],
)

OSV_ECOSYSTEMS = (
    "Go",
    "npm",
    "OSS-Fuzz",
    "PyPI",
    "RubyGems",
    "crates.io",
    "Packagist",
    "Maven",
    "NuGet",
    "Linux",
    "Debian",
    "Alpine",
    "Hex",
    "Android",
    "GitHub Actions",
    "Pub",
    "ConanCenter",
    "Rocky Linux",
    "AlmaLinux",
)


class OSVBaseModel(BaseModel):
    model_config = ConfigDict(extra="forbid", frozen=True)


# TODO: This is just base score metrics. May need to account for temporal score metrics + env score metrics
class CVSS_V3_1(OSVBaseModel):  # noqa: N801
    attack_vector_raw: Literal["N", "A", "L", "P"] = Field(repr=False, exclude=True)
    attack_complexity_raw: Literal["L", "H"] = Field(repr=False, exclude=True)
    privileges_required_raw: Literal["N", "L", "H"] = Field(repr=False, exclude=True)
    user_interaction_raw: Literal["N", "R"] = Field(repr=False, exclude=True)
    scope_raw: Literal["U", "C"] = Field(repr=False, exclude=True)
    confidentiality_raw: Literal["N", "L", "H"] = Field(repr=False, exclude=True)
    integrity_raw: Literal["N", "L", "H"] = Field(repr=False, exclude=True)
    availability_raw: Literal["N", "L", "H"] = Field(repr=False, exclude=True)

    def _nlh(self, value: Literal["N", "L", "H"]) -> Literal["None", "Low", "High"]:
        match value:
            case "N":
                return "None"
            case "L":
                return "Low"
            case "H":
                return "High"
            case _:
                typing.assert_never(value)

    @computed_field
    @property
    def vector_string(self) -> str:
        return (
            f"AV:{self.attack_vector_raw}"
            + f"/AC:{self.attack_complexity_raw}"
            + f"/PR:{self.privileges_required_raw}"
            + f"/UI:{self.user_interaction_raw}"
            + f"/S:{self.scope_raw}"
            + f"/C:{self.confidentiality_raw}"
            + f"/I:{self.integrity_raw}"
            + f"/A:{self.availability_raw}"
        )

    @computed_field
    @property
    def attack_vector(self) -> Literal["Network", "Adjacent", "Local", "Physical"]:
        match self.attack_vector_raw:
            case "N":
                return "Network"
            case "A":
                return "Adjacent"
            case "L":
                return "Local"
            case "P":
                return "Physical"
            case _:
                assert_never(self.attack_vector_raw)

    @field_validator("_attack_vector", check_fields=False)
    @classmethod
    def validate_attack_complexity(cls, value: Any):
        if not isinstance(value, str):
            raise TypeError("_attack_complexity must be a string")
        elif value not in ["L", "H"]:
            raise ValueError("_attack_complexity must be one of 'L' or 'H'")

        return cast(Literal["L", "H"], value)

    @computed_field
    @property
    def attack_complexity(self) -> Literal["Low", "High"]:
        match self.attack_complexity_raw:
            case "L":
                return "Low"
            case "H":
                return "High"
            case _:
                typing.assert_never(self.attack_complexity_raw)

    @computed_field
    @property
    def privileges_required(self) -> Literal["None", "Low", "High"]:
        return self._nlh(self.privileges_required_raw)

    @computed_field
    @property
    def user_interaction(self) -> Literal["None", "Required"]:
        match self.user_interaction_raw:
            case "N":
                return "None"
            case "R":
                return "Required"
            case _:
                typing.assert_never(self.user_interaction_raw)

    @computed_field
    @property
    def scope(self) -> Literal["Unchanged", "Changed"]:
        match self.scope_raw:
            case "U":
                return "Unchanged"
            case "C":
                return "Changed"
            case _:
                typing.assert_never(self.scope_raw)

    @computed_field
    @property
    def confidentiality(self) -> Literal["None", "Low", "High"]:
        return self._nlh(self.confidentiality_raw)

    @computed_field
    @property
    def integrity(self) -> Literal["None", "Low", "High"]:
        return self._nlh(self.integrity_raw)

    @computed_field
    @property
    def availability(self) -> Literal["None", "Low", "High"]:
        return self._nlh(self.availability_raw)


def create_cvss_v3_1(score: str) -> CVSS_V3_1:
    components = score.split("/")[1:]

    return CVSS_V3_1(
        attack_vector_raw=components[0].split(":")[1],  # pyright: ignore[reportGeneralTypeIssues]
        attack_complexity_raw=components[1].split(":")[1],  # pyright: ignore[reportGeneralTypeIssues]
        privileges_required_raw=components[2].split(":")[1],  # pyright: ignore[reportGeneralTypeIssues]
        user_interaction_raw=components[3].split(":")[1],  # pyright: ignore[reportGeneralTypeIssues]
        scope_raw=components[4].split(":")[1],  # pyright: ignore[reportGeneralTypeIssues]
        confidentiality_raw=components[5].split(":")[1],  # pyright: ignore[reportGeneralTypeIssues]
        integrity_raw=components[6].split(":")[1],  # pyright: ignore[reportGeneralTypeIssues]
        availability_raw=components[7].split(":")[1],  # pyright: ignore[reportGeneralTypeIssues]
    )


class OSVSeverity(OSVBaseModel):
    type: str
    score: str


class OSVPackage(OSVBaseModel):
    # NOTE: pyright doesn't like the `Literal[OSV_ECOSYSTEMS]`
    ecosystem: Literal[
        "Go",
        "npm",
        "OSS-Fuzz",
        "PyPI",
        "RubyGems",
        "crates.io",
        "Packagist",
        "Maven",
        "NuGet",
        "Linux",
        "Debian",
        "Alpine",
        "Hex",
        "Android",
        "GitHub Actions",
        "Pub",
        "ConanCenter",
        "Rocky Linux",
        "AlmaLinux",
    ] = Field(description="Overall library ecosystem (choices defined in OSV spec)")

    name: str = Field(
        description="String identifying the library within its ecosystem",
    )

    purl: str | None = Field(
        default=None,
        description="Package URL specification that identifies the package",
    )


class OSVEvent(OSVBaseModel):
    introduced: str | None = None
    fixed: str | None = None
    last_affected: str | None = None
    limit: str | None = None


class OSVRange(OSVBaseModel):
    type: str
    repo: str | None = None
    events: list[OSVEvent]


class OSVAffected(OSVBaseModel):
    package: OSVPackage
    severity: list[OSVSeverity] | None = None
    ranges: list[OSVRange]
    versions: list[str] | None = None
    ecosystem_specific: dict[str, Any] | None = None
    database_specific: dict[str, Any] | None = None


class OSVReference(OSVBaseModel):
    type: str
    url: str


class OSVCredit(OSVBaseModel):
    name: str
    contact: list[str]
    type: list[str]


# TODO: Validate that schema_version is >=1
class OpenSourceVulnerability(OSVBaseModel):
    schema_version: str
    id: str
    modified: str
    published: str
    aliases: list[str]
    summary: str
    details: str
    severity: list[OSVSeverity]
    affected: list[OSVAffected]
    references: list[OSVReference]
    database_specific: dict[str, Any]
    withdrawn: str | None = None
    related: list[str] | None = None
    credits: list[OSVCredit] | None = None

    @computed_field(return_type=bool, repr=False)
    @property
    def github_reviewed(self) -> bool:
        return self.database_specific.get("github_reviewed") or False

    @computed_field(return_type=list[str], repr=False)
    @property
    def cwes(self) -> list[str]:
        return self.database_specific.get("cwe_ids") or []

    @computed_field(return_type=list[str], repr=False)
    @property
    def urls(self) -> list[str]:
        return [r.url for r in self.references]

    @computed_field(return_type=CVSS_V3_1 | None, repr=False)
    @property
    def cvss_v3_1(self) -> CVSS_V3_1 | None:
        if match := [x for x in self.severity if x.type == "CVSS_V3"]:
            if len(match) == 1:
                return create_cvss_v3_1(match[0].score)
            else:
                raise Exception(f"Too many CVSS v3 matches for {self.id}: {len(match)}")
        else:
            return None


def get_ghsa_data(ghsa_id: str) -> str:
    return (base_dir / f"{ghsa_id}/{ghsa_id}.json").expand().abspath().read_text()


base_dir = Path("~/git/advisory-database/advisories/github-reviewed/2023/03/").expand().abspath()  # pyright: ignore[reportAttributeAccessIssue]

console = Console()
vuln = OpenSourceVulnerability.model_validate_json(get_ghsa_data("GHSA-xw5p-hw8j-xg4q"))
console.print_json(vuln.model_dump_json(), indent=2)

for p in base_dir.dirs():
    ghsa_id = str(p.basename())
    try:
        OpenSourceVulnerability.model_validate_json(get_ghsa_data(ghsa_id))
    except ValidationError as e:
        logger.opt(exception=e).error(f"Failed to validate {ghsa_id}")


# TODO: Avoid withdrawn/rejected CVEs
def search(vuln: OpenSourceVulnerability, keyword: str):
    if keyword in vuln.summary:
        ...

    if keyword in vuln.details:
        ...

    # NOTE: This is for multiple version ranges, e.g. Grafana 7 - 8.15 and Grafana 9 - 9.2
    affected_packages = [a.package for a in vuln.affected]

    for package in affected_packages:
        # TODO: This one must be in a specific list
        if keyword in package.ecosystem:
            ...

        if keyword in package.name:
            ...

        # TODO: This one is special
        if package.purl and keyword in package.purl:
            ...

    if keyword in vuln.summary:
        ...


def version_check(vuln: OpenSourceVulnerability, keyword: str):
    # TODO: affected[].ranges.[].type
    affected_packages = [a.ranges for a in vuln.affected]
