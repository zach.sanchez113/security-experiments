#!/usr/bin/env bash

target_ip="10.1.1.1"
target_port="80"

nc -v -z -w 3 $target_ip $target_port

socat /dev/null TCP:$target_ip:$target_port,connect-timeout=3

# Forward to local port
nc -l -p 8001 -c "nc 127.0.0.1 8000"
socat tcp-listen:8001,reuseaddr,fork tcp:localhost:8000
