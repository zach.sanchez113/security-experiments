#!/usr/bin/env bash

################################################################################
#
# Set up a new RockyLinux 8.5 VM in VirtualBox
#
# TODO: Support more VMs/customization
#
# References:
# https://www.virtualbox.org/manual/ch08.html
# https://andreafortuna.org/2019/10/24/how-to-create-a-virtualbox-vm-from-command-line/
# https://www.golinuxcloud.com/configure-nat-port-forwarding-virtualbox-cli/
#
################################################################################


##################################################
#
# Utility function
#
##################################################

# Check if command exists
#
# Args:
#   command to check existence of
#
function check_cmd() {
  command -v "${1:?no command supplied}" > /dev/null 2>&1
}


##################################################
#
# Pre-flight checks
#
##################################################

MACHINENAME="${1}"

if [ "$MACHINENAME" == "" ]; then

  # Not needed, but it looks pretty
  if check_cmd gum; then
    MACHINENAME=$(
      gum input \
      --prompt "What would you like to name your VM? " \
      --prompt.foreground "#04B575" \
      --cursor.foreground "#F4AC45"
    )

  else
    read -rp "What would you like to name your VM? " MACHINENAME

  fi

fi

if vboxmanage list vms | grep -q "$MACHINENAME"; then
  echo -e "\nThat VM already exists! Aborting." 1>&2
  exit 1
fi

mkdir -pv "${HOME}/ISOs"
mkdir -pv "${HOME}/VirtualBox VMs"


##################################################
#
# Download the ISO
#
##################################################

# Download Rocky Linux ISO
if [ ! -f "${HOME}/ISOs/Rocky-8.5-x86_64-dvd1.iso" ]; then
    wget -P "${HOME}/ISOs" https://download.rockylinux.org/pub/rocky/8/isos/x86_64/Rocky-8.5-x86_64-dvd1.iso
fi


##################################################
#
# Create the VM, perform basic setup
#
# For OS type:
# vboxmanage list ostypes | grep "^ID:" | awk '{ print $2 }' | sort
#
##################################################

# Create VM
VBoxManage createvm \
  --name "$MACHINENAME" \
  --ostype "RedHat_64" \
  --register \
  --basefolder "${HOME}/VirtualBox VMs"

# Set memory and network
# ! Do not use natnetwork - that's for multiple machines, not one
VBoxManage modifyvm "$MACHINENAME" \
  --ioapic on \
  --cpus 2 \
  --memory 6442 \
  --vram 16 \
  --graphicscontroller VMSVGA \
  --nic1 nat

declare -a port_forwarding_rules=(
  "SSH,tcp,,2222,,22"
  "Webserver,tcp,,8000,,8000"
)

# NOTE: --nat-pf1 isn't correct in newer versions of VBoxManage, but including just in case
for rule in "${port_forwarding_rules[@]}"; do
  VBoxManage modifyvm "$MACHINENAME" --nat-pf1 "${rule}" 2>/dev/null \
  || VBoxManage modifyvm "$MACHINENAME" --natpf1 "${rule}"
done

# Make sure the clipboard is bidirectional
VBoxManage modifyvm "$MACHINENAME" --clipboard-mode bidirectional


##################################################
#
# Set up the disks
#
##################################################

# Create Disk (size: 75GB)
VBoxManage createhd \
  --filename "${HOME}/VirtualBox VMs/$MACHINENAME/$MACHINENAME_DISK.vdi" \
  --size 78643200 \
  --format VDI \
  --variant Standard

# Attach disk to VM
VBoxManage storagectl "$MACHINENAME" \
  --name "SATA Controller" \
  --add sata \
  --controller IntelAhci

VBoxManage storageattach "$MACHINENAME" \
  --storagectl "SATA Controller" \
  --port 0 \
  --device 0 \
  --type hdd \
  --medium "${HOME}/VirtualBox VMs/$MACHINENAME/$MACHINENAME_DISK.vdi"

# Attach the ISO
VBoxManage storagectl "$MACHINENAME" \
  --name "IDE Controller" \
  --add ide \
  --controller PIIX4

VBoxManage storageattach "$MACHINENAME" \
  --storagectl "IDE Controller" \
  --port 1 \
  --device 0 \
  --type dvddrive \
  --medium "${HOME}/ISOs/Rocky-8.5-x86_64-dvd1.iso"

VBoxManage modifyvm "$MACHINENAME" \
  --boot1 dvd \
  --boot2 disk \
  --boot3 none \
  --boot4 none


##################################################
#
# Start the VM
#
##################################################

VBoxManage startvm "$MACHINENAME" --type gui
