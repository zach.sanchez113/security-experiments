#!/usr/bin/env python3

"""Script to test logins + privileges for an Oracle DB.

Notes:
    * The ConnectParams class may be nice for preventing ugliness. TBD.
    * To spin up a test database:

        podman pull container-registry.oracle.com/database/free:latest
        podman run -d --name oracle-db -p 1521:1521 -e ORACLE_PWD=root container-registry.oracle.com/database/free:latest

      * TODO: Podman has secret storage?? `printf "<Your Password>" | podman secret create oracle_pwd -`

      * To connect to the database:

        podman exec -it oracle-db sqlplus / as sysdba
        podman exec -it oracle-db sqlplus sys/root@FREE as sysdba
        podman exec -it oracle-db sqlplus system/root@FREE
        podman exec -it oracle-db sqlplus pdbadmin/root@FREEPDB1

      * NOTE: PDB -> Pluggable Database

    * Admin accounts/privileges:

      * References:
        * https://dba.stackexchange.com/a/408
        * https://docs.oracle.com/cd/B16351_01/doc/server.102/b14196/users_secure002.htm
        * https://asktom.oracle.com/pls/apex/f?p=100:11:0%3a%3a%3a%3aP11_QUESTION_ID:2659418700346202574

      * Note: Oracle highly suggests that the built-in admin accounts should only be used rarely.

      * SYS
        * This account can perform all administrative functions.

        * The SYS user is granted the SYSDBA privilege, which enables a user to perform high-level
          administrative tasks such as backup and recovery.

        * Tables in the SYS schema are manipulated only by the database. They should never be modified by any
          user or database administrator, and no one should create any tables in the schema of user SYS.
          Database users should not connect to the Oracle database using the SYS account.

          * From a comment: "In practice, every DBA does sqlplus / as sysdba for day-to-day work - in 15 years
            working with dozens of experienced DBAs I've never come across anyone who bothers with the SYSTEM
            account." Well then...

      * SYSTEM
        * This account can perform all administrative functions except for backup/recovery and database
          upgrades. In other words, while this account has the DBA role, it is NOT granted the SYSDBA privilege.

        * While this account can be used to perform day-to-day administrative tasks, Oracle strongly
          recommends creating named users account for administering the Oracle database to enable monitoring
          of database activity.

      * Privileges

        * SYSDBA
          * The SYSDBA system privilege is for fully empowered database administrators. It's basically like
            root on Linux.

        * SYSOPER
          * The SYSOPER system privilege allows a user to perform basic operational tasks, but without the
            ability to look at user data.

        * When you connect with the SYSDBA or SYSOPER privilege, you connect with a default schema, not with the
          schema that is generally associated with your user name. For SYSDBA this schema is SYS; for SYSOPER
          the schema is PUBLIC.

Examples:
    * DSN with port: user/password@dbhost.example.com:1521/orclpdb
    * DSN without port: user/password@dbhost.example.com/orclpdb
    * DSN without credentials or port: dbhost.example.com/orclpdb
    * JDBC connection string with a service name: jdbc:oracle:thin:@dbhost.example.com:1521/orclpdb
    * JDBC connection string with a SID: jdbc:oracle:thin:@dbhost.example.com:1521:orcl
"""

from __future__ import annotations

import os
import sys
from typing import Any

import oracledb
import rich_click as click
from loguru import logger
from oracledb.exceptions import DatabaseError
from rich.panel import Panel
from rich.table import Table

# Config for rich-click
click.rich_click.STYLE_HELPTEXT = ""
click.rich_click.SHOW_ARGUMENTS = True
click.rich_click.SHOW_METAVARS_COLUMN = False
click.rich_click.APPEND_METAVARS_HELP = True
click.rich_click.STYLE_OPTIONS_TABLE_PAD_EDGE = True
click.rich_click.COMMAND_GROUPS = {}

# Grab the Rich console
console = click.rich_click._get_rich_console()

# Base logging configuration
LOG_FORMAT = "<green>{time:YYYY-MM-DD HH:mm:ss}</green> | <level>{level: <8}</level> | <level>{message}</level>"  # fmt: skip

# Set `diagnose=False` because the tracebacks for Oracle errors are pretty verbose
logger.configure(
    handlers=[
        dict(
            sink=sys.stdout,
            level=os.environ.get("LOGURU_LEVEL", "INFO"),
            format=LOG_FORMAT,
            diagnose=False,
        ),
    ],
    levels=[
        {"name": "INFO", "color": "<blue><bold>"},
        {"name": "DEBUG", "color": "<magenta><bold>"},
    ],
)


class LoguruCatchGroup(click.RichGroup):
    """Subclass of RichGroup that wraps commands in loguru's `logger.catch` context manager."""

    def __call__(self, *args: Any, **kwargs: Any):
        with logger.catch(onerror=lambda e: sys.exit(1), exclude=(click.ClickException, click.Abort)):
            return super().__call__(*args, **kwargs)


class LoguruCatchCommand(click.RichCommand):
    """Subclass of RichCommand that wraps commands in loguru's `logger.catch` context manager."""

    def __call__(self, *args: Any, **kwargs: Any):
        with logger.catch(onerror=lambda e: sys.exit(1), exclude=(click.ClickException, click.Abort)):
            return super().__call__(*args, **kwargs)


@click.group(cls=LoguruCatchGroup, context_settings=dict(help_option_names=["-h", "--help"]))
def cli():
    """Oracle DB fun.

    \b
    TODO: Add an option for authenticating as SYSBDA or SYSOPER.
    """


@cli.command(
    no_args_is_help=True,
    context_settings=dict(show_default=True),
)
@click.option(
    "-u",
    "--username",
    type=str,
    required=True,
    help="Username to test.",
)
@click.option(
    "--password",
    type=str,
    prompt=True,
    hide_input=True,
    help="Password to test.",
)
@click.option(
    "-d",
    "--dsn",
    type=str,
    default="",
    help="DSN (Data Source Name) to connect with.",
)
@click.option(
    "-h",
    "--host",
    type=str,
    default="",
    help="Host to connect to. Ignored if DSN is provided.",
)
@click.option(
    "-p",
    "--port",
    type=int,
    default=1521,
    help="Port to connect to. Ignored if DSN is provided.",
)
@click.option(
    "-n",
    "--service-name",
    type=str,
    default="",
    help="Service to connect to. Ignored if DSN is provided.",
)
@click.option(
    "-s",
    "--sid",
    type=str,
    default="",
    help="SID (System Identifier) to connect to. Ignored if DSN is provided.",
)
@click.option(
    "--debug",
    is_flag=True,
    help="Enable debug logging.",
)
def test_login(
    username: str,
    password: str,
    dsn: str = "",
    host: str = "",
    port: int = 1521,
    service_name: str = "",
    sid: str = "",
    debug: bool = False,
):
    """Test credentials to an Oracle DB.

    \b
       ---------------------------------

    For reference, a connection string is of the format: $username/$password@$host:$port/$service_name

    Or for a SID, e.g. in a JDBC connection string: jdbc:oracle:thin:@$host:$port:$sid
    """

    if debug:
        logger.remove()
        logger.add(sink=sys.stdout, level="DEBUG", format=LOG_FORMAT)

    try:
        oracledb.init_oracle_client()
    except Exception as e:
        logger.opt(exception=e).debug("Failed to load Oracle client library")
        logger.warning(
            "Failed to load the Oracle client library! While not required, some functionality may be missing."
        )
        logger.warning(
            "To install the Oracle Instant Client, visit: https://www.oracle.com/database/technologies/instant-client/downloads.html"
        )

    if not dsn:
        missing_args = []

        if not host:
            missing_args.append("--host")  # cls=LoguruCatchCommand,

        if not port:
            missing_args.append("--port")
        if not any((service_name, sid)):
            missing_args.append("--service-name/--sid")

        if missing_args:
            raise click.ClickException(
                f"Missing the following connection parameters: {', '.join(missing_args)}"
            )

    if all((service_name, sid)):
        raise click.ClickException("Only one of --service-name and --sid can be provided")

    # TODO: Add an option for this
    auth_mode = oracledb.AUTH_MODE_DEFAULT
    if username == "sys":
        auth_mode = oracledb.AUTH_MODE_SYSDBA

    try:
        with oracledb.connect(
            user=username,
            password=password,
            dsn=dsn,
            host=host,
            port=port,
            service_name=service_name,
            sid=sid,
            mode=auth_mode,
        ):
            logger.success("Connection was successful!")

    except DatabaseError as e:
        logger.opt(exception=e).debug("Encountered error while connecting to Oracle DB")
        logger.error(f"Failed to connect! {e}")

        if not dsn and "DPY-4027" in str(e):
            logger.error(f"Try passing in a DSN instead with '-d {host}:{port}/{service_name}'")

    if username != "sys":
        try:
            with oracledb.connect(
                user=username,
                password=password,
                dsn=dsn,
                host=host,
                port=port,
                service_name=service_name,
                sid=sid,
                mode=oracledb.AUTH_MODE_SYSOPER,
            ):
                # This isn't particularly dangerous since it basically just allows starting and stopping the
                # DB, but it's a warning sign that perms could be off elsewhere.
                logger.info(
                    f"User {username!r} can connect with the SYSOPER role! "
                    + "This isn't particularly dangerous, but it may be a warning sign of shoddy permissions elsewhere."
                )

        except DatabaseError as e:
            ...

        try:
            with oracledb.connect(
                user=username,
                password=password,
                dsn=dsn,
                host=host,
                port=port,
                service_name=service_name,
                sid=sid,
                mode=oracledb.AUTH_MODE_SYSDBA,
            ):
                logger.warning(
                    f"User {username!r} can connect with the SYSDBA role! "
                    + "Only SYS should be able to do this..."
                )

        except DatabaseError as e:
            ...


@cli.command(
    no_args_is_help=True,
    context_settings=dict(show_default=True),
)
@click.option(
    "-u",
    "--username",
    type=str,
    required=True,
    help="Username to test.",
)
@click.option(
    "--password",
    type=str,
    prompt=True,
    hide_input=True,
    help="Password to test.",
)
@click.option(
    "-d",
    "--dsn",
    type=str,
    default="",
    help="DSN (Data Source Name) to connect with.",
)
@click.option(
    "-h",
    "--host",
    type=str,
    default="",
    help="Host to connect to. Ignored if DSN is provided.",
)
@click.option(
    "-p",
    "--port",
    type=int,
    default=1521,
    help="Port to connect to. Ignored if DSN is provided.",
)
@click.option(
    "-n",
    "--service-name",
    type=str,
    default="",
    help="Service to connect to. Ignored if DSN is provided.",
)
@click.option(
    "-s",
    "--sid",
    type=str,
    default="",
    help="SID (System Identifier) to connect to. Ignored if DSN is provided.",
)
@click.option(
    "--debug",
    is_flag=True,
    help="Enable debug logging.",
)
def get_privileges(
    username: str,
    password: str,
    dsn: str = "",
    host: str = "",
    port: int = 1521,
    service_name: str = "",
    sid: str = "",
    debug: bool = False,
):
    """Get privileges and roles for a user in an Oracle DB.

    \b
       ----------------------------------------------------

    Specifically, this will dump the tables USER_TAB_PRIVS, USER_SYS_PRIVS, and USER_ROLE_PRIVS in that order.

    \n
    NOTE: Output is limited to the first 100 rows for USER_TAB_PRIVS since the 'sys' user can have tens of
    thousands of rows in that table.

    TODO: Add an option for full output.

    \n
    Reference: https://stackoverflow.com/a/15066503/11832705
    """

    if debug:
        logger.remove()
        logger.add(sink=sys.stdout, level="DEBUG", format=LOG_FORMAT)

    try:
        oracledb.init_oracle_client()
    except Exception as e:
        logger.opt(exception=e).debug("Failed to load Oracle client library")
        logger.warning(
            "Failed to load the Oracle client library! While not required, some functionality may be missing."
        )
        logger.warning(
            "To install the Oracle Instant Client, visit: https://www.oracle.com/database/technologies/instant-client/downloads.html"
        )

    if not dsn:
        missing_args = []

        if not host:
            missing_args.append("--host")
        if not port:
            missing_args.append("--port")
        if not any((service_name, sid)):
            missing_args.append("--service-name/--sid")

        if missing_args:
            raise click.ClickException(
                f"Missing the following connection parameters: {', '.join(missing_args)}"
            )

    if all((service_name, sid)):
        raise click.ClickException("Only one of --service-name and --sid can be provided")

    # TODO: Add an option for this
    auth_mode = oracledb.AUTH_MODE_DEFAULT
    if username == "sys":
        auth_mode = oracledb.AUTH_MODE_SYSDBA

    with oracledb.connect(
        user=username,
        password=password,
        dsn=dsn,
        host=host,
        port=port,
        service_name=service_name,
        sid=sid,
        mode=auth_mode,
    ) as connection:
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM USER_SYS_PRIVS ORDER BY PRIVILEGE")
            user_sys_privs_columns = [col[0] for col in cursor.description]
            cursor.rowfactory = lambda *args: dict(zip(user_sys_privs_columns, args))
            user_sys_privs = cursor.fetchall()
            logger.debug(f"{len(user_sys_privs) = }")

            cursor.execute("SELECT * FROM USER_TAB_PRIVS ORDER BY TABLE_NAME")
            user_tab_privs_columns = [col[0] for col in cursor.description]
            cursor.rowfactory = lambda *args: dict(zip(user_tab_privs_columns, args))
            user_tab_privs = cursor.fetchall()[0:100]
            logger.debug(f"{len(user_tab_privs) = }")

            cursor.execute("SELECT * FROM USER_ROLE_PRIVS ORDER BY GRANTED_ROLE")
            user_role_privs_columns = [col[0] for col in cursor.description]
            cursor.rowfactory = lambda *args: dict(zip(user_role_privs_columns, args))
            user_role_privs = cursor.fetchall()
            logger.debug(f"{len(user_role_privs) = }")

    user_sys_privs_table = Table(*user_sys_privs_columns, expand=True)
    for row in user_sys_privs:
        values = [row[c] for c in user_sys_privs_columns]
        user_sys_privs_table.add_row(*values)

    user_tab_privs_table = Table(*user_tab_privs_columns, expand=True)
    for row in user_tab_privs:
        values = [row[c] for c in user_tab_privs_columns]
        user_tab_privs_table.add_row(*values)

    user_role_privs_table = Table(*user_role_privs_columns, expand=True)
    for row in user_role_privs:
        values = [row[c] for c in user_role_privs_columns]
        user_role_privs_table.add_row(*values)

    console.print(
        Panel(
            user_tab_privs_table,
            title="USER_TAB_PRIVS (object grants for which the current user is the object owner, grantor, or grantee)",
            title_align="left",
            subtitle="USER_TAB_PRIVS (object grants for which the current user is the object owner, grantor, or grantee)",
            subtitle_align="left",
            expand=True,
            border_style="yellow",
        )
    )

    console.print()

    console.print(
        Panel(
            user_sys_privs_table,
            title="USER_SYS_PRIVS (system privileges granted to the current user)",
            title_align="left",
            subtitle="USER_SYS_PRIVS (system privileges granted to the current user)",
            subtitle_align="left",
            expand=True,
            border_style="blue",
        )
    )

    console.print()

    console.print(
        Panel(
            user_role_privs_table,
            title="USER_ROLE_PRIVS (roles granted to the current user)",
            title_align="left",
            subtitle="USER_ROLE_PRIVS (roles granted to the current user)",
            subtitle_align="left",
            expand=True,
            border_style="green",
        )
    )


if __name__ == "__main__":
    cli()
