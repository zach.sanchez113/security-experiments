#!/usr/bin/env awk -f -F '\t'

BEGIN { OFS="\t"; }
{
  # Extract host from
  split($1, host, " ");

  split($2, l2, ": ");
  split(l2[2], ports, ", ");

  for(i = 1; i <= length(ports); i++) {
    print host[2], ports[i];
  }
}
