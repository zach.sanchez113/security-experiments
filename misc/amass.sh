#!/usr/bin/env bash

# Subdomain enum
amass enum -d example.com

# Turn off local DB
amass enum -d example.com -nolocaldb

# DNS enum (more params)
# -src -> show data source
# -min-for-recursive -> subdomain labels seen before recursive brute forcing
amass enum -d example.com -v -src -ip -brute -min-for-recursive 2 \
    -p 80,443 \
    -json out.json \
    -max-depth 3 \
    -w wordlist.txt
