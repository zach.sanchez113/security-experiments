"""[WIP] Search strings for base64, print if human-readable."""

from __future__ import annotations

import base64
import re

# Search strings for base64, print if human-readable

# https://stackoverflow.com/a/8571649/11832705
B64_PATTERN = re.compile(r"(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)?", flags=re.IGNORECASE)

text = ""

for line in text.split():
    matches: list[str] = B64_PATTERN.findall(line)

    for match in matches:
        txt: str = base64.b64decode(match, validate=True).decode("utf-8")

        if txt.isprintable():
            print(f"Found printable base64: {txt} <- {match}")
