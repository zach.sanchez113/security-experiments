#!/usr/bin/env python3

"""Script to extract text from scanned PDFs.

Credit: https://ploomber.io/blog/pdf-ocr/

Prerequisites:

- Install Tesseract OCR engine: https://tesseract-ocr.github.io/tessdoc/Installation.html
- Install languages for Tesseract.
    - Search for them in your package manager. For example:
        - tesseract-ocr-eng (English)
        - tesseract-ocr-spa (Spanish)
"""

from __future__ import annotations

from typing import cast

import pytesseract
from path import Path
from pdf2image import convert_from_path

pdf_path = Path("/path/to/File.pdf")
language_code = "eng"

# Convert to image using resolution 600 dpi
pages = convert_from_path(pdf_path, 600)

# Extract text
text_data = ""
for page in pages:
    text: str = cast(str, pytesseract.image_to_string(page, lang=language_code))
    text_data += text + "\n"

print(text_data.strip())
