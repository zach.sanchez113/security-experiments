#!/usr/bin/env python3

"""Module-level docstring (if desired)."""

from __future__ import annotations

import csv
import json
import os
import re
import sys

import rich_click as click
from loguru import logger
from path import Path

# Config for rich-click
click.rich_click.SHOW_ARGUMENTS = True
click.rich_click.SHOW_METAVARS_COLUMN = False
click.rich_click.APPEND_METAVARS_HELP = True
click.rich_click.STYLE_OPTIONS_TABLE_PAD_EDGE = True
click.rich_click.COMMAND_GROUPS = {}

# Grab the Rich console
console = click.rich_click._get_rich_console()

# Base logging configuration
LOG_FORMAT = (
    "<green>{time:YYYY-MM-DD HH:mm:ss}</green> | <level>{level: <8}</level> | <level>{message}</level>"
)

logger.configure(
    handlers=[
        dict(sink=sys.stdout, level=os.environ.get("LOGURU_LEVEL", "INFO"), format=LOG_FORMAT),
    ],
    levels=[
        {"name": "INFO", "color": "<blue><bold>"},
        {"name": "DEBUG", "color": "<magenta><bold>"},
    ],
)


class LoguruCatchGroup(click.RichGroup):
    """Subclass of RichGroup that wraps commands in loguru's `logger.catch` context manager."""

    def __call__(self, *args, **kwargs):
        with logger.catch(onerror=lambda e: sys.exit(1), exclude=(click.ClickException, click.Abort)):
            return super().__call__(*args, **kwargs)


class LoguruCatchCommand(click.RichCommand):
    """Subclass of RichCommand that wraps commands in loguru's `logger.catch` context manager."""

    def __call__(self, *args, **kwargs):
        with logger.catch(onerror=lambda e: sys.exit(1), exclude=(click.ClickException, click.Abort)):
            return super().__call__(*args, **kwargs)


@click.command(
    cls=LoguruCatchCommand,
    no_args_is_help=True,
    context_settings=dict(
        show_default=True,
        help_option_names=["-h", "--help"],
    ),
)
@click.argument("gnmap_file", type=click.Path(exists=True, dir_okay=False, path_type=Path))
@click.option("--debug", is_flag=True, help="Enable debug logging.")
def cli(gnmap_file: Path, debug: bool = False):
    """Convert a gnmap file to JSON."""

    if debug:
        logger.remove()
        logger.add(sink=sys.stdout, level="DEBUG", format=LOG_FORMAT)

    results = {}

    with open(gnmap_file, "r") as f:
        for row in csv.reader(gnmap_file, delimiter="\t"):
            if len(row) == 2:
                continue

            host = re.sub(r"^Host: ([\d]{3}\.[\d]{3}\.[\d]{3}\.[\d]{3}) \(\)", r"\1", row[0].strip())
            results[host] = []

            ports = re.sub(r"^Ports: (.*)", r"\1", row[1].strip()).split(", ")
            for port in ports:
                if (num_parts := port.count("//")) != 3:
                    raise click.ClickException(f"Found 3 parts to split on port info, found {num_parts}!")

                p1, name, version = port.rstrip("/").split("//")
                number, status, proto = p1.split("/")

                results[host].append(
                    {
                        "number": number,
                        "status": status,
                        "protocol": proto,
                        "name": name,
                        "version": version,
                    }
                )

    logger.info("Writing JSON...")
    with open(Path(f"{gnmap_file}.json").expand(), "w") as f:
        json.dump(results, f, default=str, indent=2)


if __name__ == "__main__":
    cli()
