# Miscellaneous/Uncategorized Scripts

Everything that doesn't quite fit with the rest.

## amass.sh

Basic reference point for amass usage.

## gnmap-host-port.awk

Extract host/port combos from gnmap output.

More for being able to say I've written a script in awk than anything.

## gnmap-to-json.py

Script to convert a gnmap TSV to JSON for easier automation.

## jq.sh

Because jq is ridiculous.

## linguist.ps1

Script that can run a payload in either PowerShell or bash. For the truly lazy pentesters.

## nmap-ports.sh

Common(-ish) ports I like to scan at the start.

## offline-utils.sh

Download static binaries of all of the terminal utilities that I enjoy.

## port-scan.sh

Alternative methods of checking whether a port is open or not.

## ssh.sh

Useful SSH commands.
