"""Variation of kraken_decryptor.py that uses esedbexport instead of impacket's libraries.

It's still a WIP, but someday I'll get around to finishing this.
"""

from __future__ import annotations

import datetime
import hashlib
import json
import re
import shutil
import subprocess
import uuid
from binascii import hexlify, unhexlify
from collections import OrderedDict
from typing import Any

import pandas as pd
from binary_fun import *
from Cryptodome.Cipher import ARC4, DES
from impacket.examples.secretsdump import LocalOperations
from ldap3.protocol.formatters.formatters import format_ad_timestamp
from loguru import logger
from passlib.handlers.windows import lmhash, nthash
from path import Path

crypto_common = CryptoCommon()
EMPTY_LM_HASH = lmhash.hash("")
EMPTY_NTLM_HASH = nthash.hash("")


def convert_json(obj: Any):
    """A sane function for json.dump(s)' `default` to convert Python data types to JSON-compatible ones."""

    if isinstance(obj, (set, tuple)):
        return list(obj)
    if isinstance(obj, OrderedDict):
        return dict(obj)
    if isinstance(obj, datetime.datetime):
        return obj.replace(microsecond=0).isoformat()
    if isinstance(obj, bytes):
        try:
            return obj.decode(encoding="utf-8")
        except UnicodeDecodeError:
            return repr(obj)

    try:
        return str(obj)
    except:
        return repr(obj)


def generate_esent_attr_map(orig_df: pd.DataFrame) -> dict:
    """Generate mapping from internal ESENT attributes to (relatively) human-readable LDAP attributes.

    TODO: Generate description on demand?
    """

    esent_ldap_attr_name = "ATTm131532"
    esent_attr_id = "ATTc131102"

    df = orig_df.copy(deep=True)
    df = df.loc[(df[esent_ldap_attr_name].notna()) & (df[esent_attr_id].notna())]
    df = df[[esent_ldap_attr_name, esent_attr_id]]

    result = {}

    for row in df.iterrows():
        attr_name, attr_id = row[1][esent_ldap_attr_name], row[1][esent_attr_id]

        matches = [
            re.fullmatch(fr"ATT[a-z]{attr_id}", col)[0]
            for col in orig_df.columns.tolist()
            if re.fullmatch(fr"ATT[a-z]{attr_id}", col)
        ]

        if len(matches) == 1:
            logger.trace(f"Matched {attr_name} with {matches[0]}")
            result[matches[0]] = attr_name
        else:
            logger.trace(f"Found {len(matches)} matches for {attr_name}. Skipping")

    return result


def convert_byte_dict(data: Any) -> Any:
    """Decode bytes, dict w/ bytes, or tuple w/ bytes.

    TODO: Tuple conversion doesn't look quite right
    """
    if isinstance(data, bytes):
        return data.decode("ascii")
    if isinstance(data, dict):
        return dict(map(convert_byte_dict, data.items()))
    if isinstance(data, tuple):
        return map(convert_byte_dict, data)

    return data


class KrakenDecryptor:
    @logger.catch(reraise=True)
    def __init__(
        self,
        base_dir: Path = Path("./").expand(),  # type: ignore
    ):
        self.hash_dir = (base_dir / str(uuid.uuid4())).expand()
        self.workdir = (self.hash_dir / f"/workdir/").makedirs_p()
        self.output_file = self.hash_dir / "output.json"

        logger.debug(f"{self.hash_dir = }")
        logger.debug(f"{self.workdir = }")
        logger.debug(f"{self.output_file = }")

        self.pek_list = list()

    def __removeRC4Layer(self, crypted_hash: CRYPTED_HASH) -> bytes:
        """Remove the RC4 layer of encryption.

        Args:
            crypted_hash (CRYPTED_HASH): Encrypted hash.

        Returns:
            bytes: The resulting plaintext.
        """

        md5 = hashlib.new("md5")
        # PEK index can be found on header of each ciphered blob (pos 8-10)
        pek_index = hexlify(crypted_hash["Header"])
        md5.update(self.pek_list[int(pek_index[8:10])])
        md5.update(crypted_hash["KeyMaterial"])
        rc4 = ARC4.new(md5.digest())
        plaintext = rc4.encrypt(crypted_hash["EncryptedHash"])
        return plaintext

    def __removeDESLayer(self, crypted_hash: bytes, rid: str) -> bytes:
        """Remove the DES layer of encryption.

        Args:
            crypted_hash (bytes): Encrypted hash.
            rid (str): User's relative identifier.

        Returns:
            bytes: The resulting plaintext.
        """

        key1, key2 = CryptoCommon().deriveKey(int(rid))
        crypt1, crypt2 = DES.new(key1, DES.MODE_ECB), DES.new(key2, DES.MODE_ECB)
        decrypted_hash = crypt1.decrypt(crypted_hash[:8]) + crypt2.decrypt(crypted_hash[8:])
        return decrypted_hash

    def decrypt_windows_hash(self, encrypted_hash: str | None, sid: str, default: str = "") -> str:
        """Decrypt a Windows hash.

        TODO: Write up details. Encryption layers:

        For Win2012: DES -> RC4 for the DES hash -> RC4 for the SysKey
        For Win2016: DES -> AES for the DES hash -> AES for the SysKey

        Args:
            encrypted_hash (str | None): Encrypted hash.
            sid (str): User's SID.
            default (str): Default to return if the encrypted hash is None.

        Returns:
            str: The decrypted hash (or the default if applicable).
        """

        if encrypted_hash is None:
            return default

        encrypted_hash_struct = CRYPTED_HASH(unhexlify(encrypted_hash))

        # Win2016 TP4 decryption is different
        if encrypted_hash_struct["Header"][:4] == b"\x13\x00\x00\x00":
            encrypted_hash_struct = CRYPTED_HASHW16(unhexlify(encrypted_hash))

            # PEK index can be found on header of each ciphered blob (pos 8-10)
            pek_index = hexlify(encrypted_hash_struct["Header"])

            intermediate_hash = CryptoCommon().decryptAES(
                self.pek_list[int(pek_index[8:10])],
                encrypted_hash_struct["EncryptedHash"][:16],
                encrypted_hash_struct["KeyMaterial"],
            )

        else:
            intermediate_hash = self.__removeRC4Layer(encrypted_hash_struct)

        rid = SAMR_RPC_SID(unhexlify(sid)).formatCanonical().split("-")[-1]
        decrypted_hash = self.__removeDESLayer(intermediate_hash, rid)

        return hexlify(decrypted_hash).decode("utf-8")

    def validate_record(self, record: dict) -> bool:
        """Ensure that an ESENT record is for a (active) user account.

        Did this have to be a long chain of if/elif/else? No, I just rewrote it that way to make
        the validation logic easier to understand.

        Args:
            record (dict | OrderedDict): Record to validate.

        Returns:
            bool: Whether the record is for a user account.
        """

        uac = record["userAccountControl"]
        sAMAccountName: str = record["sAMAccountName"]

        if not sAMAccountName.isprintable():
            logger.trace(f"Found a non-printable sAMAccountName, skipping: {sAMAccountName!r}")
        elif "$" in sAMAccountName:
            logger.trace(f"Found a machine account, skipping: {sAMAccountName!r}")
        elif "HealthMailbox" in sAMAccountName:
            logger.trace(f"Found an MS Exchange monitoring mailbox, skipping: {sAMAccountName!r}")
        elif "DEL:" in sAMAccountName:
            logger.trace(f"Found a deleted account, skipping: {sAMAccountName!r}")
        elif not isinstance(uac, int):
            logger.trace(f"Found invalid UAC {uac!r}, skipping: {sAMAccountName!r}")
        # ACCOUNTDISABLE -> 0x0002
        elif uac & (1 << 1) != 0:
            logger.trace(f"Found a disabled account, skipping: {sAMAccountName!r}")
        else:
            logger.trace(f"{sAMAccountName!r} is valid!")
            return True

        return False

    def decrypt_password_hashes(self, datatable_records: list[dict]) -> list[dict]:
        """Decrypt password hashes, add 'LMHash' and 'NTLMHash' fields to each user record

        A wrapped/condensed version of NTDSHashes.__decryptHash

        For info on the password hash decryption process, see:
        - https://www.exploit-db.com/docs/english/18244-active-domain-offline-hash-dump-&-forensic-analysis.pdf
        - https://www.insecurity.be/blog/2018/01/21/retrieving-ntlm-hashes-and-what-changed-technical-writeup/
        """

        result = []

        for record in datatable_records:

            # Decrypt password hashes + write as record to JSON if this is an active user
            if self.validate_record(record):
                sAMAccountName: str = record["sAMAccountName"]
                try:
                    logger.debug(f"Processing: {sAMAccountName!r}")

                    logger.debug(f"Converting the LM hash for {sAMAccountName!r}...")
                    record["lmHash"] = self.decrypt_windows_hash(
                        encrypted_hash=record["dBCSPwd"],
                        sid=record["objectSid"],
                        default=EMPTY_LM_HASH,
                    )

                    logger.debug(f"Converting the NTLM hash for {sAMAccountName!r}...")
                    record["ntlmHash"] = self.decrypt_windows_hash(
                        encrypted_hash=record["unicodePwd"],
                        sid=record["objectSid"],
                        default=EMPTY_NTLM_HASH,
                    )

                    # Convert this now so we don't have to worry about it later
                    logger.debug(f"Attempting to convert pwdLastSet for {sAMAccountName!r}...")
                    record["pwdLastSet"] = format_ad_timestamp(record["pwdLastSet"])

                    # Add human-readable active status because it's easier than remembering the
                    # specific condition (ACCOUNTDISABLE -> 0x0002)
                    logger.debug(f"Generating userAccountStatus for {sAMAccountName!r}...")
                    if record["userAccountControl"] & (1 << 1) == 0:
                        record["userAccountStatus"] = "Enabled"
                    else:
                        record["userAccountStatus"] = "Disabled"

                    result.append(record)

                except Exception as e:
                    logger.error(f"Failed to process {sAMAccountName}! (error: {e})")

        return result

    def decrypt_pek_list(self, datatable_records: list[dict], system_file: str) -> None:
        """Decrypt the PEK so we can decrypt the password hashes.

        Ripped from NTDSHashes.__getPek

        For info on the PEK decryption process, see:
        - https://www.exploit-db.com/docs/english/18244-active-domain-offline-hash-dump-&-forensic-analysis.pdf
        - https://www.insecurity.be/blog/2018/01/21/retrieving-ntlm-hashes-and-what-changed-technical-writeup/
        """

        # PEK is encrypted with the BOOTKEY
        #
        # Steps/info:
        # - The Encrypted SysKey needs to be decrypted, to built the RC4 decryption key, we need the Bootkey
        # - This Bootkey is calculated by reordering the 4 Class Names from JD, Skew1, GBG and Data
        # - Reorder using ShiftArray1
        logger.info("Getting the BOOTKEY...")
        bootkey = LocalOperations(system_file).getBootKey()

        # Now let's try to get the PEK
        logger.info("Iterating over table 'datatable' to find the encrypted PEK list...")
        pek_list = None

        for record in datatable_records:
            pek_list = record["pekList"]
            if pek_list:
                encrypted_pek_list = PEKLIST_ENC(pek_list)
                logger.info("Found the encrypted PEK list!")
                break
        else:
            raise Exception("Failed to find the encrypted PEK list!")

        logger.info("Decrypting the PEK list...")

        # PEK header starts this way up to Windows Server 2012 R2
        #
        # First byte of the "F" value (from 'SAM\SAM\Domains\Account'), or the "Header" field,
        # starts with 0x02 when RC4 encryption is used
        #
        # This is more legacy and is what you'll likely find in research papers/blog posts
        # Algorithm (kinda): MD5(F[0x70] + String1 + Bootkey + String2)
        if encrypted_pek_list["Header"][:4] == b"\x02\x00\x00\x00":
            logger.debug("PEK struct header indicates Windows Server 2012 R2")

            logger.debug("Generating the hashed BOOTKEY...")
            md5 = hashlib.new("md5")
            md5.update(bootkey)

            # 1,000 reincarnations of the PEK key material
            logger.debug("Updating the MD5 hash with the PEK key material...")
            for i in range(1000):
                md5.update(encrypted_pek_list["KeyMaterial"])

            logger.debug("Getting the RC4 key...")
            rc4_key = md5.digest()
            logger.trace(f"{rc4_key = }")

            # EncryptUsingRC4(partially encrypted blob, rc4key)
            # This is SystemFunction033 (RtlEncryptDecryptRC4) in AdvApi32.dll
            logger.debug("Decrypting the PEK list...")
            rc4 = ARC4.new(rc4_key)
            decrypted_pek_list = PEKLIST_PLAIN(rc4.encrypt(encrypted_pek_list["EncryptedPek"]))

            # Iterate PEK list entries, store results
            logger.debug("Getting PEKs from list...")

            PEKLen = len(PEK_KEY())
            logger.trace(f"{PEKLen = }")  # when I ran this, PEKLen == 20

            for i in range(len(decrypted_pek_list["DecryptedPek"]) // PEKLen):
                cursor = i * PEKLen
                pek = PEK_KEY(decrypted_pek_list["DecryptedPek"][cursor : cursor + PEKLen])
                self.pek_list.append(pek["Key"])

            logger.success("Decrypted the PEK list!")

        # PEK header starts this way for Windows 2016 Server TP4
        #
        # First byte of the "F" value (from 'SAM\SAM\Domains\Account'), or the "Header" field,
        # starts with 0x03 when AES encryption is used
        #
        # The encrypted PEK seems to be different, but it's actually similar to decrypting LSASS
        # In Windows Server 2016, the RC4 encryption was replaced with AES256 in CBC with IV and zero padding
        elif encrypted_pek_list["Header"][:4] == b"\x03\x00\x00\x00":
            logger.debug("PEK struct header indicates Windows Server 2016 TP4")

            # Decrypt PEK w/ AES
            # Don't need to construct the encryption key anymore - the bootkey suffices now
            #
            # Key - the bootkey
            # CipherText - PEKLIST_ENC['EncryptedPek']
            # IV - PEKLIST_ENC['KeyMaterial']
            logger.debug("Decrypting the PEK list")
            decrypted_pek_list = PEKLIST_PLAIN(
                crypto_common.decryptAES(
                    bootkey,
                    encrypted_pek_list["EncryptedPek"],
                    encrypted_pek_list["KeyMaterial"],
                )
            )

            # I assume this is just some struct magic I don't really need to decode any further
            # than the comment below
            logger.debug("Getting individual PEKs from list...")

            # PEK list entries take the form: index (4 byte LE int), PEK (16 byte key)
            # The entries are in ascending order, and the list is terminated
            # by an entry with a non-sequential index (08080808 observed)
            pos = 0
            cur_index = 0
            while True:
                pek_entry = decrypted_pek_list["DecryptedPek"][pos : pos + 20]

                # Break if PEK is truncated (should not happen)
                if len(pek_entry) < 20:
                    logger.warning("Found entry in PEK list that appears to be truncated!")
                    break

                index, pek = unpack("<L16s", pek_entry)

                # Break on non-sequential index
                if index != cur_index:
                    break

                pos += 20
                cur_index += 1
                self.pek_list.append(pek)

                logger.trace(f"PEK # {index} found and decrypted: {hexlify(pek).decode('utf-8')}")

            logger.success("Decrypted the PEK list!")

        else:
            raise Exception(f"Failed to identify the PEK struct header! Found {encrypted_pek_list['Header'][:4]!r}")

    @classmethod
    def esedbexport(cls, ntds_file: Path, hash_dir: Path) -> list[dict]:
        """Use libesedb's utility 'esedbexport' to create a TSV export of NTDS.dit, which will be
        converted to human-readable JSON.
        """

        esedbexport = shutil.which("esedbexport")
        if not esedbexport:
            raise Exception("esedbexport is not on your PATH!")

        logger.info(f"Exporting {ntds_file} to TSV...")
        result = subprocess.run([f"{esedbexport}", f"{ntds_file}"], shell=True, capture_output=True)
        if result.returncode > 0:
            raise Exception(f"Failed to run esedbexport! (code {result.returncode}, {result.stderr = })")

        export_dir = Path(f"{ntds_file.dirname()}/{ntds_file.basename()}.export").expand()
        if not export_dir.isdir():
            raise Exception(f"{export_dir} is not a directory!")

        datatable = export_dir / Path("datatable.4")
        if not datatable.isfile():
            raise Exception(f"Could not locate the export for 'datatable' at {datatable}!")

        logger.info(f"Done exporting to TSV!")

        # ! WARNING: 'dtype=object' will prevent automatic conversion of types, but will clobber them as well
        logger.info("Reading in export for 'datatable'...")
        df = pd.read_csv(f"{datatable}", sep="\t", encoding="utf-8", dtype=object)
        df = df.reset_index()

        # Dynamically rename columns from internal attribute IDs to LDAP attribute names
        logger.info("Converting 'datatable' attribute IDs into human-readable values...")
        df = df.rename(columns=generate_esent_attr_map(df))

        logger.info("Converting DataFrame to dict...")
        data = df.to_dict(orient="records")

        logger.info("Fixing up types where possible...")
        for row in data:

            # pandas leaves this in and I don't feel like fixing it properly
            if "index" in row:
                del row["index"]

            # This is what I meant by types being clobbered
            # TODO: format_ad_timestamp by attr type
            for k, v in row.items():
                if isinstance(v, str) and v.isdigit():
                    row[k] = int(v)

        logger.info("Writing JSON...")
        datatable_json_file = Path(f"{hash_dir}/datatable.json").expand()
        datatable_json_file.write_text(json.dumps(data, default=str, indent=2))

        logger.info(f"Done! The JSON export for 'datatable' can be found at {datatable_json_file}")

        return data

    @logger.catch(reraise=True)
    def ntds_decrypt(self, ntds_file: str, system_file: str) -> None:
        """Decrypt the NTDS.dit ESE DB, parse out values we care about."""

        Path(self.hash_dir).expand().mkdir_p()

        logger.info("Dumping NTDS.dit...")
        datatable_records = self.esedbexport(Path(ntds_file).expand(), Path(self.hash_dir).expand())

        logger.info("Getting the PEK list...")
        self.decrypt_pek_list(datatable_records, system_file)

        # TODO: Write to file
        logger.info("Decrypting password hashes...")
        user_records = self.decrypt_password_hashes(datatable_records)

        # TODO: Write to file
        logger.info("Generating hash file...")
        hash_lines = [f"{record['sAMAccountName']}:{record['ntlm_hash']}" for record in user_records]

        logger.success(f"Done! Results can be found at {self.output_file!r}")
