use chrono::{DateTime, Utc};
use clap::Parser;
use libesedb::{EseDb, Record, Table, Value};
use regex::Regex;
use serde::{Deserialize, Serialize};
use std::time::{Duration, SystemTime, UNIX_EPOCH};
use std::{collections::HashMap, process::exit};

const ESENT_ATTRIBUTE_ID: &str = "ATTc131102"; // attributeID
const ESENT_LDAP_DISPLAY_NAME: &str = "ATTm131532"; // lDAPDisplayName

// Variant of libesedb Value that also supports serialization
#[derive(Debug, Serialize, Deserialize)]
#[serde(untagged)]
pub enum EseValue {
    Null,
    Bool(bool),
    U8(u8),
    I16(i16),
    I32(i32),
    Currency(i64),
    F32(f32),
    F64(f64),
    DateTime(f64),
    Binary(Vec<u8>),
    Text(String),
    LargeBinary(Vec<u8>),
    LargeText(String),
    SuperLarge(Vec<u8>),
    U32(u32),
    I64(i64),
    /// A 16-byte value
    Guid(Vec<u8>),
    U16(u16),
}

impl EseValue {
    fn _time(&self) -> Option<SystemTime> {
        if let Self::DateTime(value) = self {
            // decimal number of days since 1900
            // https://docs.microsoft.com/en-us/windows/win32/extensible-storage-engine/jet-coltyp
            Some(
                UNIX_EPOCH - Duration::from_secs(60 * 60 * 24 * 25567)
                    + Duration::from_secs_f64((60 * 60 * 24) as f64 * value),
            )
        } else {
            None
        }
    }
}

impl Default for EseValue {
    fn default() -> Self {
        Self::Null
    }
}

impl ToString for EseValue {
    fn to_string(&self) -> String {
        match self {
            EseValue::Null => String::new(),
            EseValue::Bool(x) => x.to_string(),
            EseValue::U8(x) => x.to_string(),
            EseValue::I16(x) => x.to_string(),
            EseValue::I32(x) => x.to_string(),
            EseValue::Currency(x) => x.to_string(),
            EseValue::F32(x) => x.to_string(),
            EseValue::F64(x) => x.to_string(),
            EseValue::DateTime(x) => x.to_string(),
            // Change from original: no extra spacing, that way output is easier to use elsewhere
            EseValue::Binary(b) | EseValue::LargeBinary(b) | EseValue::SuperLarge(b) => {
                b.iter().map(|x| format!("{x:02x}")).collect::<Vec<_>>().join("")
            }
            EseValue::Text(x) => x.to_string(),
            EseValue::LargeText(x) => x.to_string(),
            EseValue::U32(x) => x.to_string(),
            EseValue::I64(x) => x.to_string(),
            EseValue::Guid(b) => {
                format!(
                    "{:02x}{:02x}{:02x}{:02x}-{:02x}{:02x}-{:02x}{:02x}-{:02x}{:02x}-{:02x}{:02x}{:02x}{:02x}{:02x}{:02x}",
                    b[0], b[1], b[2], b[3], b[4], b[5], b[6], b[7],
                    b[8], b[9], b[10], b[11], b[12], b[13], b[14], b[15],
                )
            }
            EseValue::U16(x) => x.to_string(),
        }
    }
}

// Custom trait. Annoying, but it must be done to copy Value to this serializable enum.
impl From<&Value> for EseValue {
    fn from(value: &Value) -> Self {
        match value {
            Value::Null => Self::Null,
            Value::Bool(x) => Self::Bool(*x),
            Value::U8(x) => Self::U8(*x),
            Value::I16(x) => Self::I16(*x),
            Value::I32(x) => Self::I32(*x),
            Value::Currency(x) => Self::Currency(*x),
            Value::F32(x) => Self::F32(*x),
            Value::F64(x) => Self::F64(*x),
            Value::DateTime(x) => Self::DateTime(*x),
            Value::Binary(x) => Self::Binary(x.to_vec()),
            Value::Text(x) => Self::Text(x.to_string()),
            Value::LargeBinary(x) => Self::LargeBinary(x.to_vec()),
            Value::LargeText(x) => Self::LargeText(x.to_string()),
            Value::SuperLarge(x) => Self::SuperLarge(x.to_vec()),
            Value::U32(x) => Self::U32(*x),
            Value::I64(x) => Self::I64(*x),
            Value::Guid(x) => Self::Guid(x.to_vec()),
            Value::U16(x) => Self::U16(*x),
        }
    }
}

/// Get the value of a record at the given column
fn get_record_value(record: &Record, value_id: &i32) -> Value {
    return record.value(*value_id).unwrap_or_default();
}

/// Ensure a record's value isn't empty
fn ensure_record_value(record: &Record, value_id: &i32) -> bool {
    get_record_value(&record, &value_id).to_string() != ""
}

/// Get a column by name
/// For some unknown reason, column ID != column index (value ID?)
/// TODO: Clean up wording, maybe create a struct that abstracts this stuff away (+ caches it?)
fn get_column_by_name<'a>(datatable: &'a Table, column_name: &str) -> Option<i32> {
    // println!("Getting column by name: {}", column_name);

    let num_cols = datatable.count_columns().unwrap();

    for i in 0..num_cols {
        let curr_column = datatable.column(i).unwrap();
        let curr_column_name = curr_column.name().unwrap().to_string();
        if curr_column_name == column_name {
            return Some(i);
        }
    }

    return None;
}

/// Turn attr ID to full column name in DB
///
/// TODO: ATTq-1630485858 and ATTf-1813520481 don't even get passed to this function
///
/// TODO: omSyntax can be used to derive the type, kinda... see ad_constants.py
fn get_esent_attribute_from_id(datatable: &Table, id: String) -> Option<String> {
    let columns = datatable
        .iter_columns()
        .unwrap()
        .map(|c| c.unwrap().name().unwrap_or_default().to_string())
        .collect::<Vec<String>>();

    let pattern = format!("^ATT[a-z\\-]{{1,2}}{}$", id);
    let re = Regex::new(&pattern).unwrap();

    if id == "3" {
        println!(
            "\nSanity check: processing ID 3 (cn).\nThis regex should return a match for ATTm3: {}",
            re
        );
    }

    match columns.iter().filter(|column| re.is_match(&column)).nth(0) {
        Some(column) => {
            let column: String = column.to_owned();

            if id == "3" {
                if column == String::from("ATTm3") {
                    println!("Sanity check: got a match for ID 3 (cn): {}\n", column);
                } else {
                    println!(
                        "Sanity check: got an incorrect match for ID 3: expected ATTm3, got {}",
                        column
                    );
                    println!("Aborting");
                    exit(1);
                }
            }

            return Some(column);
        }
        None => {
            // NOTE: Only debug a single ID because this captures some columns that don't actually show up
            if id == "3" {
                println!("Failed to get mapping for ID 3 (cn)...");
                println!("Aborting");
                exit(1);
            }

            return None;
        }
    };
}

fn generate_esent_attribute_map(datatable: &Table) -> Option<HashMap<String, String>> {
    // Get the column ID for attributeID (internal attribute ID number, minus 'ATT' + the type)
    // and for lDAPDisplayName (human-readable attribute name)
    let column_esent_attribute_id: i32;
    let column_ldap_display_name: i32;

    match get_column_by_name(&datatable, ESENT_ATTRIBUTE_ID) {
        Some(column) => {
            column_esent_attribute_id = column;
        }
        None => {
            println!(
                "Failed to locate column for ESENT attribute ID: {}",
                ESENT_ATTRIBUTE_ID
            );
            return None;
        }
    }

    match get_column_by_name(&datatable, ESENT_LDAP_DISPLAY_NAME) {
        Some(column) => {
            column_ldap_display_name = column;
        }
        None => {
            println!(
                "Failed to locate column for LDAP attribute name: {}",
                ESENT_LDAP_DISPLAY_NAME
            );
            return None;
        }
    }

    // Filter records to only those with non-empty attributeID and lDAPDisplayName
    let records = datatable.iter_records().unwrap().map(|r| r.unwrap()).filter(|record| {
        ensure_record_value(record, &column_esent_attribute_id)
            && ensure_record_value(record, &column_ldap_display_name)
    });

    // Key: internal ESENT attribute ID, e.g. 'ATTm3' (defaults to plain attributeID, e.g. '3')
    // Value: value of lDAPDisplayName, e.g. 'cn'
    let mut esent_attribute_map: HashMap<String, String> = HashMap::new();

    esent_attribute_map.extend(records.map(|record| {
        (
            get_esent_attribute_from_id(
                &datatable,
                get_record_value(&record, &column_esent_attribute_id).to_string(),
            )
            .unwrap_or_else(|| get_record_value(&record, &column_esent_attribute_id).to_string()),
            get_record_value(&record, &column_ldap_display_name).to_string(),
        )
    }));

    // NOTE: Don't bother comparing with the number of columns. More possible columns are listed
    // in the records than actually exist.
    if esent_attribute_map.len() == 0 {
        println!("ESENT attribute map is empty!");
        return None;
    }

    println!(
        "Mapped {} internal ESENT attribute names to lDAPDisplayName",
        &esent_attribute_map.len()
    );

    return Some(esent_attribute_map);
}

// CLI arguments
#[derive(Parser, Debug)]
#[command(name = "ese.rs", author = "Zach Sanchez", version = "1.0b")]
#[command(about = "Decode an Active Directory database (ntds.dit) and dump the contents to JSON.", long_about = None)]
struct Cli {
    input_file: std::path::PathBuf,
    output_file: std::path::PathBuf,
}

fn main() {
    // So I can time this when I test against larger DBs
    let start_time: DateTime<Utc> = Utc::now();

    let args = Cli::parse();

    let db = EseDb::open(&args.input_file).unwrap();
    let table = db.table_by_name("datatable").unwrap();

    println!("Loaded `datatable` from: {}", args.input_file.display());

    // Generate the human-readable column names
    let esent_attr_map: HashMap<String, String>;

    println!("Generating ESENT attribute map...");

    match generate_esent_attribute_map(&table) {
        Some(m) => {
            esent_attr_map = m;
        }
        None => {
            println!("Failed to generate ESENT attribute map...");
            println!("Aborting");
            exit(1);
        }
    }

    // Collect column name/ID map to make iteration a bit cleaner
    println!("Collecting column name/ID map...");

    // Key: human-readable column name
    // Value: column ID
    let mut columns: HashMap<String, i32> = HashMap::new();

    for column in table.iter_columns().unwrap().map(|c| c.unwrap()) {
        let column_name_internal = column.name().unwrap();
        let column_id = get_column_by_name(&table, &column_name_internal).unwrap();

        let column_name = esent_attr_map.get(&column_name_internal).unwrap_or_else(|| &column_name_internal);

        columns.insert(column_name.to_string(), column_id);
    }

    println!("Collecting/translating ESE DB records...");

    let mut records: Vec<HashMap<String, EseValue>> = Vec::new();

    for (_idx, record) in table.iter_records().unwrap().map(|r| r.unwrap()).enumerate() {
        let mut record_map: HashMap<String, EseValue> = HashMap::new();

        for (column_name, column_id) in columns.iter() {
            let value = record.value(*column_id).unwrap_or_default();

            let ese_value: EseValue = EseValue::from(&value);

            // Binary, LargeBinary, SuperLarge, Guid are all Vec under the hood, so turn them into
            // strings since that's not what we want in our JSON
            let ese_value: EseValue = match ese_value {
                EseValue::Binary(_) => EseValue::Text(ese_value.to_string()),
                EseValue::LargeBinary(_) => EseValue::Text(ese_value.to_string()),
                EseValue::SuperLarge(_) => EseValue::Text(ese_value.to_string()),
                EseValue::Guid(_) => EseValue::Text(ese_value.to_string()),
                _ => ese_value,
            };

            record_map.insert(column_name.to_string(), ese_value);
        }

        records.push(record_map);
    }

    // And finally, dump everything to JSON
    println!("Generating JSON for {} records...", &records.len());
    let json: String = serde_json::to_string(&records).unwrap();

    println!("Dumping JSON to {}...", &args.output_file.display());
    std::fs::write(&args.output_file, json).expect("Unable to write file");

    println!("Done!");

    // A personal reminder for my test ntds.dit (generated w/ BadBlood in VM)
    println!("\nStart time: {}\nEnd time: {}", start_time, Utc::now());
    println!(
        "\nTo test the output: `jq -S '.[] | select(.displayName==\"KELLY_WOLF\") | with_entries(select(.value != null))' < {}`",
        args.output_file.display()
    );
}
