#!/usr/bin/env python3

"""A really bad script for converting TSV to JSON. This is just for a reference point."""

from __future__ import annotations

import json
import sys
import pandas as pd

from loguru import logger
from path import Path


df = pd.read_csv(sys.argv[1], sep="\t", encoding="utf-8", dtype=object)
df = df.reset_index()

logger.info("Converting DataFrame to dict...")
data = df.to_dict(orient="records")

logger.info("Writing JSON...")
datatable_json_file = Path(f"{sys.argv[1]}.json").expand()
datatable_json_file.write_text(json.dumps(data, default=str, indent=2))
