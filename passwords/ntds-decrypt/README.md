# ntds-decrypt

A suite of

## ntds-decrypt.py

Goals:

- Learn how NTDS.dit can be decrypted using Python and, to a much lesser extent, learn more about ESE databases.
- Create a CLI script that can decrypt NTDS.dit and doesn't also take forever to run.

Credit to [ltdenard](https://github.com/ltdenard), who wrote the actual functionality as part of the [Kraken](https://github.com/ltdenard/kraken) project.

### 'pull' command (kraken_yoinker.py)

Pull the ntds.dit and SYSTEM bootkey from a DC with the VSS method.

The core functionality in this script was copy/pasted from `KrakenMaster.pull_ntds_bootkey`. Beyond that, I'm just applying my personal formatting preferences, adding comments, and wrapping it in `click` so this can be a CLI script.

Motivated by the fact that the ADDS attribute names are extremely opaque, and I wanted to see which other ones I could reverse.

### 'decrypt' command (kraken_decryptor.py)

Decode ntds.dit, decrypt its password hashes, and then dump to a hashfile + JSON.

The core functionality in this script was copy/pasted from `KrakenMaster.ntds_decrypt`. Beyond that, I'm just applying my personal formatting preferences, adding comments, and wrapping it in `click` so this can be a CLI script.

References:

- https://moyix.blogspot.com/2008/02/syskey-and-sam.html
- https://techcommunity.microsoft.com/t5/ask-the-directory-services-team/ese-deep-dive-part-1-the-anatomy-of-an-ese-database/ba-p/400496
- https://github.com/Velocidex/go-ese

## kraken_esedbexport.py

A variant of kraken_decryptor.py that uses [libesedb's](https://github.com/libyal/libesedb) `esedbexport` utility to dump ntds.dit and determine lDAPDisplayName based on attributeID.

## ese.rs

A Rust-based script to dump the entire contents of ntds.dit to JSON, along with decoded attribute names.

Motivatied by the fact that Python, being an interpreted language, is far slower than a compiled one. Speed is a concern when you're dumping multiple large domains on a regular basis, and I could gain access to libesedb without having to code in C.

For a 50MB ntds.dit, this script only takes 30 seconds to run from start to finish. For a 7GB ntds.dit and a private version of this script with *far* more optimizations, this takes 50-60 minutes.

To build the binary, install `cross`, then run:

```bash
CROSS_CONTAINER_ENGINE=podman cross build --bin ese
```

---

**WARNING:** This script is a failed POC. It's too slow compared to, say, Impacket's secretsdump.py. Impacket only extracts the attributes you need, but more importantly, it can actually be paralellized.

On the other hand, libesedb-rs cannot be parallized since the underlying C library, libesedb, *requires* mutable raw pointers, which naturally cannot be safely shared across threads/processes. If we look at the (effective) entrypoint `libesedb_file_initialize`, the first parameter `libesedb_file_t **file` is a null pointer that gets reassigned at the end of the function: `*file = (libesedb_file_t *)internal_file;`.

As a result, any Rust bindings will be unsafe no matter how you slice it.

Why does this script need to be parallelized? Wouldn't just smarter iteration + loading only specific attributes speed things up significantly? Well, no. If you reduce this script down to the bare basics, simply loading all records, it takes around 15 seconds to process 10,000 records. A 7GB ntds.dit has approximately 2 million records, which translates into a processing time of around 40-50 minutes. This is considerably slower than can be achieved with Impacket (around 15-20 minutes when parallelized with a lot of cores).

This is certainly impressive considering the script is single-threaded, but until a proper, safe Rust library is written (or libesedb is better at handling large databases), then this script is a no-go.
