# pyright: reportUninitializedInstanceVariable=none

"""Decrypt and decode ntds.dit + generate SAM records as JSON."""

from __future__ import annotations

import datetime
import hashlib
import json
import uuid
from binascii import hexlify, unhexlify
from collections import OrderedDict
from typing import Any

import pathos.pools as pp
from ad_constants import ESENT_TO_LDAP, ESENT_TO_LDAP_DECODED, NAME_TO_INTERNAL
from binary_fun import (
    CRYPTED_HASH,
    CRYPTED_HASHW16,
    PEK_KEY,
    PEKLIST_ENC,
    PEKLIST_PLAIN,
    SAMR_RPC_SID,
    CryptoCommon,
    unpack,
)
from Cryptodome.Cipher import ARC4, DES
from ese import ESENT_DB, ESENT_PAGE
from impacket.examples.secretsdump import LocalOperations
from ldap3.protocol.formatters.formatters import format_ad_timestamp
from loguru import logger
from passlib.handlers.windows import lmhash, nthash
from path import Path

EMPTY_LM_HASH = lmhash.hash("")
EMPTY_NTLM_HASH = nthash.hash("")


def convert_json(obj: Any):
    """A sane function for json.dump(s)' `default` to convert Python data types to JSON-compatible ones."""

    if isinstance(obj, (set, tuple)):
        return list(obj)
    if isinstance(obj, OrderedDict):
        return dict(obj)
    if isinstance(obj, datetime.datetime):
        return obj.replace(microsecond=0).isoformat()
    if isinstance(obj, bytes):
        try:
            return obj.decode(encoding="utf-8")
        except UnicodeDecodeError:
            return repr(obj)

    try:
        return str(obj)
    except:
        return repr(obj)


def get_hive_bootkey(system_hive: str) -> bytes:
    """Get the BOOTKEY.

    Steps/info:
    - The Encrypted SysKey needs to be decrypted, to built the RC4 decryption key, we need the Bootkey
    - This Bootkey is calculated by reordering the 4 Class Names from JD, Skew1, GBG and Data
    - Reorder using ShiftArray1
    """
    localOperations = LocalOperations(system_hive)
    bootKey = localOperations.getBootKey()
    return bootKey


def convert_byte_dict(data: Any) -> Any:
    """Decode bytes, dict w/ bytes, or tuple w/ bytes

    TODO: Tuple conversion doesn't look quite right
    """
    if isinstance(data, bytes):
        return data.decode("ascii")
    if isinstance(data, dict):
        return dict(map(convert_byte_dict, data.items()))
    if isinstance(data, tuple):
        return map(convert_byte_dict, data)

    return data


class KrakenDecryptor:
    def __init__(
        self,
        base_dir: Path = Path("./").expand(),  # pyright: ignore[reportCallInDefaultInitializer]
    ):
        self.hash_dir = (base_dir / str(uuid.uuid4())).expand()
        self.workdir = (self.hash_dir / f"workdir").makedirs_p()
        self.output_file = self.hash_dir / "output.json"

        logger.debug(f"{self.hash_dir = }")
        logger.debug(f"{self.workdir = }")
        logger.debug(f"{self.output_file = }")

        # Needed cross-function
        self.pek_list = list()

    def __removeRC4Layer(self, crypted_hash: CRYPTED_HASH) -> bytes:
        """Remove the RC4 layer of encryption.

        Args:
            crypted_hash (CRYPTED_HASH): Encrypted hash.

        Returns:
            bytes: The resulting plaintext.
        """

        md5 = hashlib.new("md5")
        # PEK index can be found on header of each ciphered blob (pos 8-10)
        pek_index = hexlify(crypted_hash["Header"])
        md5.update(self.pek_list[int(pek_index[8:10])])
        md5.update(crypted_hash["KeyMaterial"])
        rc4 = ARC4.new(md5.digest())
        plaintext = rc4.encrypt(crypted_hash["EncryptedHash"])
        return plaintext

    def __removeDESLayer(self, crypted_hash: bytes, rid: str) -> bytes:
        """Remove the DES layer of encryption.

        Args:
            crypted_hash (bytes): Encrypted hash.
            rid (str): User's relative identifier.

        Returns:
            bytes: The resulting plaintext.
        """

        key1, key2 = CryptoCommon().deriveKey(int(rid))
        crypt1, crypt2 = DES.new(key1, DES.MODE_ECB), DES.new(key2, DES.MODE_ECB)
        decrypted_hash = crypt1.decrypt(crypted_hash[:8]) + crypt2.decrypt(crypted_hash[8:])
        return decrypted_hash

    def decrypt_windows_hash(self, encrypted_hash: str | None, sid: str, default: str = "") -> str:
        """Decrypt a Windows hash.

        TODO: Write up details. Encryption layers:

        For Win2012: DES -> RC4 for the DES hash -> RC4 for the SysKey
        For Win2016: DES -> AES for the DES hash -> AES for the SysKey

        Args:
            encrypted_hash (str | None): Encrypted hash.
            sid (str): User's SID.
            default (str): Default to return if the encrypted hash is None.

        Returns:
            str: The decrypted hash (or the default if applicable).
        """

        if encrypted_hash is None:
            return default

        encrypted_hash_struct = CRYPTED_HASH(unhexlify(encrypted_hash))

        # Win2016 TP4 decryption is different
        if encrypted_hash_struct["Header"][:4] == b"\x13\x00\x00\x00":
            encrypted_hash_struct = CRYPTED_HASHW16(unhexlify(encrypted_hash))

            # PEK index can be found on header of each ciphered blob (pos 8-10)
            pek_index = hexlify(encrypted_hash_struct["Header"])

            intermediate_hash = CryptoCommon().decryptAES(
                self.pek_list[int(pek_index[8:10])],
                encrypted_hash_struct["EncryptedHash"][:16],
                encrypted_hash_struct["KeyMaterial"],
            )

        else:
            intermediate_hash = self.__removeRC4Layer(encrypted_hash_struct)

        rid = SAMR_RPC_SID(unhexlify(sid)).formatCanonical().split("-")[-1]
        decrypted_hash = self.__removeDESLayer(intermediate_hash, rid)

        return hexlify(decrypted_hash).decode("utf-8")

    def validate_record(self, record: dict[str, Any] | OrderedDict[str, Any]) -> bool:
        """Ensure that an ESENT record is for a (active) user account.

        Did this have to be a long chain of if/elif/else? No, I just rewrote it that way to make
        the validation logic easier to understand.

        Args:
            record (dict | OrderedDict): Record to validate.

        Returns:
            bool: Whether the record is for a user account.
        """

        uac = record["userAccountControl"]
        sAMAccountName: str = record["sAMAccountName"]
        name = record["name"]

        if not sAMAccountName.isprintable():
            logger.trace(f"Found a non-printable sAMAccountName, skipping: {sAMAccountName!r}")
        elif "$" in sAMAccountName:
            logger.trace(f"Found a machine account, skipping: {sAMAccountName!r}")
        elif "HealthMailbox" in sAMAccountName:
            logger.trace(f"Found an MS Exchange monitoring mailbox, skipping: {sAMAccountName!r}")
        # More technically, this is a phantom object:
        # https://learn.microsoft.com/en-us/troubleshoot/windows-server/active-directory/phantoms-tombstones-infrastructure-master
        elif "DEL:" in name:
            logger.trace(f"Found a deleted account, skipping: {sAMAccountName!r}")
        elif not isinstance(uac, int):
            logger.trace(f"Found invalid UAC {uac!r}, skipping: {sAMAccountName!r}")
        # ACCOUNTDISABLE -> 0x0002
        elif uac & (1 << 1) != 0:
            logger.trace(f"Found a disabled account, skipping: {sAMAccountName!r}")
        else:
            logger.trace(f"{sAMAccountName!r} is valid!")
            return True

        return False

    def decode_esent_record_keys(self, record: dict[str, Any] | OrderedDict[str, Any]) -> dict[str, Any]:
        """Convert internal ESENT column names into the matching LDAP attributes.

        Args:
            record (dict | OrderedDict): Record to convert.

        Returns:
            dict: Converted record.
        """

        def get_esent_key(k: Any) -> str:
            return ESENT_TO_LDAP.get(k) or ESENT_TO_LDAP_DECODED.get(k) or k

        return {(get_esent_key(k)): v for k, v in record.items()}

    def process_esent_tag(self, page_data: ESENT_PAGE, tag_num: int) -> bool:
        """Convert a tag (data blob) of the given page to human-readable JSON w/ password hashes.

        NOTE: The core hash decryption code is basically a wrapped/condensed version of Impacket's
        NTDSHashes.__decryptHash.

        For info on the password hash decryption process, see:
        - https://www.exploit-db.com/docs/english/18244-active-domain-offline-hash-dump-&-forensic-analysis.pdf
        - https://www.insecurity.be/blog/2018/01/21/retrieving-ntlm-hashes-and-what-changed-technical-writeup/

        Args:
            page_data (ESENT_PAGE): Page of the ESENT DB to process.
            tag_num (int): Tag to process.

        Returns:
            bool: Whether the tag exists.
        """

        logger.trace(f"Parsing tag {tag_num}...")

        if not (tag := self.ESEDB.getleaf(page_data, tag_num)):
            return False

        if record := self.ESEDB.tagToRecord(self.cursor, tag["EntryData"]):
            record: dict[str, Any] = convert_byte_dict(self.decode_esent_record_keys(record))

            # Decrypt password hashes + write as record to JSON if this is an active user
            if self.validate_record(record):
                try:
                    sAMAccountName: str = record["sAMAccountName"]
                    logger.debug(f"Found a record for: {sAMAccountName!r}")

                    logger.debug(f"Converting the LM hash for {sAMAccountName!r}...")
                    record["lmHash"] = self.decrypt_windows_hash(
                        encrypted_hash=record["dBCSPwd"],
                        sid=record["objectSid"],
                        default=EMPTY_LM_HASH,
                    )

                    logger.debug(f"Converting the NTLM hash for {sAMAccountName!r}...")
                    record["ntlmHash"] = self.decrypt_windows_hash(
                        encrypted_hash=record["unicodePwd"],
                        sid=record["objectSid"],
                        default=EMPTY_NTLM_HASH,
                    )

                    # Convert this now so we don't have to worry about it later
                    logger.debug(f"Attempting to convert pwdLastSet for {sAMAccountName!r}...")
                    record["pwdLastSet"] = format_ad_timestamp(record["pwdLastSet"])

                    # Add human-readable active status because it's easier than remembering the
                    # specific condition (ACCOUNTDISABLE -> 0x0002)
                    logger.debug(f"Generating userAccountStatus for {sAMAccountName!r}...")
                    if record["userAccountControl"] & (1 << 1) == 0:
                        record["userAccountStatus"] = "Enabled"
                    else:
                        record["userAccountStatus"] = "Disabled"

                    output_file = Path(f"{self.workdir}/{str(uuid.uuid4())}").expand()
                    logger.debug(f"Writing JSON for {sAMAccountName!r} to {output_file}...")

                    with open(output_file, "w") as f:
                        json.dump(record, f, default=convert_json)

                # Log these exceptions since they're actual errors instead of lower-level
                # errors that crop up due to Impacket's somewhat shoddy code
                except Exception as e:
                    logger.exception(e)

        return True

    def process_esent_page(self, page_data: ESENT_PAGE) -> None:
        """Process a page of the ESENT DB.

        NOTE: Data is stored within pages, which form a B-tree w/ data in actual leaf pages (-> subpage?).
              This data is accessible via "tags" (data blobs).

        Args:
            page_data (ESENT_PAGE): Page of the ESENT DB to process.
        """

        tag_num = 1

        try:
            while self.process_esent_tag(page_data, tag_num=tag_num):
                tag_num += 1

        # Seems like this fails on the first tag for a fair few pages, so I guess some pages
        # don't have any tags?
        except Exception as e:
            logger.trace(f"Failed to parse {tag_num = } - skipping this page (error: {e})")

    def decrypt_pek_list(self, ntds_file: Path, system_file: Path) -> None:
        """Decrypt the PEK so we can decrypt the password hashes.

        Ripped from NTDSHashes.__getPek

        For info on the PEK decryption process, see:
        - https://www.exploit-db.com/docs/english/18244-active-domain-offline-hash-dump-&-forensic-analysis.pdf
        - https://www.insecurity.be/blog/2018/01/21/retrieving-ntlm-hashes-and-what-changed-technical-writeup/
        """

        # PEK is encrypted with the BOOTKEY
        logger.info("Getting the BOOTKEY...")
        bootkey = get_hive_bootkey(system_file)

        # Now let's try to get the PEK
        logger.info("Mounting the ESE DB...")
        ESEDB = ESENT_DB(ntds_file, isRemote=False)
        cursor = ESEDB.openTable("datatable")
        peklist = None

        logger.info("Iterating over table 'datatable' to find the encrypted PEK list...")
        while True:
            try:
                record = ESEDB.getNextRow(cursor)
            except BaseException:
                logger.warning("Error while calling getNextRow(), trying the next one...")
                continue

            # ? Out of records?
            if record is None:
                break

            # If this isn't null, we've found the PEK
            elif record[NAME_TO_INTERNAL["pekList"]] is not None:
                logger.debug("Seems we've found the encrypted PEK")
                peklist = unhexlify(record[NAME_TO_INTERNAL["pekList"]])
                break

        # Decrypt the PEK list
        if peklist is not None:
            logger.info("Found the encrypted PEK list!")

            encrypted_pek_list = PEKLIST_ENC(peklist)

            # PEK header starts this way up to Windows Server 2012 R2
            #
            # First byte of the "F" value (from 'SAM\SAM\Domains\Account'), or the "Header" field,
            # starts with 0x02 when RC4 encryption is used
            #
            # This is more legacy and is what you'll likely find in research papers/blog posts
            # Algorithm (kinda): MD5(F[0x70] + String1 + Bootkey + String2)
            if encrypted_pek_list["Header"][:4] == b"\x02\x00\x00\x00":
                logger.debug("PEK struct header indicates Windows Server 2012 R2")

                logger.debug("Generating the hashed BOOTKEY...")
                md5 = hashlib.new("md5")
                md5.update(bootkey)

                # 1,000 reincarnations of the PEK key material
                logger.debug("Updating the MD5 hash with the PEK key material...")
                for i in range(1000):
                    md5.update(encrypted_pek_list["KeyMaterial"])

                logger.debug("Getting the RC4 key...")
                rc4_key = md5.digest()
                logger.trace(f"{rc4_key = }")

                # EncryptUsingRC4(partially encrypted blob, rc4key)
                # This is SystemFunction033 (RtlEncryptDecryptRC4) in AdvApi32.dll
                logger.debug("Decrypting the PEK list...")
                rc4 = ARC4.new(rc4_key)
                decrypted_pek_list = PEKLIST_PLAIN(rc4.encrypt(encrypted_pek_list["EncryptedPek"]))

                # Iterate PEK list entries, store results
                logger.debug("Getting PEKs from list...")

                PEKLen = len(PEK_KEY())
                logger.trace(f"{PEKLen = }")  # when I ran this, PEKLen == 20

                for i in range(len(decrypted_pek_list["DecryptedPek"]) // PEKLen):
                    cursor = i * PEKLen
                    pek = PEK_KEY(decrypted_pek_list["DecryptedPek"][cursor : cursor + PEKLen])
                    self.pek_list.append(pek["Key"])

                logger.success("Decrypted the PEK list!")

            # PEK header starts this way for Windows 2016 Server TP4
            #
            # First byte of the "F" value (from 'SAM\SAM\Domains\Account'), or the "Header" field,
            # starts with 0x03 when AES encryption is used
            #
            # The encrypted PEK seems to be different, but it's actually similar to decrypting LSASS
            # In Windows Server 2016, the RC4 encryption was replaced with AES256 in CBC with IV and zero padding
            elif encrypted_pek_list["Header"][:4] == b"\x03\x00\x00\x00":
                logger.debug("PEK struct header indicates Windows Server 2016 TP4")

                # Decrypt PEK w/ AES
                # Don't need to construct the encryption key anymore - the bootkey suffices now
                #
                # Key - the bootkey
                # CipherText - PEKLIST_ENC['EncryptedPek']
                # IV - PEKLIST_ENC['KeyMaterial']
                logger.debug("Decrypting the PEK list")
                decrypted_pek_list = PEKLIST_PLAIN(
                    CryptoCommon().decryptAES(
                        bootkey,
                        encrypted_pek_list["EncryptedPek"],
                        encrypted_pek_list["KeyMaterial"],
                    )
                )

                # I assume this is just some struct magic I don't really need to decode any further
                # than the comment below
                logger.debug("Getting PEKs from list...")

                # PEK list entries take the form: index (4 byte LE int), PEK (16 byte key)
                # The entries are in ascending order, and the list is terminated
                # by an entry with a non-sequential index (08080808 observed)
                pos = 0
                cur_index = 0
                while True:
                    pek_entry = decrypted_pek_list["DecryptedPek"][pos : pos + 20]

                    # Break if PEK is truncated (should not happen)
                    if len(pek_entry) < 20:
                        logger.warning("Found entry in PEK list that appears to be truncated!")
                        break

                    index, pek = unpack("<L16s", pek_entry)

                    # Break on non-sequential index
                    if index != cur_index:
                        break

                    pos += 20
                    cur_index += 1
                    self.pek_list.append(pek)

                    logger.trace(f"PEK # {index} found and decrypted: {hexlify(pek).decode('utf-8')}")

                logger.success("Decrypted the PEK list!")

            else:
                raise Exception("Failed to identify the PEK struct header!")

        else:
            raise Exception("Failed to find the PEK list!")

    def ntds_decrypt(
        self,
        ntds_file: Path,
        system_file: Path,
        clear_workdir: bool = True,
        strip_null: bool = True,
    ) -> None:
        """Decrypt the NTDS.dit ESE DB, decode/parse out values we care about.

        Uses multiprocessing so we don't need to iterate over each page one-by-one, which significantly
        speeds up the runtime.

        Reference: https://adsecurity.org/?p=2398

        Args:
            ntds_file (Path): Path to ntds.dit.
            system_file (Path): Path to SYSTEM bootkey.
            clear_workdir (bool, default): Whether to remove the workdir after completion. Defaults to True.
            strip_null (bool, optional): Remove keys with value 'null' from output JSON (it's pretty noisy). Defaults to True.
        """

        if not (ntds_file := ntds_file.expand()).exists():
            raise Exception(f"ntds.dit doesn't exist at path: {ntds_file}")
        if not (system_file := system_file.expand()).exists():
            raise Exception(f"SYSTEM bootkey doesn't exist at path: {system_file}")

        # Create the directories we need
        for directory in [self.hash_dir, self.workdir]:
            Path(directory).makedirs_p()

        # Get PEK list
        logger.info("Getting the PEK list...")
        self.decrypt_pek_list(ntds_file, system_file)

        # Initialize ESE DB connection
        logger.info("Mounting the ESE database...")
        self.ESEDB = ESENT_DB(ntds_file, isRemote=False)
        self.cursor: dict[str, Any] | None = self.ESEDB.openTable("datatable")
        total_pages: int | None = self.ESEDB.totalPages
        all_pages: list[ESENT_PAGE] = self.ESEDB.getPages(total_pages)

        # Initialize multiprocessing pool
        pool = pp.ProcessPool()

        # Convert ESE DB records to JSON
        logger.info("Converting database records to JSON...")
        pool.map(self.process_esent_page, all_pages)

        # Wait for everything to execute + perform cleanup
        pool.close()
        pool.join()
        pool.clear()

        # Concatenate all of the records into one file
        workdir_contents = self.workdir.files()

        logger.success(f"Finished parsing NTDS.dit records! Found {len(workdir_contents)} users.")
        logger.info("Concatenating records...")

        records = []

        for record_json in workdir_contents:
            with open(record_json) as f:
                record = json.load(f)

                if strip_null:
                    record = {k: v for k, v in record.items() if v is not None}

                records.append(record)

        with open(self.output_file, "w") as f:
            json.dump(records, f, indent=2)

        if clear_workdir:
            logger.info(f"Clearing workdir...")
            self.workdir.rmdir_p()

        logger.success(f"Done! Results can be found at {self.output_file!r}")
