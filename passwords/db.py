#!/usr/bin/env python3

import re
import sqlite3
import traceback

from loguru import logger
from passlib.handlers.windows import nthash


def generate_ntlm_hash(password: str) -> str:
    return nthash.hash(password)


def create_connection(fn: str):
    """Create a database connection to a SQLite database"""
    conn = None
    try:
        conn = sqlite3.connect(fn)
        # logger.debug(sqlite3.version)
    except sqlite3.Error as e:
        logger.error(f"Error during connection init! {e}")

    return conn


def create_table(conn: sqlite3.Connection):
    try:
        cursor = conn.cursor()
        _ = cursor.execute(
            """
                CREATE TABLE IF NOT EXISTS rainbow_table_ntlm (
                    hash TEXT PRIMARY KEY,
                    plaintext TEXT
                );
            """
        )

    except sqlite3.Error as e:
        logger.error(f"Error during table creation! {e}")


def insert_record(conn: sqlite3.Connection, hash: str, plaintext: str):
    try:
        cursor = conn.cursor()
        _ = cursor.execute(
            "INSERT INTO rainbow_table_ntlm (hash,plaintext) VALUES (:hash, :plaintext);",
            {"hash": hash, "plaintext": plaintext},
        )
        conn.commit()

    except sqlite3.Error as e:
        logger.error(f"Error during data insertion! {e}")


logger.debug("starting...")
raise Exception("Missing correct debug condition, and process is running away for some reason (54GB used + 20GB journal, ~8.5 hours execution time)")
conn = create_connection("./rainbow-table-ntlm.db")
if conn:
    i = None
    line = None

    try:
        cursor = conn.cursor()

        _ = cursor.execute(
            """
                CREATE TABLE IF NOT EXISTS rainbow_table_ntlm (
                    hash TEXT PRIMARY KEY,
                    plaintext TEXT
                );
            """
        )

        # Generate 1 million rows -> 5 seconds
        # Generate 10 million rows -> 1 minute
        # Execute 10 million rows -> 1 minute
        # Commit 10 million rows -> 1 second
        with open(
            "/home/zsanchez113/cracking/wordlists/probable-wordlists/Top2Billion-probable-v2.txt",
            encoding="ISO-8859-1",
        ) as f:

            rows = []
            # _ = cursor.execute("BEGIN TRANSACTION")

            for i, line in enumerate(f):
                executed = False
                line = re.sub(r"\n$", "", line)
                line = line.strip()

                rows.append({"hash": generate_ntlm_hash(line), "plaintext": line})

                # TODO: fix condition for debug log after initial commit
                if rows and len(rows) % 10_000_000 == 0:
                    if i <= 9_999_999 or  i % 100_000_000 == 0:
                        logger.debug(f"{i = :,} - executing...")

                    # 'INSERT OR IGNORE' since the words aren't guaranteed to be unique...
                    _ = cursor.executemany(
                        "INSERT OR IGNORE INTO rainbow_table_ntlm (hash,plaintext) VALUES (:hash, :plaintext);",
                        rows,
                    )

                    if i <= 9_999_999 or i % 100_000_000 == 0:
                        logger.debug(f"{i = :,} - committing...")

                    conn.commit()

                    rows = []

                    if i <= 9_999_999 or i % 100_000_000 == 0:
                        logger.debug(f"{i = :,} - done")

                # if i > 10_000_000:
                #     logger.debug(f"breaking at {i = :,}")
                #     break

        conn.commit()

        # _ = cursor.execute("COMMIT")

        _ = cursor.execute("SELECT * FROM rainbow_table_ntlm LIMIT 1;")
        example_row = cursor.fetchone()
        logger.debug(f"{example_row = }")

    except Exception as e:
        logger.error(traceback.format_exc())
        if line:
            logger.error(f"{i = }, {generate_ntlm_hash(line) = }, {line = }")
    finally:
        conn.close()
