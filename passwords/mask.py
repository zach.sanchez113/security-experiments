#!/usr/bin/env python3

"""A failed attempt at Pythonizing hashcat masks. Might come in handy someday, though."""

from __future__ import annotations

import math
import sys

import click
import path
from loguru import logger


LOG_FORMAT = (
    "<green>{time:YYYY-MM-DD HH:mm:ss}</green> | <level>{level: <8}</level> | <level>{message}</level>"
)

# Remove default logger, add custom
logger.remove()
logger.add(
    sys.stdout,
    format=LOG_FORMAT,
    level="DEBUG",
)
logger.configure(
    levels=[dict(name="INFO", color="<blue><bold>"), dict(name="DEBUG", color="<magenta><bold>")]  # type: ignore
)

# region
# See: https://hashcat.net/wiki/doku.php?id=mask_attack
# Also see: https://project-rainbowcrack.com/charset.txt
charset_lower = list("abcdefghijklmnopqrstuvwxyz")
charset_upper = list("abcdefghijklmnopqrstuvwxyz".upper())
charset_digits = list("0123456789")
charset_special = list(r""" !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~""")
charset_hash_lower = list("abcdef") + charset_digits
charset_hash_upper = list("abcdef".upper()) + charset_digits

charset_all = charset_lower + charset_upper + charset_digits + charset_special
charset_alphanum_lower = charset_lower + charset_digits
charset_alphanum = charset_lower + charset_upper + charset_digits

masks = {
    "?l": len(charset_lower),
    "?u": len(charset_upper),
    "?d": len(charset_digits),
    "?h": len(charset_hash_lower),
    "?H": len(charset_hash_upper),
    "?s": len(charset_special),
    "?a": len(charset_all),
    # "?b": 0,
}

# endregion


@click.command()
@click.argument("wordlist", type=path.Path)
@click.option("-m", "--mask", type=str, required=False)
@click.option("-f", "--hcmask", type=path.Path, required=False)
def cli(wordlist: path.Path, mask: str | None, hcmask: path.Path | None):
    if not mask and not hcmask:
        logger.error("One of -m/--mask or -f/--hcmask is required!")
        sys.exit(1)

    wordlist = wordlist.lines(retain=False)  # type: ignore cause screw it

    final_count = len(wordlist)

    if mask:
        start_piece = mask.split("?", 1)[0]

        parts = [p for p in mask.split("?") if p != start_piece]
        mask_parts = [p[0] for p in parts if p]
        # extras = [p[1:] for p in parts if p[1:]]

        # if start_piece:
        #     extras.append(start_piece)

        logger.debug(f"{start_piece = }")
        logger.debug(f"{parts = }")
        logger.debug(f"{mask_parts = }")
        # logger.debug(f"{extras = }")

        if any(f"?{m}" not in masks for m in mask_parts):
            logger.error("Found custom and/or unknown charset! Aborting")
            sys.exit(1)

        mask_lens = [masks.get(f"?{m}", -10**9) for m in mask_parts]
        num_combos = math.prod(mask_lens)
        # num_combos = num_combos_mask * (len(extras) or 1)

        final_count = len(wordlist) + len(wordlist) * num_combos

        logger.info(f"{len(wordlist) = :,}")
        logger.info(f"{num_combos = :,}")
        logger.info(f"{final_count = :,}")


if __name__ == "__main__":
    cli()
