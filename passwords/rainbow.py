#!/usr/bin/env python3

import itertools
import sqlite3
from textwrap import dedent
from typing import Generator, List

from passlib.handlers.windows import nthash
from path import Path


EMPTY_NTLM_HASH = "31d6cfe0d16ae931b73c59d7e0c089c0"

# See: https://hashcat.net/wiki/doku.php?id=mask_attack
# Also see: https://project-rainbowcrack.com/charset.txt
charset_lower = list("abcdefghijklmnopqrstuvwxyz")
charset_upper = list("abcdefghijklmnopqrstuvwxyz".upper())
charset_digits = list("0123456789")
charset_special = list(r""" !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~""")
charset_hash_lower = list("abcdef") + charset_digits
charset_hash_upper = list("abcdef".upper()) + charset_digits

charset_all = charset_lower + charset_upper + charset_digits + charset_special
charset_alphanum_lower = charset_lower + charset_digits
charset_alphanum = charset_lower + charset_upper + charset_digits

ntlm_hash_length = "16 bytes"


def generate_ntlm_hash(password: str) -> str:
    return nthash.hash(password)


def verify_ntlm_hash(password: str, hash: str) -> bool:
    if not nthash.identify(hash):
        raise Exception(f"{hash} doesn't appear to be an NTLM hash!")

    return nthash.verify(password, hash)


def rainbow_table_size(charset: List[str], size: int) -> int:
    """Determine size of rainbow table given charset + number of chars."""
    num_chars = len(charset)
    return num_chars ** size


def generate_rainbow(charset: List[str], size: int = 1) -> Generator[dict]:
    """Generate NTLM rainbow table"""

    PROBABLE = 1_973_218_846  # 22 GB

    assert rainbow_table_size(charset, size) <= PROBABLE, "That's a large rainbow table! Aborting."

    for chars in itertools.product(charset, repeat=size):
        plaintext = "".join(chars)
        yield {"hash": generate_ntlm_hash(plaintext), "plaintext": plaintext}


print(f"{len(charset_all) = }")
print(generate_ntlm_hash("Alana2223"))

# rainbow_table = generate_rainbow(2)
# Path("/tmp/rainbow.txt").write_lines(rainbow_table)

print(
    f"\nFor reference, there are {1_973_218_846:,} lines in probable-wordliststop top 2 billion, which is ~22GB"
)
print(
    dedent(
        f"""
            Additionally, here are some numbers from the RainbowCrack Project:

            - ascii-32-95 (charset_all) x7 ->                        70,576,641,626,495 ->  52 GB
            - loweralpha-numeric (charset_alphanum_lower) x9 ->     104,461,669,716,084 ->  65 GB
            - mixalpha-numeric (charset_alphanum) x8 ->             221,919,451,578,090 -> 127 GB
            - loweralpha-numeric (charset_alphanum_lower) x10 ->  3,760,620,109,779,060 -> 316 GB
            - ascii-32-95 (charset_all) x8 ->                     6,704,780,954,517,120 -> 460 GB
            - mixalpha-numeric (charset_alphanum) x9 ->          13,759,005,997,841,642 -> 690 GB
        """
    )
)

# region
print(f"{rainbow_table_size(charset_lower, 1) = :>23,}")
print(f"{rainbow_table_size(charset_lower, 2) = :>23,}")
print(f"{rainbow_table_size(charset_lower, 3) = :>23,}")
print(f"{rainbow_table_size(charset_lower, 4) = :>23,}")
print(f"{rainbow_table_size(charset_lower, 5) = :>23,}")
print(f"{rainbow_table_size(charset_lower, 6) = :>23,}")
print(f"{rainbow_table_size(charset_lower, 7) = :>23,}")
print(f"{rainbow_table_size(charset_lower, 8) = :>23,}")
print(f"{rainbow_table_size(charset_lower, 9) = :>23,}")

print()

print(f"{rainbow_table_size(charset_alphanum_lower, 1) = :>23,}")
print(f"{rainbow_table_size(charset_alphanum_lower, 2) = :>23,}")
print(f"{rainbow_table_size(charset_alphanum_lower, 3) = :>23,}")
print(f"{rainbow_table_size(charset_alphanum_lower, 4) = :>23,}")
print(f"{rainbow_table_size(charset_alphanum_lower, 5) = :>23,}")
print(f"{rainbow_table_size(charset_alphanum_lower, 6) = :>23,}")
print(f"{rainbow_table_size(charset_alphanum_lower, 7) = :>23,}")
print(f"{rainbow_table_size(charset_alphanum_lower, 8) = :>23,}")
print(f"{rainbow_table_size(charset_alphanum_lower, 9) = :>23,}")

print()

print(f"{rainbow_table_size(charset_alphanum, 1) = :>23,}")
print(f"{rainbow_table_size(charset_alphanum, 2) = :>23,}")
print(f"{rainbow_table_size(charset_alphanum, 3) = :>23,}")
print(f"{rainbow_table_size(charset_alphanum, 4) = :>23,}")
print(f"{rainbow_table_size(charset_alphanum, 5) = :>23,}")
print(f"{rainbow_table_size(charset_alphanum, 6) = :>23,}")
print(f"{rainbow_table_size(charset_alphanum, 7) = :>23,}")
print(f"{rainbow_table_size(charset_alphanum, 8) = :>23,}")
print(f"{rainbow_table_size(charset_alphanum, 9) = :>23,}")

print()

print(f"{rainbow_table_size(charset_all, 1) = :>23,}")
print(f"{rainbow_table_size(charset_all, 2) = :>23,}")
print(f"{rainbow_table_size(charset_all, 3) = :>23,}")
print(f"{rainbow_table_size(charset_all, 4) = :>23,}")
print(f"{rainbow_table_size(charset_all, 5) = :>23,}")
print(f"{rainbow_table_size(charset_all, 6) = :>23,}")
print(f"{rainbow_table_size(charset_all, 7) = :>23,}")
print(f"{rainbow_table_size(charset_all, 8) = :>23,}")
print(f"{rainbow_table_size(charset_all, 9) = :>23,}")

# endregion
