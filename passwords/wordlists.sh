#!/usr/bin/env bash

mkdir -p \
  /usr/local/share/wordlists/probable-wordlists \
  /usr/local/share/wordlists/weakpass \
  /usr/local/share/wordlists/imsdb

pushd /usr/local/share/

# Clone SecLists, symlink to wordlists
git clone https://github.com/danielmiessler/SecLists.git
ln -s $(readlink -f ./SecLists/Passwords) /usr/local/share/wordlists/SecLists/

# Begin wordlists
pushd wordlists/

# probable-wordlists via weakpass
pushd probable-wordlists

curl -LO https://download.weakpass.com/wordlists/1852/Top109Million-probable-v2.txt.gz
curl -LO https://download.weakpass.com/wordlists/1853/Top12Thousand-probable-v2.txt.gz
curl -LO https://download.weakpass.com/wordlists/1854/Top1575-probable2.txt.gz
curl -LO https://download.weakpass.com/wordlists/1855/Top1pt6Million-probable-v2.txt.gz
curl -LO https://download.weakpass.com/wordlists/1856/Top207-probable-v2.txt.gz
curl -LO https://download.weakpass.com/wordlists/1857/Top29Million-probable-v2.txt.gz
curl -LO https://download.weakpass.com/wordlists/1858/Top2Billion-probable-v2.txt.gz
curl -LO https://download.weakpass.com/wordlists/1859/Top304Thousand-probable-v2.txt.gz
curl -LO https://download.weakpass.com/wordlists/1860/Top353Million-probable-v2.txt.gz

gunzip *gz

popd

# https://github.com/initstring/passphrase-wordlist
curl -L -o passphrase-wordlist.txt https://f002.backblazeb2.com/file/passphrase-wordlist/passphrases.txt

# Download movie scripts
# TODO: Also need to extract phrases
pushd imsdb

pip install beautifulsoup4==4.4.1 requests==2.10.0
curl -LO https://raw.githubusercontent.com/j2kun/imsdb_download_all_scripts/main/download_all_scripts.py
chmod +x download_all_scripts.py && ./download_all_scripts.py

popd

# Exit wordlists/, exit /usr/local/share/
popd
popd
