#!/usr/bin/env python3

from __future__ import annotations

import argparse
import datetime
import hashlib
import json
import math
import os
import re
import shutil
import stat
import sys
import tempfile
import uuid

import rich_click as click
from git import NULL_TREE, Repo, Commit
from loguru import logger
from path import Path
from truffleHogRegexes.regexChecks import regexes

# Config for rich-click
click.rich_click.SHOW_ARGUMENTS = True
click.rich_click.SHOW_METAVARS_COLUMN = False
click.rich_click.APPEND_METAVARS_HELP = True
click.rich_click.STYLE_OPTIONS_TABLE_PAD_EDGE = True
click.rich_click.COMMAND_GROUPS = {}

# Grab the Rich console
console = click.rich_click._get_rich_console()

# Base logging configuration
LOG_FORMAT = (
    "<green>{time:YYYY-MM-DD HH:mm:ss}</green> | <level>{level: <8}</level> | <level>{message}</level>"
)

logger.configure(
    handlers=[
        dict(sink=sys.stdout, level=os.environ.get("LOGURU_LEVEL", "INFO"), format=LOG_FORMAT),
    ],
    levels=[
        {"name": "INFO", "color": "<blue><bold>"},
        {"name": "DEBUG", "color": "<magenta><bold>"},
    ],
)

# truffleHog constants
BASE64_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="
HEX_CHARS = "1234567890abcdefABCDEF"


class bcolors:
    HEADER = "\033[95m"
    OKBLUE = "\033[94m"
    OKGREEN = "\033[92m"
    WARNING = "\033[93m"
    FAIL = "\033[91m"
    ENDC = "\033[0m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"


# TODO: Unused
def clean_up(output: dict):
    project_path = output.get("project_path", None)
    if project_path and os.path.isdir(project_path):
        shutil.rmtree(output["project_path"])

    issues_path = output.get("issues_path", None)
    if issues_path and os.path.isdir(issues_path):
        shutil.rmtree(output["issues_path"])


def print_results(print_json: bool, issue: dict) -> None:
    commit_time = issue["date"]
    branch_name = issue["branch"]
    prev_commit = issue["commit"]
    printable_diff = issue["printDiff"]
    commit_hash = issue["commit_hash"]
    reason = issue["reason"]
    path = issue["path"]

    if print_json:
        print(json.dumps(issue, sort_keys=True))
    else:
        print("~~~~~~~~~~~~~~~~~~~~~")
        reason = f"{bcolors.OKGREEN}Reason: {reason}{bcolors.ENDC}"
        print(reason)
        dateStr = f"{bcolors.OKGREEN}Date: {commit_time}{bcolors.ENDC}"
        print(dateStr)
        hashStr = f"{bcolors.OKGREEN}Hash: {commit_hash}{bcolors.ENDC}"
        print(hashStr)
        filePath = f"{bcolors.OKGREEN}Filepath: {path}{bcolors.ENDC}"
        print(filePath)

        branchStr = f"{bcolors.OKGREEN}Branch: {branch_name}{bcolors.ENDC}"
        print(branchStr)
        commitStr = f"{bcolors.OKGREEN}Commit: {prev_commit}{bcolors.ENDC}"
        print(commitStr)
        print(printable_diff)
        print("~~~~~~~~~~~~~~~~~~~~~")


def shannon_entropy(data: str, iterator: str) -> float:
    """Determine the Shannon entropy for a string.

    Borrowed from http://blog.dkbza.org/2007/05/scanning-data-for-entropy-anomalies.html

    Args:
        data (str): String to determine the Shannon entropy for.
        iterator (str): Character set to count as valid.

    Returns:
        float: The Shannon entopy of the string.
    """

    if not data:
        return 0

    entropy = 0

    for x in iterator:
        p_x = float(data.count(x)) / len(data)

        if p_x > 0:
            entropy += -p_x * math.log(p_x, 2)

    return entropy


def get_strings_of_set(word: str, char_set: str, threshold: int = 0) -> list[str]:
    count = 0
    letters = ""
    strings = []

    for char in word:
        if char in char_set:
            letters += char
            count += 1
        else:
            if count > threshold:
                strings.append(letters)

            letters = ""
            count = 0

    if count > threshold:
        strings.append(letters)

    return strings


def regex_check(
    printable_diff: str,
    commit_time,
    branch_name,
    prev_commit,
    blob,
    commit_hash,
    custom_regexes={},
) -> list[dict]:
    if custom_regexes:
        secret_regexes = custom_regexes
    else:
        secret_regexes = regexes

    regex_matches = []

    for key in secret_regexes:
        found_strings = secret_regexes[key].findall(printable_diff)

        found_diff = None  # because Pylance is yelling about potentially unbound found_diff...
        for found_string in found_strings:
            found_diff = printable_diff.replace(printable_diff, bcolors.WARNING + found_string + bcolors.ENDC)

        if found_strings:
            found_regex = {}
            found_regex["date"] = commit_time
            found_regex["path"] = blob.b_path if blob.b_path else blob.a_path
            found_regex["branch"] = branch_name
            found_regex["commit"] = prev_commit.message
            found_regex["diff"] = blob.diff.decode("utf-8", errors="replace")
            found_regex["strings_found"] = found_strings
            found_regex["printDiff"] = found_diff
            found_regex["reason"] = key
            found_regex["commit_hash"] = prev_commit.hexsha

            regex_matches.append(found_regex)

    return regex_matches


def find_entropy(
    printable_diff: str,
    commit_time: str,
    branch_name: str,
    prev_commit: Commit,
    blob: str,
    commit_hash: str,
):
    strings_found = []

    for line in printable_diff.split("\n"):
        for word in line.split():
            base64_strings = get_strings_of_set(word, BASE64_CHARS)
            hex_strings = get_strings_of_set(word, HEX_CHARS)

            for string in base64_strings:
                b64Entropy = shannon_entropy(string, BASE64_CHARS)
                if b64Entropy > 4.5:
                    strings_found.append(string)
                    printable_diff = printable_diff.replace(string, bcolors.WARNING + string + bcolors.ENDC)

            for string in hex_strings:
                hexEntropy = shannon_entropy(string, HEX_CHARS)
                if hexEntropy > 3:
                    strings_found.append(string)
                    printable_diff = printable_diff.replace(string, bcolors.WARNING + string + bcolors.ENDC)

    entropic_diff = None

    if len(strings_found) > 0:
        entropic_diff = {}
        entropic_diff["date"] = commit_time
        entropic_diff["path"] = blob.b_path if blob.b_path else blob.a_path
        entropic_diff["branch"] = branch_name
        entropic_diff["commit"] = prev_commit.message
        entropic_diff["diff"] = blob.diff.decode("utf-8", errors="replace")
        entropic_diff["strings_found"] = strings_found
        entropic_diff["printDiff"] = printable_diff
        entropic_diff["commit_hash"] = prev_commit.hexsha
        entropic_diff["reason"] = "High Entropy"

    return entropic_diff


def diff_worker(
    diff,
    curr_commit: Commit,
    prev_commit: Commit,
    branch_name: str,
    commit_hash: str,
    custom_regexes,
    do_entropy: bool,
    do_regex: bool,
    print_json: bool,
    surpress_output: bool,
):
    issues = []

    for blob in diff:
        printable_diff: str = blob.diff.decode("utf-8", errors="replace")

        if printable_diff.startswith("Binary files"):
            continue

        commit_time = datetime.datetime.fromtimestamp(prev_commit.committed_date).strftime(
            "%Y-%m-%d %H:%M:%S"
        )

        found_issues = []

        if do_entropy and (
            entropic_diff := find_entropy(
                printable_diff, commit_time, branch_name, prev_commit, blob, commit_hash
            )
        ):
            found_issues.append(entropic_diff)

        if do_regex:
            found_issues += regex_check(
                printable_diff, commit_time, branch_name, prev_commit, blob, commit_hash, custom_regexes
            )

        if not surpress_output:
            for found_issue in found_issues:
                print_results(print_json, found_issue)

        issues += found_issues

    return issues


def handle_results(output: dict, output_dir: str, found_issues: list) -> dict:
    for found_issue in found_issues:
        result_path = os.path.join(output_dir, str(uuid.uuid4()))

        with open(result_path, "w+") as result_file:
            result_file.write(json.dumps(found_issue))

        output["found_issues"].append(result_path)

    return output


def clone_git_repo(git_url) -> str:
    project_path = tempfile.mkdtemp()
    Repo.clone_from(git_url, project_path)
    return project_path


def find_strings(
    git_url,
    since_commit=None,
    max_depth=1000000,
    print_json=False,
    do_regex=False,
    do_entropy=True,
    surpress_output=True,
    custom_regexes={},
    branch=None,
):
    project_path = clone_git_repo(git_url)
    repo = Repo(project_path)
    already_searched = set()
    output_dir = tempfile.mkdtemp()

    output = {
        "found_issues": [],
        "project_path": project_path,
        "clone_uri": git_url,
        "issues_path": output_dir,
    }

    if branch:
        branches = repo.remotes.origin.fetch(branch)
    else:
        branches = repo.remotes.origin.fetch()

    for remote_branch in branches:
        since_commit_reached = False
        branch_name = remote_branch.name
        prev_commit = None
        curr_commit = None
        commit_hash = None

        for curr_commit in repo.iter_commits(branch_name, max_count=max_depth):
            commit_hash = curr_commit.hexsha

            if commit_hash == since_commit:
                since_commit_reached = True

            if since_commit and since_commit_reached:
                prev_commit = curr_commit
                continue

            # if not prev_commit, then curr_commit is the newest commit. And we have nothing to diff with.
            # But we will diff the first commit with NULL_TREE here to check the oldest code.
            # In this way, no commit will be missed.
            diff_hash = hashlib.md5((str(prev_commit) + str(curr_commit)).encode("utf-8")).digest()
            if not prev_commit:
                prev_commit = curr_commit
                continue
            elif diff_hash in already_searched:
                prev_commit = curr_commit
                continue
            else:
                diff = prev_commit.diff(curr_commit, create_patch=True)

            # avoid searching the same diffs
            already_searched.add(diff_hash)

            found_issues = diff_worker(
                diff,
                curr_commit,
                prev_commit,
                branch_name,
                commit_hash,
                custom_regexes,
                do_entropy,
                do_regex,
                print_json,
                surpress_output,
            )

            output = handle_results(output, output_dir, found_issues)
            prev_commit = curr_commit

        if not curr_commit:
            logger.warning(f"Looks like branch {branch_name} doesn't have any commits! Skipping")
            continue

        # Handling the first commit
        diff = curr_commit.diff(NULL_TREE, create_patch=True)

        found_issues = diff_worker(
            diff,
            curr_commit,
            prev_commit,
            branch_name,
            commit_hash,
            custom_regexes,
            do_entropy,
            do_regex,
            print_json,
            surpress_output,
        )

        output = handle_results(output, output_dir, found_issues)

    return output


# TODO: Double-check default do_entropy
@click.command(context_settings=dict(help_option_names=["-h", "--help"]))
@click.argument(
    "git_url",
    type=str,
)
@click.option(
    "--json",
    "output_json",
    is_flag=True,
    help="Output in JSON",
)
@click.option(
    "--regex",
    "do_regex",
    is_flag=True,
    help="Enable high signal regex checks",
)
@click.option(
    "--rules",
    "rules_file",
    type=click.Path(exists=True, dir_okay=False, resolve_path=True, path_type=Path),
    help="Ignore default regexes and source from json list file",
)
@click.option(
    "--entropy",
    "do_entropy",
    default=True,
    is_flag=True,
    help="Enable entropy checks",
)
@click.option(
    "--since-commit",
    type=str,
    default=None,
    help="Only scan from a given commit hash",
)
@click.option(
    "--max_depth",
    type=int,
    default=1_000_000,
    help="The max commit depth to go back when searching for secrets",
)
@click.option(
    "--branch",
    type=str,
    default=None,
    help="Name of the branch to be scanned",
)
def main(
    git_url: str,
    output_json: bool = False,
    do_regex: bool = False,
    rules_file: Path | None = None,
    do_entropy: bool = True,
    since_commit: str = "",
    max_depth: int = 1000000,
    branch: str | None = None,
):
    """Find secrets hidden in the depths of git."""

    rules = {}
    if rules_file:
        try:
            with open(rules_file) as ruleFile:
                rules = json.loads(ruleFile.read())
                for rule in rules:
                    rules[rule] = re.compile(rules[rule])
        except (OSError, ValueError) as e:
            raise click.ClickException(f"Error reading rules file {rules_file}")
        for regex in dict(regexes):
            del regexes[regex]
        for regex in rules:
            regexes[regex] = rules[regex]

    logger.debug(f"{rules = }")

    output = find_strings(
        git_url,
        since_commit,
        max_depth,
        output_json,
        do_regex,
        do_entropy,
        surpress_output=False,
        branch=branch,
    )

    project_path = output.get("project_path")
    if project_path:
        Path(project_path).rmdir_p()

    if output["found_issues"]:
        sys.exit(1)
    else:
        sys.exit(0)


if __name__ == "__main__":
    main()
