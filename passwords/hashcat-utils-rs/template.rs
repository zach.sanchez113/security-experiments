use std::env;
use std::io;
use std::io::Write;
use std::process::exit;

fn main() -> io::Result<()> {
    let args: Vec<String> = env::args().collect();

    dbg!(&args);

    if args.len() == 1 || (args.len() == 2 && args[1] == "-h" || args[1] == "--help") {
        println!("Usage: {}", &args[0]);
        exit(1);
    }

    // Read from stdin
    let mut buffer = String::new();
    while io::stdin().read_line(&mut buffer)? != 0 {
        let buffer = std::mem::take(&mut buffer);

        // Skip if this is just a newline
        if buffer.trim() == "" {
            continue;
        }

        dbg!(&buffer);

        // TODO: Insert checks

        // Write to stdout if all checks succeeded
        io::stdout().write_all(buffer.as_bytes())?;
    }

    Ok(())
}
