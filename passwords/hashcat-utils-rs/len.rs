use std::env;
use std::io;
use std::io::Write;
use std::process::exit;

fn main() -> io::Result<()> {
    let args: Vec<String> = env::args().collect();

    dbg!(&args);

    if args.len() < 3
        || args.len() > 5
        || (args.len() != 2 && args[1] == "-h" || args[1] == "--help")
    {
        println!("Usage: {} [--min N] [--max N] < infile > outfile", &args[0]);
        exit(1);
    }

    // Set everything to zero so we can skip wherever it's needed
    let mut min_length = 0;
    let mut max_length = 0;
    let mut min_length_idx = 0;
    let mut max_length_idx = 0;

    // Parse out the first option
    if args[1] == "--min" {
        min_length_idx = 2;
    } else if args[1] == "--max" {
        max_length_idx = 2;
    } else {
        eprintln!("Unknown option: {}", args[1]);
        exit(1);
    }

    // Parse out the second option if given
    if args.len() > 3 {
        if args[3] == "--min" {
            min_length_idx = 4;
        } else if args[3] == "--max" {
            max_length_idx = 4;
        } else {
            eprintln!("Unknown option: {}", args[1]);
            exit(1);
        }
    }

    dbg!(&min_length_idx, &max_length_idx);

    // Extract + convert each option
    if min_length_idx > 0 {
        match &args[min_length_idx].parse::<usize>() {
            Ok(n) => {
                min_length = *n;
            }
            Err(_e) => {
                eprintln!("Minimum length must be an integer!");
                exit(1);
            }
        };
    }

    if max_length_idx > 0 {
        match &args[max_length_idx].parse::<usize>() {
            Ok(n) => {
                max_length = *n;
            }
            Err(_e) => {
                eprintln!("Maximum length must be an integer!");
                exit(1);
            }
        };
    }

    dbg!(&min_length, &max_length);

    // Read from stdin
    let mut buffer = String::new();
    while io::stdin().read_line(&mut buffer)? != 0 {
        let buffer = std::mem::take(&mut buffer);

        // Skip if this is just a newline
        if buffer.trim() == "" {
            continue;
        }

        // TODO: Graphemes (for a create, see: https://stackoverflow.com/a/46290728/11832705)
        let buf_length = buffer.chars().count();

        dbg!(&buffer, &buf_length);

        // Check length against min/max
        if min_length > 0 && buf_length < min_length {
            continue;
        }

        if max_length > 0 && buf_length > max_length {
            continue;
        }

        // Write to stdout if all checks succeeded
        io::stdout().write_all(buffer.as_bytes())?;
    }

    Ok(())
}
