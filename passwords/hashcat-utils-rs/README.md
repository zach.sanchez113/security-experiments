# hashcat-utils-rs

This is a Rust port of the hashcat utils, which I'm doing for two purposes:

1. To practice writing Rust (this is my first attempt)
2. To add some extra functionality that is missing from the hashcat utils

May also be able to combine some utilities into a "password policy" binary at some point.
