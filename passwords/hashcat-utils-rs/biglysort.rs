// Based on: https://betterprogramming.pub/how-to-sort-a-20g-file-in-rust-12abfffbd92b

use std::fs::{remove_file, File};
use std::io::Write;
use std::io::{self, BufWriter};
use std::io::{BufRead, BufReader, Lines};
use std::path::{Path, PathBuf};
use std::process::exit;
use std::{env, mem};

use itertools::Itertools;
use tempfile::{tempdir, TempDir};

// TODO: 4_000_000_000?
const BUFFER_CAPACITY: usize = 10_000_000;
const MAX_MEM_USE: usize = 10_000_000;

/// Remove a string from a vector, meant to prevent duplicates
///
/// # Arguments
///
/// * `value` - String to remove
/// * `vector` - Vector to remove string from
fn remove_string_from_vector(value: &String, vector: &mut Vec<String>) {
    vector.retain(|elem| *elem != *value);
}

/// Do final cleanup
/// TODO: Remove entire tmp dir?
fn clean_up(file_names: &Vec<String>) {
    file_names.iter().for_each(|f| remove_file(f).unwrap());
}

/// Merge multiple files into one
/// TODO: Don't interpret lines as f64 (it's from the reference article)
fn merge(tmp_file_names: &Vec<String>, file_name: &str) -> io::Result<()> {
    let result_file_name = format!("{}-sorted.txt", file_name.strip_suffix(".txt").unwrap());
    let result_file: File;

    match File::create(&result_file_name) {
        Ok(f) => result_file = f,
        Err(_) => {
            eprintln!("Failed to create output file: {}", result_file_name);
            exit(1);
        }
    }

    let mut file = BufWriter::with_capacity(BUFFER_CAPACITY, result_file);

    let mut active_readers = tmp_file_names
        .iter()
        .map(|name| BufReader::with_capacity(BUFFER_CAPACITY, File::open(name).unwrap()).lines())
        .collect::<Vec<Lines<BufReader<File>>>>();

    let mut values = active_readers
        .iter_mut()
        .map(|r| r.next().unwrap().unwrap().parse::<f64>().unwrap())
        .collect::<Vec<f64>>();

    while active_readers.len() > 0 {
        // Get the minimum value, write it to the final output file
        let (i, min_val) = values
            .iter()
            .enumerate()
            .min_by(|(_, a), (_, b)| a.partial_cmp(b).unwrap())
            .unwrap();

        writeln!(file, "{}", min_val)?;

        // Replace w/ a new line from a reader, or just remove it if we're done
        if let Some(x) = active_readers[i].next() {
            values[i] = x.unwrap().parse::<f64>().unwrap();
        } else {
            values.remove(i);
            active_readers.remove(i);
        }
    }

    file.flush().unwrap();

    Ok(())
}

/// Sort a vector, write values to a file
fn sort_and_write_to_file(
    lines: &mut Vec<f64>,
    tmp_dir: &TempDir,
    tmp_file_names: &mut Vec<String>,
) -> io::Result<()> {
    // Sort the lines
    lines.sort_by(|a, b| a.partial_cmp(b).unwrap());

    // Create the temporary file
    let tmp_file_name = format!("tmp_sort_{}.txt", tmp_file_names.len());

    let tmp_file: File = File::create(&tmp_dir.path().join(&tmp_file_name)).unwrap();

    let mut buffer = BufWriter::with_capacity(BUFFER_CAPACITY, tmp_file);

    for line in lines.iter() {
        writeln!(buffer, "{}", line)?;
    }

    buffer.flush().unwrap();

    tmp_file_names.push(tmp_file_name.to_string());
    lines.clear();

    Ok(())
}

/// Main external sorting logic.
///
/// TODO: max_mem_use arg
/// TODO: Don't interpret lines as f64
///
/// # Arguments
///
/// * `filename` - File to sort.
/// * `max_mem_use` - Maximum memory to use during sorting operations.
fn extern_sort(filename: &str, max_mem_use: usize, tmp_dir: &TempDir) -> io::Result<()> {
    let file_buffer: BufReader<File>;

    match File::open(filename) {
        Ok(f) => {
            file_buffer = BufReader::with_capacity(BUFFER_CAPACITY, f);
        }
        Err(_) => {
            eprint!("Failed to open file: {}", filename);
            exit(1);
        }
    }

    let mut current_lines = vec![];
    let mut tmp_file_names = vec![];

    for line in file_buffer.lines() {
        current_lines.push(line.unwrap().parse::<f64>().unwrap());

        if mem::size_of::<f64>() * current_lines.len() > MAX_MEM_USE {
            sort_and_write_to_file(&mut current_lines, tmp_dir, &mut tmp_file_names)?;
        }
    }

    // Process any lines that didn't divide neatly into max memory
    if current_lines.len() > 0 {
        sort_and_write_to_file(&mut current_lines, tmp_dir, &mut tmp_file_names)?;
    }

    // Merge everything
    merge(&tmp_file_names, filename)?;

    // Remove temporary files
    clean_up(&tmp_file_names);

    Ok(())
}

/// TODO: Add support for specific temporary directory (e.g. tmpfs is small but file is large)
/// TODO: See if Err should be returned
/// TODO: Don't just propagate errors to main (need better patterns)
fn main() {
    let args: Vec<String> = env::args().collect();

    dbg!(&args);

    if args.len() != 3 || (args[1] == "-h" || args[1] == "--help") {
        println!("Usage: {} INPUT_FILE OUTPUT_FILE [TMP_DIR]", &args[0]);
        exit(1);
    }

    let filename: String = String::from(&args[1]);
    let tmp_dir: TempDir = TempDir::new_in(&args[3]).unwrap_or(tempdir().unwrap());

    dbg!(&filename);
    dbg!(&tmp_dir);

    match Path::new(&filename).try_exists() {
        Err(_) => {
            eprintln!("Couldn't locate file: {}", filename);
            exit(1);
        }
        _ => (),
    }

    match tmp_dir.path().try_exists() {
        Err(_) => {
            eprintln!("Temp directory doesn't exist: {}", tmp_dir.path().display());
            exit(1);
        }
        _ => (),
    }
}
