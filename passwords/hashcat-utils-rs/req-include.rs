use std::env;
use std::io;
use std::io::Write;
use std::process::exit;

const LOWER: i32 = 1 << 0;
const UPPER: i32 = 1 << 1;
const DIGIT: i32 = 1 << 2;
const SYMBOL: i32 = 1 << 3;
const OTHER: i32 = 1 << 4;

fn main() -> io::Result<()> {
    let args: Vec<String> = env::args().collect();

    dbg!(&args);

    if args.len() != 2 || (args.len() != 2 && args[1] == "-h" || args[1] == "--help") {
        println!("Extract words with at least one character matching 'inc_mask'\n");
        println!("Usage: {} inc_mask", &args[0]);
        println!("  'inc_mask' is the mask of required (at least one character of EACH) types");
        println!("  type masks: add together the numbers, i.e. lower + upper = 3");
        println!("     LOWER  1");
        println!("     UPPER  2");
        println!("     DIGIT  3");
        println!("     SYMBOL 4 (0x20 to 0x7e NOT IN lower, upper, digit)");
        println!("     OTHER  5 (tab, high ASCII, etc.)");
        exit(1);
    }

    let req_mask: i32;

    match &args[1].parse::<i32>() {
        Ok(n) => req_mask = *n,
        Err(_e) => {
            eprintln!("Maximum length must be an integer!");
            exit(1);
        }
    };

    if req_mask <= 0 || req_mask >= 32 {
        eprintln!("The mask must be between 1 and 31!");
        exit(1);
    }

    dbg!(req_mask);

    // Read from stdin
    let mut buffer = String::new();
    while io::stdin().read_line(&mut buffer)? != 0 {
        let mut buffer = std::mem::take(&mut buffer);

        buffer = buffer.trim().to_string();

        // Skip if the buffer is empty
        if buffer == "" {
            continue;
        }

        dbg!(&buffer);

        let mut curr_mask = 0;

        for c in buffer.chars() {
            if c >= 'a' && c <= 'z' {
                curr_mask |= LOWER;
            } else if c >= 'A' && c <= 'Z' {
                curr_mask |= UPPER;
            } else if c >= '0' && c <= '9' {
                curr_mask |= DIGIT;
            } else if c >= '\u{20}' && c <= '\u{7e}' {
                curr_mask |= SYMBOL;
            } else {
                curr_mask |= OTHER;
            }
        }

        // println!("Current mask is {:#06b}, expecting: {:#06b}", curr_mask, req_mask);

        if curr_mask & req_mask != req_mask {
            continue;
        }

        // Repair this now that checks are done
        buffer.push_str("\n");
        dbg!(&buffer);

        // Write to stdout if all checks succeeded
        io::stdout().write_all((buffer).as_bytes())?;
    }

    Ok(())
}
