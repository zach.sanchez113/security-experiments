#!/usr/bin/env python3

from __future__ import annotations

import os
import re
import sys

import rich_click as click
from loguru import logger
from more_itertools import chunked
from path import Path

# Config for rich-click
click.rich_click.SHOW_ARGUMENTS = True
click.rich_click.SHOW_METAVARS_COLUMN = False
click.rich_click.APPEND_METAVARS_HELP = True
click.rich_click.STYLE_OPTIONS_TABLE_PAD_EDGE = True
click.rich_click.COMMAND_GROUPS = {}

# Grab the Rich console
console = click.rich_click._get_rich_console()

# Base logging configuration
LOG_FORMAT = (
    "<green>{time:YYYY-MM-DD HH:mm:ss}</green> | <level>{level: <8}</level> | <level>{message}</level>"
)

logger.configure(
    handlers=[
        dict(sink=sys.stdout, level=os.environ.get("LOGURU_LEVEL", "INFO"), format=LOG_FORMAT),
    ],
    levels=[
        {"name": "INFO", "color": "<blue><bold>"},
        {"name": "DEBUG", "color": "<magenta><bold>"},
    ],
)


def grammer(
    words: list[str],
    gram_size: int,
    remove_whitespace: bool = False,
    include_punctuation: bool = False,
) -> list[str]:
    """Create N-grams for a list of words.

    Args:
        words (list[str]): Words for N-grams.
        gram_size (int): The number of words per gram.
        remove_whitespace (bool, optional): Remove whitespace from each gram. Recommended since hashcat rules
                                            can remove these. Defaults to False.
        include_punctuation (bool, optional): Include special characters in each gram. Defaults to False.

    Returns:
        list[str]: List of N-grams.
    """

    if gram_size == 1:
        return list(set(words))

    grams = set()
    for chunk in chunked(words, gram_size):
        if remove_whitespace:
            chunk = "".join(chunk)
        else:
            chunk = " ".join(chunk)

        # Keep only alphanum, along with hyphen + single quote to account for names
        # TODO: Might be missing some edge cases
        if not include_punctuation:
            chunk = re.sub(r"[^a-zA-Z0-9\-\']+", "", chunk.strip("-").strip("-"))

            # Remove hyphens with spaces on both sides
            chunk = re.sub(r" - ", "", chunk)

            # Remove single quotes surrounding words
            chunk = re.sub(r"\b'([\w]+)'\b", r"\1", chunk)

        # Dedupe multiple spaces just in case
        grams.add(re.sub(r"[\s]{2,}", " ", chunk))

    return list(grams)


@click.command(
    no_args_is_help=True,
    context_settings=dict(
        show_default=True,
        help_option_names=["-h", "--help"],
    ),
)
@click.argument(
    "source_text",
    type=click.Path(exists=True, dir_okay=False, resolve_path=True, path_type=Path),
)
@click.argument(
    "output_file",
    type=click.Path(exists=False, dir_okay=False, writable=True, resolve_path=True, path_type=Path),
)
@click.option(
    "--min-size",
    default=1,
    type=click.IntRange(1, None),
    help="Lower bound of gram size.",
)
@click.option(
    "--max-size",
    default=None,
    type=click.IntRange(1, None),
    help="Upper bound of gram size if multiple sizes are desired.",
)
@click.option(
    "--remove-whitespace",
    is_flag=True,
    help="Remove whitespace from each N-gram.",
)
@click.option(
    "--include-punctuation",
    is_flag=True,
    help="Include punctuation in N-grams.",
)
@click.option(
    "--debug",
    is_flag=True,
    help="Enable debug logging.",
)
def cli(
    source_text: Path,
    output_file: Path,
    min_size: int = 2,
    max_size: int | None = None,
    remove_whitespace: bool = False,
    include_punctuation: bool = False,
    debug: bool = False,
):
    """Split a text file into N-grams.

    This is a script I wrote during CMIYC 2022 to generate wordlists from books, scripts, etc.
    Hopefully you find good use for it as well.
    """

    if debug:
        logger.remove()
        logger.add(sink=sys.stdout, level="DEBUG", format=LOG_FORMAT)

    try:
        if max_size is None:
            max_size = min_size

        if max_size < min_size:
            raise click.ClickException(f"Minimum gram size cannot be larger than the maximum!")

        # Load in the source file only once
        lines = [line.strip() for line in Path(source_text).expand().lines(retain=False) if line.strip()]

        # Join all lines, replace multiple spaces with single, then split each word
        text = re.sub(r"[\s]{2,}", " ", " ".join(lines))
        words = text.split(" ")

        grams = []
        for gram_size in range(min_size, max_size + 1):
            grams.extend(
                grammer(
                    words=words,
                    gram_size=gram_size,
                    include_punctuation=include_punctuation,
                    remove_whitespace=remove_whitespace,
                )
            )

        Path(output_file).expand().write_lines(grams)

    except Exception as e:
        console.print_exception(show_locals=False)
        raise click.Abort


if __name__ == "__main__":
    cli()
