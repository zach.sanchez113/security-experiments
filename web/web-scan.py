#!/usr/bin/env python3

from __future__ import annotations

import csv
import json
import os
import re
import subprocess
import sys
from collections.abc import Mapping, Sequence
from typing import Any

import dynaconf.utils.functional
import requests
import rich_click as click
import urllib3
from loguru import logger
from path import Path

# No warnings for this during scanning, please
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# Config for rich-click
click.rich_click.SHOW_ARGUMENTS = True
click.rich_click.SHOW_METAVARS_COLUMN = False
click.rich_click.APPEND_METAVARS_HELP = True
click.rich_click.STYLE_OPTIONS_TABLE_PAD_EDGE = True
click.rich_click.COMMAND_GROUPS = {}

# Grab the Rich console
console = click.rich_click._get_rich_console()

# Base logging configuration
LOG_FORMAT = (
    "<green>{time:YYYY-MM-DD HH:mm:ss}</green> | <level>{level: <8}</level> | <level>{message}</level>"
)

logger.configure(
    handlers=[
        dict(sink=sys.stdout, level=os.environ.get("LOGURU_LEVEL", "INFO"), format=LOG_FORMAT),
    ],
    levels=[
        {"name": "INFO", "color": "<blue><bold>"},
        {"name": "DEBUG", "color": "<magenta><bold>"},
    ],
)


class LoguruCatchGroup(click.RichGroup):
    """Subclass of RichGroup that wraps commands in loguru's `logger.catch` context manager."""

    def __call__(self, *args, **kwargs):
        with logger.catch(onerror=lambda e: sys.exit(1), exclude=(click.ClickException, click.Abort)):
            return super().__call__(*args, **kwargs)


class LoguruCatchCommand(click.RichCommand):
    """Subclass of RichCommand that wraps commands in loguru's `logger.catch` context manager."""

    def __call__(self, *args, **kwargs):
        with logger.catch(onerror=lambda e: sys.exit(1), exclude=(click.ClickException, click.Abort)):
            return super().__call__(*args, **kwargs)


def walk_nested(
    obj: Sequence[Any] | Mapping[Any, Any] | dynaconf.utils.functional.LazyObject,
    *args: Any,
    default: Any = None,
    all_or_nothing: bool = True,
) -> Any:
    """Walk the nested objects inside 'obj' with the specified *args as key names and return the value at the end.

    This is slightly more flexible than obj.get('foo', {}).get('bar', {}).get('baz') because a TypeError can be thrown
    if the key name exists but is not a dictionary.

    Examples:
        obj = {'foo': {'bar': {'baz': 1}}, 'buzz': 'ERROR' }
        walk_nested(obj, 'foo', 'bar', 'baz')  # 1
        walk_nested(obj, 'foo', 'bar', 'buzz', all_or_nothing=False)  # {'baz': 1}
        walk_nested(obj, 'buzz', 'baz', all_or_nothing=False) # 'ERROR'

    Args:
        obj (Sequence | Mapping): A dictionary/iterable-like object containing nested dictionaries/elements.
        *args (Any): The individual keys and/or indices to walk.
        default (Any, optional): Default value to use if an exception occurs. Defaults to None.
        all_or_nothing (bool, optional): If True and an error is encountered, return the default. If False, return the most recent obj. Defaults to True.

    Returns:
        Any: The return value, determined per above.
    """

    try:
        for arg in args:
            obj = obj[arg]
        return obj
    except (KeyError, TypeError, IndexError, AttributeError):
        return default if all_or_nothing else obj


def parse_gnmap_file(filepath: Path, debug: bool = False) -> dict[str, Any]:
    """Extract hosts/ports/service info from a gnmap file (TSV)."""

    scan_results = {}

    with open(Path(filepath)) as f:
        reader = csv.reader(f, delimiter="\t")
        for row in reader:
            logger.debug(f"{row = }")

            if not row or row[0].startswith("#"):
                continue

            try:
                host = re.match(r"Host: ([\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}) .*", row[0]).group(1)

                if host not in scan_results:
                    scan_results[host] = {"status": None, "ports": []}

                if "port" in row[1].lower():
                    for port in re.match(r"Ports: (.*)", row[1]).group(1).split("/, "):
                        num, state, protocol, _, name, _, description = port.split("/", maxsplit=6)
                        port_data = {
                            "port": int(num),
                            "state": state,
                            "protocol": protocol,
                            "name": name,
                            "description": description.rstrip(
                                "/"
                            ),  # because the last port won't have the "/" removed
                        }

                        logger.debug(f"Found port for host {host}:")
                        if debug:
                            console.print(port_data)

                        scan_results[host]["ports"].append(port_data)

                elif "status" in row[1].lower():
                    scan_results[host]["status"] = re.match(r"Status: (.*)", row[1]).group(1)

            except Exception as e:
                logger.warning(f"Continuing on error: {e}")
                continue

                # console.print_exception(show_locals=True)
                # sys.exit(1)

    return scan_results


def determine_url(host: str, port: int) -> str | None:
    """Attempt to perform HTTP GET against host/port, return response (redirect) URL if successful."""

    if port == 80:
        url = f"http://{host}"
    elif port == 443:
        url = f"https://{host}"
    else:
        url = f"http://{host}:{port}"

    try:
        return requests.request(method="GET", url=url, verify=False, timeout=5).url
    except requests.exceptions.SSLError:
        return f"https://{host}" if port in [80, 443] else f"https://{host}:{port}"
    except Exception as e:
        logger.warning(f"Exception for {host}:{port} - {e.__class__}: {e}")


def run_zgrab2(host: str, port: int, url: str, output_file: Path) -> None:
    logger.info(f"Running zgrab2 for {url}...")

    zgrab2_args = [
        "-f-",
        f"-o {output_file}",
        f"-p {port}",
        "--heartbleed",
        "--retry-https",
        "--fail-http-to-https",
        "--redirects-succeed",
    ]

    command = f"echo '{host}' | zgrab2 http {' '.join(zgrab2_args)}"
    result = subprocess.run(command, shell=True, capture_output=True)

    logger.debug(f"{command = }")
    logger.debug(f"{result.returncode = }")
    logger.debug(f"{result.stdout.decode() = }")
    logger.debug(f"{result.stderr.decode() = }")

    if result.returncode != 0:
        logger.error(f"Failed to run zgrab2 for {url}: {result.stderr.decode()}")


def run_wappalyzer(url: str, output_file: Path) -> None:
    logger.info(f"Running wappalyzer for {url}...")

    command = f"wappalyzer '{url}' --pretty --extended | tee {output_file}"
    result = subprocess.run(command, shell=True, capture_output=True)

    logger.debug(f"{command = }")
    logger.debug(f"{result.returncode = }")
    logger.debug(f"{result.stdout.decode() = }")
    logger.debug(f"{result.stderr.decode() = }")

    if result.returncode != 0:
        logger.error(f"Failed to run wappalyzer for {url}: {result.stderr.decode()}")


def run_feroxbuster(url: str, output_file: Path, keep_hta: bool = False) -> None:
    logger.info(f"Running feroxbuster for {url}...")

    feroxbuster_args = [
        "--no-state",
        f"--url {url}",
        "-k",
        "--redirects",
        "--time-limit 5m",
        "--extract-links",
        "--collect-words",
        "--collect-backups",
        "--wordlist /usr/share/dirb/wordlists/common.txt",
        "--json",
        f"--output '{output_file}'",
    ]

    command = f"feroxbuster {' '.join(feroxbuster_args)}"
    result = subprocess.run(command, shell=True, capture_output=True)

    logger.debug(f"{command = }")
    logger.debug(f"{result.returncode = }")
    logger.debug(f"{result.stdout.decode() = }")
    logger.debug(f"{result.stderr.decode() = }")

    if result.returncode != 0:
        logger.error(f"Failed to run feroxbuster for {url}: {result.stderr.decode()}")

    else:
        logger.debug(f"Fixing feroxbuster JSON output for {url}...")
        feroxbuster_result = [json.loads(line) for line in output_file.lines(retain=False)]

        if not keep_hta:
            feroxbuster_result = [r for r in feroxbuster_result if "/.ht" not in r.get("url")]

        output_file.write_text(json.dumps(feroxbuster_result, indent=2))


def run_wpscan():
    ...


def run_joomscan():
    ...


def run_droopescan():
    ...


@click.group(cls=LoguruCatchGroup, context_settings=dict(help_option_names=["-h", "--help"]))
def cli():
    """Script for web scanning."""


@cli.command(no_args_is_help=True, context_settings=dict(help_option_names=["-h", "--help"]))
@click.argument(
    "gnmap_output",
    type=click.Path(exists=True, dir_okay=False, readable=True, resolve_path=True, path_type=Path),
)
@click.option("--keep-hta", is_flag=True, help="Keep /.ht* files in feroxbuster output.")
@click.option("--debug", is_flag=True, help="Enable debug logging.")
def run(gnmap_output: Path, keep_hta: bool = False, debug: bool = False):
    """Entrypoint for web scanning.

    TODO: Parse the XML for script results
    """

    if debug:
        logger.remove()
        logger.add(sink=sys.stdout, level="DEBUG", format=LOG_FORMAT)

    scan_results = parse_gnmap_file(gnmap_output, debug=debug)
    console.print(scan_results)

    Path("./web-scan-gnmap.json").write_text(json.dumps(scan_results, indent=2))

    urls = set()

    for host, data in scan_results.items():
        if data.get("status") == "Down":
            continue

        ports = data.get("ports", [])

        for port in ports:
            number = port.get("port")

            # False positive that's kinda annoying
            if "Microsoft Windows RPC over HTTP" in port.get("description"):
                continue

            if (
                number in [80, 443, 3000, 8000, 8080, 8888]
                or "http" in port.get("name").lower()
                or "http" in port.get("description").lower()
            ):
                if url := determine_url(host, number):
                    urls.add(url)
                else:
                    urls.add(f"http://{host}" if number not in [80, 443] else f"http://{host}:{port}")
                    urls.add(f"https://{host}" if number not in [80, 443] else f"https://{host}:{port}")

    Path("./web-scan-urls.json").write_text(json.dumps(list(urls), indent=2))

    for url in urls:
        logger.debug(f"Processing {url = }")

        match = re.match(r"http(?:s)?//([\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3})(:[\d]{1,5})?", url)
        if not match:
            raise Exception(f"Failed to match url?? {url = }")

        logger.debug(match.groups())

        host, port = match.groups()

        if port:
            port = int(match.group(2).lstrip(":"))
        elif "https" in url:
            port = 443
        else:
            port = 80

        logger.debug(f"Extracted {host = }, {port = }")

        Path("./zgrab2").expand().mkdir_p()
        output_file = Path(f"./zgrab2/{host}.json")

        run_zgrab2(host=host, port=port, url=url, output_file=output_file)

        if output_file.exists():
            heartbleed_vulnerable = walk_nested(
                json.loads(output_file.read_text()),
                "data",
                "http",
                "result",
                "response",
                "request",
                "tls_log",
                "heartbleed_log",
                "heartbleed_vulnerable",
                default=False,
            )

            if heartbleed_vulnerable:
                logger.success(f"Found heartbleed for: {url}")

        # TODO: .technologies.slug -> wordpress, mysql...
        # TODO: .technologies.version -> php version
        # TODO: joomscan
        Path("./wappalyzer").mkdir_p()
        output_file = Path(f"./wappalyzer/{host}.json")
        run_wappalyzer(url=url, output_file=output_file)

        if output_file.exists():
            data = json.loads(output_file.read_text())
            technologies = walk_nested(data, "technologies", default=[])

            # TODO: droopescan
            if any(t["slug"] == "drupal" for t in technologies):
                ...

        # TODO: May not always wanna remove /.ht*
        # TODO: Check wp*
        Path("./feroxbuster").mkdir_p()
        output_file = Path(f"./feroxbuster/{host}.json")
        run_feroxbuster(url=url, output_file=output_file, keep_hta=keep_hta)

        if output_file.exists():
            data = json.loads(output_file.read_text())
            urls = [d["url"] for d in data if walk_nested(d, "url", default="")]
            urls += [
                d["message"].split(" ")[3]
                for d in data
                if "directory listing" in walk_nested(d, "message", default="")
            ]

            # TODO: wpscan
            if wp_includes := [url for url in urls if re.match(r"wp-includes/$", url)]:
                wp_base_url = re.sub(r"wp-includes/", "", wp_includes[0])


if __name__ == "__main__":
    cli()
