#!/usr/bin/env bash

# Run wappalyzer to determine tech stack in use
#
# Args:
#   $1: uri
#
# Returns:
#   JSON output of wappalyzer
#
function run_wappalyzer() {

  local uri="${1:?}"

  wappalyzer "$uri" --pretty --extended

}
