#!/usr/bin/env bash

################################################################################
#
# Web scanning script for initial recon
#
################################################################################

##################################################
#
# Utility functions copy/pasted from other scripts
#
##################################################

# Run zgrab2 to grab basic HTTP info
#
# Includes checks for heartbleed, re-attempts with HTTPS if needed
#
# Args:
#   $1: http_host
#   $2: http_port
#
# Returns:
#   JSON output of zgrab2
#
function run_zgrab2() {

  local http_host="${1:?}"
  local http_port="${2:?}"

  echo -e "$http_host" \
    | zgrab2 http -f- -p "$http_port"  --heartbleed --retry-https --fail-http-to-https --redirects-succeed \
    | jq

}

# Run wappalyzer to determine tech stack in use
#
# Args:
#   $1: uri
#
# Returns:
#   JSON output of wappalyzer
#
function run_wappalyzer() {

  local uri="${1:?}"

  wappalyzer "$uri" --pretty --extended

}

# Run feroxbuster to perform forced browsing/spidering
#
# Args:
#   $1: URI
#   $2: Output file for JSON
#
function run_feroxbuster() {

  local uri="${1:?}"
  local output_file="${2:?}"

  feroxbuster --no-state --url "$uri" -k --redirects \
    --time-limit 5m \
    --extract-links \
    --collect-words \
    --collect-backups \
    --wordlist /usr/share/dirb/wordlists/common.txt \
    --json \
    --output "${output_file}"

    # ! Don't use this, auto-tune is pretty annoying
    # --smart
    # TODO
    # --auto-bail
    # ! extra wordlists
    # --wordlist /usr/share/dirb/wordlists/big.txt
    # --wordlist /usr/share/seclists/Discovery/Web-Content/
    # --wordlist /usr/share/dirbuster/wordlists/directory-list-2.3-small.txt
    # ! kinda noisy
    # --thorough
    # --collect-words
    # --collect-extensions

}


##################################################
#
# Main runner
#
##################################################

# Run all of the web scanning tools
#
# Args:
#   $1: http_host
#   $2: http_port
#   $3: uri
#
function run_web_scan() {

  local http_host="${1:?}"
  local http_port="${2:?}"
  local uri="${3:?}"

  # First, run zgrab2 so we have an overview of what's going on
  run_zgrab2 "$http_host" "$http_port" | tee "http/zgrab2/$http_host-$http_port.json"

  # Then check wappalyzer so we can see what technogologies we're working with
  run_wappalyzer "$uri" | tee "http/wappalyzer/$http_host-$http_port.json"

  # Now do some forced browsing/spidering
  # To remove 403: `jq '.[] | select(.status != 403)' < filename`
  run_feroxbuster "$uri" "http/feroxbuster/$http_host-$http_port.json"

}


##################################################
#
# Parse out HTTP hosts/ports, determine URL, run
# full scan suite
#
##################################################

input_file="${1:?no input file was provided (use gnmap-host-port.awk)}"

# Using gnmap-host-port.awk to get tab-delimited host/port combos for http only
mapfile http_lines < <(grep -iE "http|ssl" "${input_file}" | awk 'BEGIN { OFS=":"; } { split($2, port, "/"); print $1, port[1]; }')

for http_line in "${http_lines[@]}"; do

  http_host=$(echo "$http_line" | cut -d: -f 1)
  http_port=$(echo "$http_line" | cut -d: -f 2)

  # Can't just rely on port
  if [[ "$http_port" == "80" ]]; then
    run_scan "$http_host" "$http_port" "http://$http_host"

  elif [[ "$http_port" == "443" ]]; then
    run_scan "$http_host" "$http_port" "https://$http_host"

  else

    # Check if this is HTTPS before continuing onwards
    if curl --connect-timeout 3 "https://$http_host:$http_port" &>/dev/null; then
      run_scan "$http_host" "$http_port" "https://$http_host:$http_port"

    else
      run_scan "$http_host" "$http_port" "http://$http_host:$http_port"

    fi

  fi

done
