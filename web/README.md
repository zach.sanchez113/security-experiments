# Web

A collection of scripts related to web scanning/exploitation.

## clone-website.sh

As the name implies, clone a website. Could be useful for phishing, recreating a target environment, etc.

## sqlmap.sh

Basic reference point for sqlmap usage.

## web-scan.sh

Basic automation of the steps I do when scanning webapps.

This is practically copy/paste from what I used for PWK labs, so it's not particularly good. But, it's still handy for a quick copy/paste into my setup, so.

## [WIP] web-scan.py

Basic automation of the steps I do when scanning webapps.

## XXE

Example XXE payloads.

## ysoserial.sh

A terrible script to remind me how to use `ysoserial`, base64 encode it, then send it in a POST request.
