#!/usr/bin/env bash

# Run feroxbuster to perform forced browsing/spidering
#
# Args:
#   $1: URI
#   $2: Output file for JSON
#
function run_feroxbuster() {

  local uri="${1:?}"
  local output_file="${2:?}"

  feroxbuster --no-state --url "$uri" -k --redirects \
    --time-limit 5m \
    --extract-links \
    --collect-words \
    --collect-backups \
    --wordlist /usr/share/dirb/wordlists/common.txt \
    --json \
    --output "${output_file}"

    # ! Don't use this, auto-tune is pretty annoying
    # --smart
    # TODO
    # --auto-bail
    # ! extra wordlists
    # --wordlist /usr/share/dirb/wordlists/big.txt
    # --wordlist /usr/share/seclists/Discovery/Web-Content/
    # --wordlist /usr/share/dirbuster/wordlists/directory-list-2.3-small.txt
    # ! kinda noisy
    # --thorough
    # --collect-words
    # --collect-extensions

}
