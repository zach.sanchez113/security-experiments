#!/usr/bin/env bash

export WPSCAN_API_TOKEN=""
export TARGET_URL=""

wpscan -e vp vt cb dbe --url "${TARGET_URL}"

wpscan \
  --url "${TARGET_URL}" \
  --enumerate p \
  --plugins-detection aggressive

# Look for usernames
wpscan -e u --url "${TARGET_URL}"

# Password attack
wpscan --password-attack wp-login -U ./usernames.txt -P ./passwords --url "${TARGET_URL}"
