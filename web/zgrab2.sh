#!/usr/bin/env bash

# Run zgrab2 to grab basic HTTP info
#
# Includes checks for heartbleed, re-attempts with HTTPS if needed
#
# Args:
#   $1: http_host
#   $2: http_port
#
# Returns:
#   JSON output of zgrab2
#
function run_zgrab2() {

  local http_host="${1:?}"
  local http_port="${2:?}"

  echo -e "$http_host" \
    | zgrab2 http -f- -p "$http_port"  --heartbleed --retry-https --fail-http-to-https --redirects-succeed \
    | jq

}
