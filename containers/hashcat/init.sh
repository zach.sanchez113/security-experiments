#!/usr/bin/env bash

# Yes, this is trash.
# It's the startup script I used for vast.ai - I'll be creating a real Dockerfile soon.

# TODOs:
# - More custom rules files/hcmasks
# - Extra password cracking utilities

# FROM dizcza/docker-hashcat:latest

apt install -y p7zip-full vim zip zsh

# python build env
apt-get install make build-essential libssl-dev zlib1g-dev \
libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm \
libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev

cd /root
git clone https://gitlab.com/zach.sanchez113/linux-setup.git
cp linux-setup/\.* /root
rm /root/.zshrc
echo "alias hashcat='hashcat --backend-ignore-cuda'" >> .zshalias
echo "alias ll='ls -lhA'" >> .zshalias

mkdir -p /usr/local/src

mkdir -p \
  /usr/local/share/wordlists \
  /usr/local/share/rules \
  /usr/local/share/masks \
  /usr/local/share/potfiles \
  /usr/local/share/hashes

cd /usr/local/src

git clone https://github.com/NotSoSecure/password_cracking_rules.git
cp password_cracking_rules/*rule /usr/local/share/rules

git clone https://github.com/travco/rephraser.git
cp rephraser/*rule /usr/local/share/rules

git clone https://github.com/hashcat/princeprocessor.git
cd /usr/local/src/princeprocessor/src/
make
cp pp64.bin /usr/local/bin/

mkdir -p /usr/share/hashcat/rules/

cd /usr/local/share
git clone https://github.com/danielmiessler/SecLists.git

cd /usr/local/share/wordlists

curl -Lk -O https://download.weakpass.com/wordlists/1852/Top109Million-probable-v2.txt.gz
curl -Lk -O https://download.weakpass.com/wordlists/1853/Top12Thousand-probable-v2.txt.gz
curl -Lk -O https://download.weakpass.com/wordlists/1854/Top1575-probable2.txt.gz
curl -Lk -O https://download.weakpass.com/wordlists/1855/Top1pt6Million-probable-v2.txt.gz
curl -Lk -O https://download.weakpass.com/wordlists/1856/Top207-probable-v2.txt.gz
curl -Lk -O https://download.weakpass.com/wordlists/1857/Top29Million-probable-v2.txt.gz
curl -Lk -O https://download.weakpass.com/wordlists/1858/Top2Billion-probable-v2.txt.gz
curl -Lk -O https://download.weakpass.com/wordlists/1859/Top304Thousand-probable-v2.txt.gz
curl -Lk -O https://download.weakpass.com/wordlists/1860/Top353Million-probable-v2.txt.gz

gunzip *gz
